<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class UserTableExpand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('name', 'first_name');
            $table->string('last_name');
            $table->string('username');
            $table->dateTime('last_login')->nullable();
            $table->boolean('active')->default(false);
            $table->dateTime('activated_on')->nullable();
            $table->dateTime('deactivated_on')->nullable();
            $table->boolean('tc_accepted');
            $table->boolean('is_admin');
        });

        // Insert some stuff

        DB::table('users')->insert([
            [
                'first_name'   => 'Stas',
                'last_name'    => 'Solodkin',
                'username'     => 'admin',
                'email'        => 'info@alion.com.au',
                'is_admin'     => 1,
                'password'     => '$2y$10$9HMzCl6TkaU5l1UhPjLEMOyoqEyoUF6AJKLNzwYYkGRkajAuWV5xu', // 3RdInRoot
                'active'       => 1,
                'tc_accepted'  => 1,
                'activated_on' => new Carbon(),
                'created_at'   => new Carbon(),
                'updated_at'   => new Carbon()
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
