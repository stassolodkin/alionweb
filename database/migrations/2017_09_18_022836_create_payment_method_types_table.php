<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethodTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_method_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_name');
            $table->string('sub_type_name');
            $table->text('description');
            $table->timestamps();
        });
      
       DB::table('payment_method_types')->insert([
            [
                'type_name' => 'PayPal',
                'sub_type_name' => 'Express Checkout',
                'description' => 'This is the main PayPal payment method',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'type_name' => 'Credit Card',
                'sub_type_name' => 'Visa',
                'description' => 'Visa Credit Card',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'type_name' => 'Credit Card',
                'sub_type_name' => 'Master Card',
                'description' => 'Master Card',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'type_name' => 'Credit Card',
                'sub_type_name' => 'American Express',
                'description' => 'AMEX Credit Card',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'type_name' => 'Credit Card',
                'sub_type_name' => 'Discover Card',
                'description' => 'Discover Card',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'type_name' => 'Credit Card',
                'sub_type_name' => 'Diners Club',
                'description' => 'Diners Club Card',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_method_types');
    }
}
