<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company', 50);
            $table->string('department', 50)->nullable();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('position', 50);
            $table->string('email', 100);
            $table->string('skills', 255)->nullable();
            $table->string('job_description', 255);
            $table->string('message', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
