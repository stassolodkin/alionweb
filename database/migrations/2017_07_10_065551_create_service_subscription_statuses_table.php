<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;
use \App\Models\ServiceSubscriptionStatus;

class CreateServiceSubscriptionStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_subscription_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });

        // Insert some stuff
        DB::table('service_subscription_statuses')->insert([
            [
                'name' => 'Created',
                'description' => 'Just created but has not been paid. It should be part of a pending invoice.',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Active',
                'description' => 'Activated by email.',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Suspended',
                'description' => 'Subscription has been suspended for non payment.',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Cancelled',
                'description' => 'Subscription has been cancelled.',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Ended',
                'description' => 'Subscription has ended.',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_subscription_statuses');
    }
}
