<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('account_status_id');
//            $table->integer('account_number');
            $table->dateTime('activated_on')->nullable();
            $table->dateTime('deactivated_on')->nullable();
            $table->string('domain');
            $table->string('business_name')->nullable();
            $table->string('abn')->nullable();
            $table->timestamps();
        });

        DB::update("ALTER TABLE accounts AUTO_INCREMENT = 12450;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
