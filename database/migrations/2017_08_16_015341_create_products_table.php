<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ProductType;
use App\Models\ProductStatus;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('external_reference');
            $table->unsignedInteger('product_type_id');
            $table->unsignedInteger('product_status_id');
            $table->float('initial_price', 8, 2)->nullable();
            $table->unsignedInteger('initial_term')->nullable();
            $table->float('price', 8, 2)->nullable();
            $table->string('frequency')->nullable();
            $table->string('currency')->nullable();
            $table->timestamps();
        });

        // Insert some stuff
        DB::table('products')->insert([
            [
                'name' => 'Good',
                'description' => 'Good Plan',
                'external_reference' => 'basic',
                'product_type_id' => ProductType::MAIN,
                'product_status_id' => ProductStatus::ACTIVE,
                'initial_price' => 5,
                'initial_term' => 12,
                'price' => 8,
                'frequency' => '1 month',
                'currency' => 'AUD',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Better',
                'description' => 'Better Plan',
                'external_reference' => 'intermediate',
                'product_type_id' => ProductType::MAIN,
                'product_status_id' => ProductStatus::ACTIVE,
                'initial_price' => 8,
                'initial_term' => 12,
                'price' => 12,
                'frequency' => '1 month',
                'currency' => 'AUD',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Best',
                'description' => 'Best Plan',
                'external_reference' => 'advanced',
                'product_type_id' => ProductType::MAIN,
                'product_status_id' => ProductStatus::ACTIVE,
                'initial_price' => 0,
                'initial_term' => 6,
                'price' => 19,
                'frequency' => '1 month',
                'currency' => 'AUD',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'IP',
                'description' => 'Dedicated IP address',
                'external_reference' => 'Dedicated IP',
                'product_type_id' => ProductType::ADDON,
                'product_status_id' => ProductStatus::ACTIVE,
                'initial_price' => 4.99,
                'initial_term' => null,
                'price' => 4.99,
                'frequency' => '1 month',
                'currency' => 'AUD',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Backups',
                'description' => 'Daily backup service.',
                'external_reference' => 'backups',
                'product_type_id' => ProductType::ADDON,
                'product_status_id' => ProductStatus::ACTIVE,
                'initial_price' => 2.99,
                'initial_term' => null,
                'price' => 2.99,
                'frequency' => '1 month',
                'currency' => 'AUD',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Shell',
                'description' => 'SSH access for advanced users',
                'external_reference' => 'ssh',
                'product_type_id' => ProductType::ADDON,
                'product_status_id' => ProductStatus::ACTIVE,
                'initial_price' => 4.99,
                'initial_term' => null,
                'price' => 4.99,
                'frequency' => '1 month',
                'currency' => 'AUD',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'SSL',
                'description' => 'SSL Certificate',
                'external_reference' => 'SSL',
                'product_type_id' => ProductType::ADDON,
                'product_status_id' => ProductStatus::ACTIVE,
                'initial_price' => 4.99,
                'initial_term' => null,
                'price' => 4.99,
                'frequency' => '1 month',
                'currency' => 'AUD',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
