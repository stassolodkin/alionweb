<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;
use App\Models\ServiceStatus;

class CreateServiceStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });

        // Insert some stuff
        DB::table('service_statuses')->insert([
            [
                'name' => 'Created',
                'description' => 'Just created.',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Active',
                'description' => 'Activated by email.',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Suspended',
                'description' => 'Suspended for non payment.',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ],
            [
                'name' => 'Cancelled',
                'description' => 'Account was cancelled.',
                'created_at' => new \Carbon\Carbon(),
                'updated_at' => new \Carbon\Carbon()
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_statuses');
    }
}
