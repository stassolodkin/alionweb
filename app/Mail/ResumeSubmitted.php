<?php

namespace App\Mail;

use App\Models\Candidate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResumeSubmitted extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $firstName;
    public $lastName;
    public $email;
    public $phone;
    public $skills;
    public $resume;
    public $message;
    public $submissionDate;

    private $toCandidate;

    /**
     * Create a new message instance.
     *
     * @param Candidate $candidate
     * @param bool $toCandidate
     *
     * @return void
     */
    public function __construct(Candidate $candidate, $toCandidate=false)
    {
        $this->firstName = $candidate->first_name;
        $this->lastName = $candidate->last_name;
        $this->email = $candidate->email;
        $this->phone = $candidate->phone;
        $this->skills = $candidate->skills;
        $this->resume = $candidate->resume;
        $this->message = $candidate->message;
        $this->submissionDate = $candidate->created_at;

        $this->toCandidate = $toCandidate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->toCandidate) {
            return $this->view('email.resume_submitted_to_candidate');
        } else {

            $extension = pathinfo($this->resume, PATHINFO_EXTENSION);

            return $this->view('email.resume_submitted')->attach( $this->resume, [
                'as' => 'Resume_' . $this->firstName . '_' . $this->lastName . '_' . $this->submissionDate->format('m/d/Y') . '.' . $extension,
                'mime' => mime_content_type($this->resume),
            ]);
        }
    }
}
