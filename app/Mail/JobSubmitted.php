<?php

namespace App\Mail;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobSubmitted extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $company;
    public $department;
    public $firstName;
    public $lastName;
    public $position;
    public $email;
    public $skills;
    public $jobDescription;
    public $message;
    public $submissionDate;

    private $toEmployer;

    /**
     * Create a new message instance.
     *
     * @param Job $job
     * @param bool $toEmployer
     *
     * @return void
     */
    public function __construct(Job $job, $toEmployer=false)
    {
        $this->company = $job->company;
        $this->department = $job->department;
        $this->firstName = $job->first_name;
        $this->lastName = $job->last_name;
        $this->position = $job->position;
        $this->email = $job->email;
        $this->skills = $job->skills;
        $this->jobDescription = $job->job_description;
        $this->message = $job->message;
        $this->submissionDate = $job->created_at;

        $this->toEmployer = $toEmployer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->toEmployer) {
            return $this->view('email.job_submitted_to_employer');
        } else {

            $extension = pathinfo($this->jobDescription, PATHINFO_EXTENSION);

            return $this->view('email.job_submitted')->attach( $this->jobDescription, [
                'as' => 'Job_' . $this->company . '_' . $this->position . '_' . $this->submissionDate->format('m/d/Y') . '.' . $extension,
                'mime' => mime_content_type($this->jobDescription),
            ]);
        }
    }
}
