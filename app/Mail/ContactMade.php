<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMade extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $emailName;
    public $emailFrom;
    public $emailSubject;
    public $emailBody;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emailName, $emailFrom, $emailSubject, $emailBody)
    {
        $this->emailName = $emailName;
        $this->emailFrom = $emailFrom;
        $this->emailSubject = $emailSubject;
        $this->emailBody = $emailBody;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.contact_made');
    }
}
