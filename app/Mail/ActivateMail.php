<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivateMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $firstName;
    public $link;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstName, $link)
    {
        $this->firstName = $firstName;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.user_activate');
    }
}
