<?php

namespace App\Mail;

use App\Models\CallRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CallRequestReceived extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $phone;
    public $topic;


    /**
     * CallRequestReceived constructor.
     * @param CallRequest $callRequest
     */
    public function __construct(CallRequest $callRequest)
    {
        $this->name = $callRequest->name;
        $this->email = $callRequest->email;
        $this->phone = $callRequest->phone;
        $this->topic = $callRequest->topic;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.call_request_received');
    }
}
