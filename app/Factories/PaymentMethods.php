<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 16/8/17
 * Time: 5:08 PM
 */

namespace App\Factories;

use App\Models\PaymentMethod;
use App\Models\PaymentMethodType;
use App\Services\CreditCardHelper;

class PaymentMethods
{

  public function createPaymentMethod($data, $accountId) {
      
    $cardTypeId = CreditCardHelper::getCardType($data['cardNumber']);
    if(empty($cardTypeId)) {
        return null;
    }
    
    try {
      $paymentMethod = new PaymentMethod();
      $paymentMethod->account_id = $accountId;
      $paymentMethod->name = "Credit Card";
      $paymentMethod->description = PaymentMethodType::find($cardTypeId) . " Credit Card";
      $paymentMethod->payment_method_type_id = $cardTypeId;
      $paymentMethod->primary = true;
      $paymentMethod->active = true;
      $paymentMethod->method_data = serialize([
          'cardNumber' => $data['cardNumber'],
          'cardHolder' => $data['cardHolder'],
          'expiryMonth' => $data['expiryMonth'],
          'expiryYear' => $data['expiryYear'],
          'cvvCode' => $data['cvvCode']
      ]);
      $paymentMethod->save();
      
    } catch (\Exception $e) {
      return null;
    }
    
    return $paymentMethod;
  }
}