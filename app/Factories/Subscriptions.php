<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 16/8/17
 * Time: 5:08 PM
 */

namespace App\Factories;

use App\Models\Account;
use App\Models\Product;
use App\Models\Service;
use App\Models\ServiceStatus;
use App\Models\ServiceSubscription;
use App\Models\ServiceSubscriptionStatus;

class Subscriptions
{

    /**
     *
     * name, description and external reference may later change after the service has been paid for and resources are allocated in cpanel
     * Initially they are copied from the product table
     * This is the main package subscription
     *
     * @param Product $product
     * @param Account $account
     * @return ServiceSubscription
     */
    public function createInitialSubscription(Product $product, Account $account, $term = null) {

        /** @var Service $service */
        $service = Service::create([
            'account_id'           => $account->id,
            'product_id'           => $product->id,
            'name'                 => $product->name,
            'start_date'           => new \Carbon\Carbon(),
            'description'          => $product->description,
            'external_reference'   => $product->external_reference,
            'service_status_id'    => ServiceStatus::CREATED
        ]);

        /** @var ServiceSubscription $serviceSubscription */
        $serviceSubscription = ServiceSubscription::create([
            'service_id'                      => $service->id,
            'service_subscription_status_id'  => ServiceSubscriptionStatus::CREATED,
            'monthly_rate'                    => $product->initial_price,
            'term_months'                     => empty($term) ? $product->initial_term : $term
        ]);

        return $serviceSubscription;
    }
}