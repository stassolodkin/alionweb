<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentMethod
 * @property int id
 * @property string type_name
 * @property string sub_type_name
 * @property string description
 */
class PaymentMethodType extends Model
{
    const PAY_PAL = 1;
    const VISA = 2;
    const MASTER_CARD = 3;
    const AMEX = 4;
    const DISCOVER = 5;
    const DINERS = 6;

    protected $fillable = ['type_name', 'sub_type_name', 'description'];
  
    public function paymentMethods() {
        return $this->hasMany(PaymentMethod::class);
    }
}
