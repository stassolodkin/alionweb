<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;
// use Illuminate\Database\Eloquent\Model;

/**
 * Class SlackTeam
 */
// class SlackTeam extends Model
class SlackTeam
{
    use Notifiable;

    // Specify Slack Webhook URL to route notifications to
    public function routeNotificationForSlack()
    {
        return config('app.slack_webhook_url');
    }
}
