<?php

namespace App\Models;

class ServiceType
{
    const PACKAGE = 1;
    const IP = 2;
    const SHELL = 3;
    const BACKUP = 4;
    const SSL = 5;
}
