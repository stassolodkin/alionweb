<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceSubscription
 * @property int id
 * @property int service_id
 * @property int service_subscription_status_id
 * @property float monthly_rate
 * @property int term_months
 * @property \DateTime activated_on
 * @property \DateTime deactivated_on
 * @property \DateTime created_at
 * @property \DateTime updated_at
 */
class ServiceSubscription extends Model
{
    protected $fillable = [
        'service_id',
        'service_subscription_status_id',
        'invoice_id',
        'monthly_rate',
        'term_months',
        'activated_on',
        'deactivated_on'
    ];

    public function serviceSubscriptionStatus() {
        return $this->belongsTo(ServiceSubscriptionStatus::class);
    }

    public function service() {
        return $this->belongsTo(Service::class);
    }

    public function invoice() {
        return $this->belongsTo(Invoice::class);
    }
}
