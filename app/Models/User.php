<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Job
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $password
 * @property string $d_password
 * @property string $email
 * @property boolean $is_admin
 * @property boolean $active
 * @property Carbon $activated_on
 * @property Carbon $deactivated_on
 * @property boolean $tc_accepted
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
        'd_password',
        'is_admin',
        'active',
        'activated_on',
        'deactivated_on',
        'tc_accepted',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function messages() {
        return $this->hasMany(  Message::class);
    }

    // All the notes created by this user
    public function accountNotes() {
        return $this->hasMany(  AccountNote::class);
    }

    public function account() {
        return $this->hasOne(Account::class);
    }
}
