<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Document
 * @property int id
 * @property string name
 * @property string description
 * @property string content
 * @property string link
 * @property string type
 * @property string sort_order
 * @property string published
 */

class Document extends Model
{
    protected $fillable = ['name', 'description', 'content', 'link', 'type', 'sort_order', 'published'];
}
