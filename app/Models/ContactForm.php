<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ContactForm
 * @property int id
 * @property string name
 * @property string email
 * @property string subject
 * @property string body
 * @property string source_ip
 */
class ContactForm extends Model
{
    protected $fillable = ['name', 'email', 'subject', 'body', 'source_ip'];

    public function toString() {
        return    "Name: " . $this->name .
                ", Email: " . $this->email .
                ", Subject: " . $this->subject .
                ", Body: " . $this->body .
                ", Source IP: " . $this->source_ip;
    }
}
