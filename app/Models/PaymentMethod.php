<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentMethod
 * @property int id
 * @property int account_id
 * @property int payment_method_type_id
 * @property string name
 * @property string description
 * @property string method_data
 * @property boolean primary
 * @property boolean active
 */
class PaymentMethod extends Model
{
    protected $fillable = ['account_id', 'payment_method_type_id', 'name', 'description', 'method_data', 'primary', 'active'];
  
    public function account() {
        return $this->belongsTo(Account::class);
    }
  
    public function paymentMethodType() {
        return $this->belongsTo(PaymentMethodType::class);
    }
  
   public function payments() {
        return $this->hasMany(Payment::class);
    }
}
