<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Candidate
 * @property int id
 * @property string name
 * @property string email
 * @property string phone
 * @property string topic
 * @property string source_ip
 */
class CallRequest extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'topic', 'source_ip'];

    public function toString() {
        return   "Name: " . $this->name .
               ", Email: " . $this->email .
               ", Phone: " . $this->phone .
               ", Topic: " . $this->topic .
               ", Source IP: " . $this->source_ip;
    }
}
