<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountStatus
 * @property int id
 * @property string name
 * @property string description
 */

class AccountStatus extends Model
{
    const CREATED = 1;
    const ACTIVE = 2;
    const SUSPENDED = 3;
    const CANCELLED = 4;

    protected $fillable = ['name', 'description'];

    public function accounts() {
        return $this->hasMany(Account::class);
    }
}
