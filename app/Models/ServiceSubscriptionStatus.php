<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceSubscriptionStatus
 * @property int id
 * @property string name
 * @property string description
 */

class ServiceSubscriptionStatus extends Model
{
    const CREATED = 1;
    const ACTIVE = 2;
    const SUSPENDED = 3;
    const CANCELLED = 4;
    const ENDED = 5;

    protected $fillable = ['name', 'description'];

    public function serviceSubscriptions() {
        return $this->hasMany(ServiceSubscription::class);
    }
}
