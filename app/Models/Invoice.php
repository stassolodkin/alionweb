<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Invoice
 * @property int id
 * @property int account_id
 * @property int invoice_status_id
 * @property int term
 * @property float total
 * @property string payment_data
 * @property \DateTime created_at
 * @property \DateTime updated_at
 */
class Invoice extends Model
{
    protected $fillable = [
        'account_id',
        'invoice_status_id',
        'term',
        'total',
        'payment_data'
    ];

    public function invoiceStatus() {
        return $this->belongsTo(InvoiceStatus::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account() {
        return $this->belongsTo(Account::class);
    }

    public function serviceSubscriptions() {
        return $this->hasMany(ServiceSubscription::class);
    }
  
    public function payments() {
        return $this->hasMany(Payment::class);
    }

    public function addLine(ServiceSubscription $serviceSubscription) {
        $this->serviceSubscriptions()->save($serviceSubscription);
        $this->total += $serviceSubscription->monthly_rate * $serviceSubscription->term_months;
        $this->save();
    }

    /**
     * This is a private function - not a controller action
     * @param $invoice
     * @return array
     */
    public function getInvoiceLines() {

        $subscriptions = [
            'items'=>[],
            'invoice_id' => $this->id
        ];

        /** @var ServiceSubscription $serviceSubscription */
        foreach($this->serviceSubscriptions as $serviceSubscription) {

            $s =  $serviceSubscription->service;
            $p = $s->product;
            $pt = $p->productType;

            if($serviceSubscription->service_subscription_status_id == ServiceSubscriptionStatus::CREATED) {
                $subscriptions['items'][] = [
                    'subscription_id' => $serviceSubscription->id,
                    'name' => $serviceSubscription->service->name,
                    'monthly_rate' => $serviceSubscription->monthly_rate,
                    'term_months' => $serviceSubscription->term_months,
                    'product_type' => $pt->name
                ];
            }
        }

        return $subscriptions;
    }
  
  public function updateTotal() {
    
        $this->total = 0;
        /** @var ServiceSubscription $serviceSubscription */
        foreach($this->serviceSubscriptions as $serviceSubscription) {
           if($serviceSubscription->service_subscription_status_id == ServiceSubscriptionStatus::CREATED) {
             $this->total += $serviceSubscription->term_months * $serviceSubscription->monthly_rate;
           }
        }
    
        $this->save();
  }

    public function enableAllSubscriptions() {

        try {

            /** @var ServiceSubscription $serviceSubscription */
            foreach($this->serviceSubscriptions as $serviceSubscription) {
                if($serviceSubscription->service_subscription_status_id == ServiceSubscriptionStatus::CREATED) {

                    $serviceSubscription->service_subscription_status_id = ServiceSubscriptionStatus::ACTIVE;
                    $serviceSubscription->activated_on = new \Carbon\Carbon();
                    $serviceSubscription->save();

                    $serviceSubscription->service->service_status_id = ServiceStatus::ACTIVE;
                    $serviceSubscription->service->save();
                }
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function convertToPayPalData() {

        $invoiceItems = $this->getInvoiceLines();
        $invoiceToPay = [];
        $invoiceToPay['total'] = $this->total;
        $invoiceToPay['items'] = [];

        foreach($invoiceItems['items'] as $item) {

            $singleItem = [
                'name' => $item['name'],
                'price' => $item['monthly_rate'] * $item['term_months'],
                'quantity' => 1
            ];

            $invoiceToPay['items'][] = $singleItem;
        }

        $now = \Carbon\Carbon::now();
        $invoiceToPay['invoice_id'] = $this->id . '---' . $now->toAtomString();
        $invoiceToPay['invoice_description'] = "Invoice #{$this->id} for account #{$this->account->id}";
        $invoiceToPay['return_url'] = url('/paypal/success');
        $invoiceToPay['cancel_url'] = url('/signup_step_2');

        return $invoiceToPay;
    }

}
