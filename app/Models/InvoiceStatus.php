<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceStatus extends Model
{
    const UNPAID = 1;
    const PAID = 2;
    const CANCELLED = 3;

    protected $fillable = ['name', 'description'];

    public function invoices() {
        return $this->hasMany(Invoice::class);
    }
}
