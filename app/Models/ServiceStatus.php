<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceStatus
 * @property int id
 * @property string name
 * @property string description
 */

class ServiceStatus extends Model
{
    const CREATED = 1;
    const ACTIVE = 2;
    const SUSPENDED = 3;
    const CANCELLED = 4;

    protected $fillable = ['name', 'description'];

    public function services() {
        return $this->hasMany(Service::class);
    }
}
