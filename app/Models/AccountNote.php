<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AccountNote
 * @property int id
 * @property int account_id
 * @property int user_id
 * @property string text
 * @property \DateTime created_at
 * @property \DateTime updated_at
 */
class AccountNote extends Model
{

    protected $fillable = [
        'text'
    ];

    public function account() {
        return $this->belongsTo(Account::class);
    }

    // User that added the note
    public function user() {
        return $this->belongsTo(User::class);
    }
}
