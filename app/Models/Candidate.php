<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Candidate
 * @property int id
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string phone
 * @property string resume
 * @property string skills
 * @property string message
 * @property string source_ip
 */
class Candidate extends Model
{
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'resume', 'skills', 'message', 'source_ip'];

    public function toString()
    {
        return "First Name: " . $this->first_name .
            ", Last Name: " . $this->last_name .
            ", Email: " . $this->email .
            ", Phone: " . $this->phone .
            ", Resume: " . $this->resume .
            ", Skills: " . $this->skills .
            ", Message: " . $this->message .
            ", Source IP: " . $this->source_ip;
    }
}