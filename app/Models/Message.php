<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Job
 * @property int id
 * @property string message
 * @property string conversation_id
 * @property string user_name
 * @property string user_id
 * @property \DateTime created_at
 * @property \DateTime updated_at
 */
class Message extends Model
{
    protected $fillable = ['message', 'user_name', 'user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function conversation() {
        return $this->belongsTo(Conversation::class);
    }
}
