<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    const MAIN = 1;
    const ADDON = 2;

    protected $fillable = ['name'];


    public function products() {
        return $this->hasMany(  Product::class);
    }
}
