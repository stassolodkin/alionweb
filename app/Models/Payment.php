<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Invoice
 * @property int id
 * @property int invoice_id
 * @property int payment_method_id
 * @property string payment_data
 * @property \DateTime created_at
 * @property \DateTime updated_at
 */
class Payment extends Model
{
    protected $fillable = ['invoice_id', 'payment_method_id', 'payment_data'];

    public function invoice() {
        return $this->belongsTo(Invoice::class);
    }

    public function paymentMethod() {
        return $this->belongsTo(PaymentMethod::class);
    }
}