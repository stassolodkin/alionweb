<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Account
 * @property int id
 * @property int account_id
 * @property int service_status_id
 * @property string name
 * @property string description
 * @property string external_reference
 * @property \DateTime start_date
 * @property \DateTime end_date
 * @property \DateTime created_at
 * @property \DateTime updated_at
 */
class Service extends Model
{
    protected $fillable = [
        'name',
        'account_id',
        'product_id',
        'description',
        'service_status_id',
        'external_reference',
        'start_date',
        'end_date'
    ];

    public function account() {
        return $this->belongsTo(Account::class);
    }

    public function serviceStatus() {
        return $this->belongsTo(ServiceStatus::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function serviceSubscriptions() {
        return $this->hasMany(  ServiceSubscription::class);
    }

    public function getCurrentUnpaidSubscription() {

        $currentUnpaidSubscription = DB::table('service_subscriptions')
            ->whereNull('activated_on')
            ->where('service_id', '=', $this->id)
            ->where('created_at', '<', Carbon::now())
            ->where('subscription_status_id', '=', ServiceSubscriptionStatus::CREATED)
            ->first();

        return $currentUnpaidSubscription;
    }
}
