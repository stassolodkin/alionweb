<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Job
 * @property int id
 * @property string company
 * @property string department
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string phone
 * @property string position
 * @property string type
 * @property string skills
 * @property string job_description
 * @property string message
 * @property string source_ip
 */
class Job extends Model
{
    protected $fillable = [
            'company',
            'department',
            'first_name',
            'last_name',
            'email',
            'phone',
            'position',
            'type',
            'skills',
            'job_description',
            'message',
            'source_ip'
        ];

    public function toString() {
        return    "Company: " . $this->company .
                ", Department: " . $this->department .
                ", First Name: " . $this->first_name .
                ", Last Name: " . $this->last_name .
                ", Email: " . $this->email .
                ", Phone: " . $this->phone .
                ", Position: " . $this->position .
                ", Type: " . $this->type .
                ", Skills: " . $this->skills .
                ", Job Description: " . $this->job_description .
                ", Message: " . $this->message .
                ", Source IP: " . $this->source_ip;
    }
}
