<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

/**
 * Class Account
 * @property int $id
 * @property int $user_id
 * @property int $account_status_id
 * @property int $account_number
 * @property string $business_name
 * @property string $abn
 * @property string $domain
 * @property \DateTime $activated_on
 * @property \DateTime $deactivated_on
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 */
class Account extends Model
{

    protected $fillable = [
        'user_id',
        'account_number',
        'business_name',
        'abn',
        'domain',
        'account_status_id',
        'activated_on',
        'deactivated_on'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function accountStatus() {
        return $this->belongsTo(AccountStatus::class);
    }

    public function services() {
        return $this->hasMany(Service::class);
    }

    // All the notes for this account
    public function accountNotes() {
        return $this->hasMany(AccountNote::class);
    }

    public function invoices() {
        return $this->hasMany(Invoice::class);
    }
  
    public function paymentMethods() {
        return $this->hasMany(PaymentMethod::class);
    }

    public function getSubscriptionsDue() {

        $subscriptions = [];

        /** @var Service $service */
        foreach($this->services as $service) {
            $subscriptions[] = $service->getCurrentUnpaidSubscription();
        }

        return $subscriptions;
    }

    public function enable() {

        try {
            $this->account_status_id = AccountStatus::ACTIVE;
            $this->activated_on = new \Carbon\Carbon();
            $this->save();
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return Invoice
     */
    public function getCurrentUnpaidInvoice() {

        $currentUnpaidInvoice = DB::table('invoices')
            ->where('account_id', '=', $this->id)
            ->where('invoice_status_id', '=', InvoiceStatus::UNPAID)
            ->latest()
            ->first();

        if(empty($currentUnpaidInvoice)) {
            return null;
        }
        return Invoice::find($currentUnpaidInvoice->id);
    }
}
