<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @property int id
 * @property string name
 * @property string description
 * @property string external_reference
 * @property float initial_price
 * @property int   initial_term
 * @property float price
 * @property int frequency
 * @property string currency
 * @property int product_type_id
 * @property int product_status_id
 * @property \DateTime created_at
 * @property \DateTime updated_at
 */

class Product extends Model
{
    protected $fillable = [
        'name',
        'description',
        'external_reference',
        'initial_price',
        'initial_term',
        'price',
        'frequency',
        'currency',
        'product_type_id',
        'product_status_id'
    ];

    public function productStatus() {
        return $this->belongsTo(ProductStatus::class);
    }

    public function productType() {
        return $this->belongsTo(ProductType::class);
    }

    public function services() {
        return $this->hasMany(  Service::class);
    }
}
