<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Job
 * @property int id
 * @property string user_name
 * @property string alion_user
 * @property string token
 * @property string status
 * @property string source_ip
 */
class Conversation extends Model
{
    protected $fillable = ['user_name', 'alion_user', 'token', 'status', 'source_ip'];

    public function messages() {
        return $this->hasMany(Message::class);
    }
}
