<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStatus extends Model
{

    const ACTIVE = 1;
    const INACTIVE = 2;

    protected $fillable = ['name', 'description'];

    public function products() {
        return $this->hasMany(  Product::class);
    }
}
