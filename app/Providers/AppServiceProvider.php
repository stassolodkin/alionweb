<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use App\Factories\Subscriptions;
use App\Factories\PaymentMethods;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//         $this->app->singleton(Subscriptions::class, function () {

//             return new Subscriptions();

//         });
      
//       $this->app->singleton(PaymentMethods::class, function () {

//             return new PaymentMethods();

//         });
    }
}
