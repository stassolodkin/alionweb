<?php

namespace App\Events;

use App\Models\Message;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class MessagePosted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Message $message
     */
    public $message;

    /**
     * @var User $user
     */
    public $user;

    /**
     * @var string $userName
     */
    public $userName;
    /**
     * @var string $token
     */
    public $token;


    /**
     * MessagePosted constructor.
     * @param Message $message
     * @param User $user
     */
    public function __construct(Message $message, $userName, $token, User $user=null)
    {
        $this->message = $message;
        $this->user = $user;
        $this->token = $token;
        $this->userName = $userName;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('livechat.' . $this->token);
    }
}
