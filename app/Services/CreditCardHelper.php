<?php
/**
 * Created by PhpStorm.
 * User: stassolodkin
 * Date: 19/09/2017
 * Time: 9:55 PM
 */
namespace App\Services;

use App\Models\Invoice;
use App\Models\PaymentMethodType;

class CreditCardHelper
{

    public static function getCardType($cardNumber) {

        $visa = "/^(?:4[0-9]{12}(?:[0-9]{3})?)$/";
        $masterCard = "/^(?:5[1-5][0-9]{14})$/";
        $amex = "/^(?:3[47][0-9]{13})$/";

        if(preg_match($visa, $cardNumber)) {
            return PaymentMethodType::VISA;
        } else if(preg_match($masterCard, $cardNumber)) {
            return PaymentMethodType::MASTER_CARD;
        } else if(preg_match($amex, $cardNumber)) {
            return PaymentMethodType::AMEX;
        }

        return 0;
    }
}