<?php
/**
 * Created by PhpStorm.
 * User: stassolodkin
 * Date: 19/09/2017
 * Time: 9:55 PM
 */
namespace App\Services;

use App\Models\Invoice;
use App\Models\ServiceSubscriptionStatus;
use App\Models\ProductType;
//use function Sodium\add;

class CPanel
{

    private $apiHost;
    private $apiKey;

    public function __construct($apiHost, $apiKey) {
        $this->apiHost = $apiHost;
        $this->apiKey = $apiKey;
    }

    public function createNewServiceForInvoice(Invoice $invoice) {

        $account = $invoice->account;
        $user = $account->user;
      
        $username = $user->username;
        $password = decrypt($user->d_password);
        $domain = $account->domain;
        $email = $user->email;
      
        $plan = "";
        $ip = 'n';
        $hasShell = 0;
        $ssl = 0;
        $backup = 0;
      
        foreach($invoice->serviceSubscriptions as $serviceSubscription) {
            if($serviceSubscription->service_subscription_status_id == ServiceSubscriptionStatus::ACTIVE) {
                if($serviceSubscription->service->product->product_type_id == ProductType::MAIN) {
                    $plan = $serviceSubscription->service->external_reference;
                } else if($serviceSubscription->service->name == "IP") {
                    $ip = 'y';
                } else if($serviceSubscription->service->name == "Shell") {
                    $hasShell = 1;
                } else if($serviceSubscription->service->name == "SSL") {
                    $ssl = 1;
                } else if($serviceSubscription->service->name == "Backups") {
                    $backup = 1;
                }
            }
        }

        return $this->createServices(
                                $username,
                                $password,
                                $domain,
                                $email,
                                $plan,
                                $ip,
                                $hasShell,
                                $ssl,
                                $backup
                            );

    }

    /**
     * @param $username
     * @param $password
     * @param $domain
     * @param $email
     * @param $plan
     * @param $ip
     * @param $hasShell
     * @param $ssl
     * @param $backup
     * @return bool
     * @throws \Exception
     */
    private function createServices(
                                    $username,
                                    $password,
                                    $domain,
                                    $email,
                                    $plan,
                                    $ip,
                                    $hasShell,
                                    $ssl,
                                    $backup
                                ) {

        $createAccountQuery = urlencode("/json-api/createacct?api.version=1" .
                                '&username=' . $username .
                                '&password=' . $password .
                                '&domain='   . $domain .
                                '&email='    . $email .
                                '&plan='     . $plan .
                                '&ip='       . $ip .
                                '&hasshell=' . $hasShell .
                                '&language=en&owner=root');

        try {
            $result = $this->sendQuery($createAccountQuery);
            $json = json_decode($result);
            if($json['metadata']['result'] == 0) {
                return false;
            }
            $x=1;
//             foreach ($json->{'data'}->{'acct'} as $userdetails) {
//                 echo "\t" . $userdetails->{'user'} . "\n";
//             }

        } catch (\Exception $e) {
            throw $e;
        }

        return true;

    }
  
    public function listAccounts() {

        $listAccountQuery = "/json-api/listaccts?api.version=1";

        try {
            $result = $this->sendQuery($listAccountQuery);
//             $json = json_decode($result);
            
//             foreach ($json->{'data'}->{'acct'} as $userdetails) {
//                 echo "\t" . $userdetails->{'user'} . "\n";
//             }

        } catch (\Exception $e) {
            throw $e;
        }
        
        return $result;
    }
  
    private function sendQuery($query) {
 
        $fullQuery = $this->apiHost . $query;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);

        $header[0] = "Authorization: whm root:" . $this->apiKey;
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl, CURLOPT_URL, $fullQuery);

        $result = curl_exec($curl);

        $httpStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($httpStatus == 200) {
            curl_close($curl);
            return $result;
        } else {
            curl_close($curl);
            throw new \Exception("[!] Error: " . $httpStatus . " returned\n");
        }
    }
}