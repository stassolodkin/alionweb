<?php

namespace App\Http\Controllers;

use App\Factories\Subscriptions;
use App\Factories\PaymentMethods;
use App\Models\Account;
use App\Models\AccountStatus;
use App\Models\Invoice;
use App\Models\InvoiceStatus;
use App\Models\Product;
use App\Models\ServiceSubscription;
use App\Models\ServiceSubscriptionStatus;
use App\Models\Service;
use App\Models\ServiceStatus;
use App\Models\User;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\Models\PaymentMethodType;
use App\Repositories\Products;
use App\Repositories\ServiceSubscriptions;
use App\Mail\GeneralMail;
use App\Services\CPanel;
use App\Services\CreditCardHelper;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Srmklive\PayPal\Facades\PayPal;
use Illuminate\Support\Facades\DB;
use Srmklive\PayPal\Services\ExpressCheckout;
use Cartalyst\Stripe\Stripe;


class UserHomeController extends Controller
{

    protected $products;
    protected $serviceSubscriptions;
    protected $cpanel;

    /** @var Subscriptions $subscriptionFactory */
    protected $subscriptionFactory;
    /** @var PaymentMethods $paymentMethodFactory */
    protected $paymentMethodFactory;

    public function __construct(Products $products, ServiceSubscriptions $servicesSubscriptions, CPanel $cpanel, Subscriptions $subscriptionFactory, PaymentMethods $paymentMethodFactory)
    {
        
        $this->products             = $products;
        $this->serviceSubscriptions = $servicesSubscriptions;
        $this->cpanel               = $cpanel;
        $this->subscriptionFactory  = $subscriptionFactory;
        $this->paymentMethodFactory = $paymentMethodFactory;
    }

    public function signUpStep2() {

        /** @var User $user */
        $user = Auth::user();

        if($user->is_admin) {
            return view('home');
        }

        /** @var Account $account */
        $account = $user->account;

        $invoice = $account->getCurrentUnpaidInvoice();

        if(empty($invoice)) {
            return redirect('/home');
        }

        return view('pages.signup2', ['account' => $account, 'invoice' => $invoice]);
    }

    public function getInvoiceItems($invoice_id) {

        /** @var User $user */
        $user = Auth::user();

        /** @var Account $account */
        $account = $user->account;

        $accountInvoice = $account->getCurrentUnpaidInvoice();

        /** @var Invoice $invoice */
        $invoice = Invoice::find($invoice_id);

        if($invoice->id != $accountInvoice->id) {
            // This performs a security check and makes sure that the account of the logged in user actually owns the invoice
            return json_encode(['error' => 'Account does not own the specified invoice']);
        }

        $invoiceItems = $invoice->getInvoiceLines();

        return json_encode($invoiceItems);
    }


    public function addSubscription(Request $request) {

        $invoiceId = $request->get('invoiceId');
        /** @var Invoice $invoice */
        $invoice = Invoice::find($invoiceId);

        // Use name if no subscription ID
        $productName = $request->get('productName');
        $serviceSubscriptionId = $request->get('currentSubscription');

        if(!empty($serviceSubscriptionId)) {

            /** @var ServiceSubscription $serviceSubscription */
            $serviceSubscription = ServiceSubscription::find($serviceSubscriptionId);
            if(!empty($serviceSubscription)) {
                $serviceSubscription->service_subscription_status_id = ServiceSubscriptionStatus::CREATED;
                $serviceSubscription->save();

                $invoice->updateTotal();
                $invoiceItems = $invoice->getInvoiceLines();
                return json_encode($invoiceItems);
            }
        }

        // Find an existing subscription for this invoice if it exists
        /** @var ServiceSubscription $serviceSubscription */
        $serviceSubscription = $this->serviceSubscriptions->getServiceSubscriptionByNameAndInvoice($productName, $invoiceId);
        if(!empty($serviceSubscription)) {
            $serviceSubscription->service_subscription_status_id = ServiceSubscriptionStatus::CREATED;
            $serviceSubscription->save();
        } else {
            /** @var Product $product */
            $product = $this->products->getProductByName($productName);
            if(!empty($product)) {

                /** @var User $user */
                $user = Auth::user();

                /** @var Account $account */
                $account = $user->account;

                $serviceSubscription = $this->subscriptionFactory->createInitialSubscription($product, $account, $invoice->term);
                $invoice->addLine($serviceSubscription);

            } else {
                abort(522);
            }
        }

        $invoice->updateTotal();
        $invoiceItems = $invoice->getInvoiceLines();
        return json_encode($invoiceItems);

    }

    public function removeSubscription(Request $request) {

        $invoiceId = $request->get('invoiceId');
        $serviceSubscriptionId = $request->get('currentSubscription');

        /** @var ServiceSubscription $serviceSubscription */
        $serviceSubscription = ServiceSubscription::find($serviceSubscriptionId);
        if(!empty($serviceSubscription)) {
            $serviceSubscription->service_subscription_status_id = ServiceSubscriptionStatus::CANCELLED;
            $serviceSubscription->save();
        } else {
            return response()->json(['error' => "Cannot remove a subscription that does not exist"], 422);
        }

        /** @var Invoice $invoice */
        $invoice = Invoice::find($invoiceId);
        $invoice->updateTotal();
        $invoiceItems = $invoice->getInvoiceLines();

        return json_encode($invoiceItems);

    }

    public function signUpFinal(Request $request) {

        $invoiceId = $request->get('invoiceId');
        /** @var Invoice $invoice */
        $invoice = Invoice::find($invoiceId);
  
        if(empty($invoice)) {
          abort(522, "Are you trying to access this URL directly?"); // This should never happen as the invoice has resently been retrieved
        }

        $paymentOption = $request->get('payment_option');

        if($paymentOption == "credit_card") {
          
            $this->validate($request, [
              'cardNumber' => 'required',
              'cardHolder' => 'required',
              'expiryMonth' => 'required',
              'expiryYear' => 'required',
              'cvvCode' => 'required',
            ]);

            $cardTypeId = CreditCardHelper::getCardType($request->get('cardNumber'));
            if(empty($cardTypeId)) {
                return back()->withInput(['PAY_ERROR' => "Could not process invoice #" . $invoice->id . ". We only accept Visa, Master Card or American Express"]);
            }

            DB::beginTransaction();
            
            $payment = $this->payThroughStripe($request, $invoice);
            if(!empty($payment)) {

                if($request->filled('remember') && $request->filled('remember') == 'on') {
                  
                    $paymentMethod = $this->paymentMethodFactory->createPaymentMethod($request->all(), $invoice->account_id);
                    if(empty($paymentMethod)) {
                        $this->abortTransaction("Could not process invoice #" . $invoice->id . ". Unable to create payment method");
                    } else {
                      $payment->payment_method_id = $paymentMethod->id;
                      $payment->save();
                    }
                }
              
                DB::commit();
                $this->logAndEmail('CreditCard: Successfully paid for invoice #' . $invoiceId);
                return redirect('/home');
            } else {
                DB::rollBack();
                return back()->withInput(['PAY_ERROR' => 'Unable to process payment. Please check your credit card details']);
            }
          
        } else if($paymentOption == "paypal") {
  
            $provider = new ExpressCheckout();
            $options = [
                'BRANDNAME' => 'Alion Host',
                'LOGOIMG' => 'https://www.alion.com.au/images/alion_logo_green_blue.png',
                'CHANNELTYPE' => 'Merchant'
            ];

            $invoiceToPay = $invoice->convertToPayPalData();
            $response = $provider->addOptions($options)->setExpressCheckout($invoiceToPay);

            if(!empty($response['ACK']) && $response['ACK'] == 'Failure') {
                 $errorMessage = "PayPal says: " . $response['L_SHORTMESSAGE0'];
                 return back()->withInput(['PAY_ERROR' => $errorMessage]);
            }

            return redirect($response['paypal_link']);

        } else {
            return back()->withInput(); // ThiOk - thank yous should never happen unless the link is accessed directly
        }
    }
    
    public function testCpanelAPI() {
        
        try {
            $response = $this->cpanel->listAccounts();
            dd($response); die;
        } catch (\Exception $e) {
            dd('Error occurred');die;
        }
    }
    
    public function paypalSuccess(Request $request) {

        $token = $request->get('token');
        $payerID = $request->get('PayerID');
        
        $provider = new ExpressCheckout();
        
        $checkoutData = $provider->getExpressCheckoutDetails($token);
        $invoiceId = explode('---', $checkoutData['INVNUM']);
        $invoiceId = $invoiceId[0];

        /** @var Invoice $invoice */
        $invoice = Invoice::find($invoiceId);
        
        if(empty($invoice)) {
          $this->logAndEmail('paypalSuccess: Unable to locate invoice #' . $invoiceId);
          abort(522); // This should never happen as this was checked before the data was sent to PayPal
        }

        $invoiceToPay = $invoice->convertToPayPalData();
        $paymentData = $provider->doExpressCheckoutPayment($invoiceToPay, $token, $payerID);
  
        if(!empty($paymentData) && $paymentData['ACK'] != 'Failure') {          
          
          try {
            
            DB::beginTransaction();
            // This is the initial idea, so now the paymentData is being saved twice, in invoice and in a separate payments table
            $invoice->payment_data = serialize($paymentData);
            $invoice->invoice_status_id = InvoiceStatus::PAID;
            $invoice->save();
            
            $payment = new Payment();
            $payment->invoice_id = $invoice->id;
            $payment->payment_data = serialize($paymentData);
            $payment->save();
            
          } catch (\Exception $e) {
            $this->abortTransaction('paypalSuccess: ' . $e->getMessage() . ' for invoice #' . $invoice->id);
          }
        
          if(!$invoice->enableAllSubscriptions()) {
              $this->abortTransaction("Could not enable subscriptions");
          }

          if(!$invoice->account->enable()) {
              $this->abortTransaction("Could not enable account");
          }
      
          if(!$this->cpanel->createNewServiceForInvoice($invoice)) {
              $this->abortTransaction("Unadble to rete services");
          }
        }

        DB::commit();
        $this->logAndEmail('PaypalSuccess: Successfully paid for invoice #' . $invoiceId);
        return redirect('/home');
    }
  
    private function abortTransaction($message) {
        $this->logAndEmail($message);
        DB::rollBack();
        abort(522, $message);
    }
  
    private function payThroughStripe(Request $request, Invoice $invoice) {

          $stripeConfig = config('services.stripe');

          $stripe = new Stripe($stripeConfig['secret'], $stripeConfig['version']);
         
          try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $request->get('cardNumber'),
                        'exp_month' => $request->get('expiryMonth'),
                        'exp_year'  => $request->get('expiryYear'),
                        'cvc'       => $request->get('cvvCode'),
                    ],
                ]);

                if (!isset($token['id'])) {
                    $this->logAndEmail('payThroughStripe: The Stripe Token was not generated correctly for invoice #' . $invoice->id);
                    return null;
                }
            
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => 'AUD',
                    'amount'   => $invoice->total,
                    'description' => 'Add in wallet',
                ]);
            
                if($charge['status'] == 'succeeded') {
                  
                  $invoice->payment_data = serialize($charge);
                  $invoice->invoice_status_id = InvoiceStatus::PAID;
                  $invoice->save();

                  try {
                      $payment = new Payment();
                      $payment->invoice_id = $invoice->id;
                      $payment->payment_data = serialize($charge);;
                      $payment->save();
                  } catch (\Exception $e) {
                      $stripe->refunds()->create($charge['id']);
                      $this->logAndEmail('payThroughStripe:Cannot create a payment record for invoice #' . $invoice->id .
                                        "Issuing a refund. Charge ID: " . $charge['id'] . ", Account ID: " . $invoice->account_id);
                      return null;
                  }

                  if(!$invoice->enableAllSubscriptions()) {
                      $stripe->refunds()->create($charge['id']);
                      $this->logAndEmail('Unable to enable subscriptions for invoice #' . $invoice->id .
                                        "Issuing a refund. Charge ID: " . $charge['id'] . ", Account ID: " . $invoice->account_id);
                      return null;
                  }
                  if(!$invoice->account->enable()) {
                      $stripe->refunds()->create($charge['id']);
                      $this->logAndEmail('Unable to activate account #' . $invoice->account_id .
                                        "Issuing a refund. Charge ID: " . $charge['id'] . ", Account ID: " . $invoice->id);
                      return null;
                  }
                  if(!$this->cpanel->createNewServiceForInvoice($invoice)) {
                      $stripe->refunds()->create($charge['id']);
                      $this->logAndEmail('Unable to enable account #' . $invoice->id .
                                        "Issuing a refund. Charge ID: " . $charge['id'] . ", Account ID: " . $invoice->account_id);
                      return null;
                  }
                  
                  $this->logAndEmail('payThroughStripe: Successfully paid for invoice #' . $invoice->id);      
                  return $payment;  
                  
                } else {
                  $this->logAndEmail('payThroughStripe: Unable to make a payment for invoice #' . $invoice->id);
                  return null;
                }
        } catch (\Exception $e) {
            if(!empty($charge) && !empty($stripe)) {
                $refund = $stripe->refunds()->create($charge['id']);
                $this->logAndEmail('Stripe token was not generated. Invoice ID #' . $invoice->id .
                    "Issuing a refund. Charge ID: " . $charge['id'] . ", Account ID: " . $invoice->account_id);
            }
            $this->logAndEmail('payThroughStripe: The Stripe Token was not generated correctly for invoice #' . $invoice->id);
            return null;
        }
      
          
    }
  
    /**
    * @param $message
    */
    private function logAndEmail($message) {
        Log::error($message);
        Mail::to(config('app.alion_email'))->send(new GeneralMail($message));
    }
}
