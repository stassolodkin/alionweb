<?php

namespace App\Http\Controllers;

use App\Models\AccountStatus;
use App\Models\Document;
use App\Models\CallRequest;
use App\Models\Candidate;
use App\Models\ContactForm;
use App\Models\Conversation;
use App\Models\Job;
use App\Models\Account;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var User $user */
        $user = Auth::user();
 
        if($user->is_admin == 1) {
            return view('home');
        }
        /** @var Account $account */
        $account = $user->account;

        if($account->account_status_id == AccountStatus::CREATED) {
            return redirect('signup_step_2');
        }
      
        return view('home');
    }

    public function applications() {
        return view('userarea.applications');
    }

    public function getCandidates() {
//        $candidates = Candidate::all();
        return DataTables::eloquent(Candidate::query())->make(true);
    }

    public function jobs() {
        return view('userarea.jobs');
    }

    public function getJobs() {
        return DataTables::eloquent(Job::query())->make(true);
    }

    public function callRequests() {
        return view('userarea.call_requests');
    }

    public function getCallRequests() {
        return DataTables::eloquent(CallRequest::query())->make(true);
    }

    public function contactEmails() {
        return view('userarea.contact_emails');
    }

    public function getContactEmails() {
        return DataTables::eloquent(ContactForm::query())->make(true);
    }

    public function liveChat() {
        return view('userarea.live_chat');
    }

    public function attendLiveChat(Request $request, $token, $readonly) {

        $conversation = Conversation::where('token', $token)->first();

        if(empty($conversation)) {
            // Conversation not found - severe error.
            return null;
        }
      
        $parameters = [
            'token' => $token,
            'userName' => Auth::user()->first_name,
            'conversationID' => $conversation->id,
            'readOnly' => $readonly
        ];
        return view('userarea.attend_live_chat', $parameters);
//        ->with('token', $token);
    }

    public function getLiveChats() {

        return DataTables::queryBuilder(
                DB::table('conversations')->orderBy('status', 'desc')->orderBy('updated_at', 'asc')
        )->make(true);
    }

    public function deleteCandidate(Request $request) {

        $candidateId = $request->get('candidateId');
        try {
            $deleted = Candidate::destroy($candidateId);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['success' => "Successfully deleted $deleted candidate(s)."]);
    }

    public function deleteJob(Request $request) {

        $jobId = $request->get('jobId');
        try {
            $deleted = Job::destroy($jobId);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['success' => "Successfully deleted $deleted job(s)."]);
    }

    public function deleteDocument(Request $request) {

        $documentId = $request->get('documentId');
        try {
            $deleted = Document::destroy($documentId);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['success' => "Successfully deleted $deleted document(s)."]);
    }

    public function toggleDocumentPublished(Request $request) {

        $documentId = $request->get('documentId');

        try {
            /** @var Document $document */
            $document = Document::find($documentId);
            $published = $document->published;
            if($published) {
                $document->published = 0;
            } else {
                $document->published = 1;
            }
            $document->save();

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['success' => "Successfully toggled the document's published status"]);
    }

    public function deleteCallRequest(Request $request) {

        $callRequestId = $request->get('callRequestId');
        try {
            $deleted = CallRequest::destroy($callRequestId);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['success' => "Successfully deleted $deleted call request(s)."]);
    }

    public function deleteContactEmail(Request $request) {

        $contactEmailId = $request->get('contactEmailId');
        try {
            $deleted = ContactForm::destroy($contactEmailId);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['success' => "Successfully deleted $deleted contact email(s)."]);
    }

    public function userDownload($type, $objectId) {

       switch ($type) {
           case 1:
               /** @var Job $job */
               $job = Job::find($objectId);
               $file = $job->job_description;

               if(!Storage::disk('local')->exists($file)) {
                   return response()->view('errors.404', [], 404);
               }

               $name = "PD-" . $job->company . "-" . $job->position . "." . pathinfo($file, PATHINFO_EXTENSION);
               break;

           case 2:
               /** @var Candidate $candidate */
               $candidate = Candidate::find($objectId);
               $file = $candidate->resume;

               if(!Storage::disk('local')->exists($file)) {
                   return response()->view('errors.404', [], 404);
               }

               $name = "Resume-" . $candidate->first_name . "-" . $candidate->last_name . "." . pathinfo($file, PATHINFO_EXTENSION);
               break;

           default:
               return response()->view('errors.404', [], 404);
       }

        return response()->download($file, $name);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function addDocument() {
        return view('userarea.edit_document');
    }

    public function editDocument(Request $request, $documentId) {

        $document = Document::find($documentId);
        if(empty($document)) {
            abort(404);
        }

        return view('userarea.edit_document', ['document' => $document]);
    }

    public function storeDocument(Request $request) {

        $inputVariables = $request->all();

        $userNotices=[];
//      TODO: Check if recapture is required
//        if(empty($inputVariables['g-recaptcha-response']) ||  !$this->checkRecapture($inputVariables['g-recaptcha-response'])) {
//            $userNotices[] = "Recapture failed";
//        }

        $this->validate($request, [
            'name' => 'required|max:100',
            'description' => 'max:100',
            'link' => 'required|max:100',
            'type' => 'required|max:10',
            'content' => 'required',
            'sort_order' => 'required|integer',
        ]);

        if(!empty($request->get('id'))) {
            $document = Document::find(trim($request->get('id')));
            if(empty($document)) {
                abort(404);
            }
        } else {
            $document = new Document();
            $this->validate($request, [
                'name' => 'unique:documents,name',
                'link' => 'unique:documents,link'
            ]);
        }

        try {
            $document->name = trim($request->get('name'));
            $document->description = trim($request->get('description'));
            $document->content = trim($request->get('content'));
            $document->link = trim($request->get('link'));
            $document->type = trim($request->get('type'));
            $document->published = trim($request->get('published')) === "on" ? true : false;
            $document->sort_order = trim($request->get('sort_order'));
            $document->save();
        } catch(\Exception $e) {
            $userNotices[] = "System error. Please report this error to the technical team.";
            return view('userarea.edit_document', ['userNotices' => $userNotices, 'document' => $document]);
        }

        return redirect('/documents');
    }

    private function checkRecapture($recapture) {

        $secret = config('app.recapture_secret');
        $reCaptureValidation = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $recapture . "&remoteip=".$_SERVER['REMOTE_ADDR']), true);

        if(empty($reCaptureValidation['success'])) {
            return false;
        }

        return true;
    }

    public function viewDocuments() {
        return view('userarea.documents');
    }

    public function getDocuments() {
        return DataTables::eloquent(
            Document::query()->orderBy('sort_order')
        )->make(true);
    }
}
