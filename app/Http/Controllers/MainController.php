<?php

namespace App\Http\Controllers;

use App\Factories\Subscriptions;
use App\Mail\ActivateMail;
use App\Models\Account;
use App\Models\AccountStatus;
use App\Models\Document;
use App\Mail\CallRequestReceived;
use App\Mail\ContactMade;
use App\Mail\GeneralMail;
use App\Mail\JobSubmitted;
use App\Mail\ResumeSubmitted;
use App\Models\CallRequest;
use App\Models\Candidate;
use App\Models\ContactForm;
use App\Models\Conversation;
use App\Models\Invoice;
use App\Models\InvoiceStatus;
use App\Models\Job;
use App\Models\Message;
use App\Models\Product;
use App\Models\ProductStatus;
use App\Models\Service;
use App\Models\ServiceStatus;
use App\Models\ServiceSubscription;
use App\Models\ServiceSubscriptionStatus;
use App\Models\ServiceType;
use App\Models\SlackTeam;
use App\Notifications\LiveChat;
use App\Repositories\Products;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Events\MessagePosted;
use App\Models\User;
use Illuminate\Validation\Rule;
use Mockery\Exception;
use App\Models\ActivationService;

class MainController extends Controller
{

    protected $activationService;
    protected $products;

    /** @var Subscriptions $subscriptionFactory */
    protected $subscriptionFactory;

    public function __construct(ActivationService $activationService, Products $products)
    {
//        $this->middleware('guest', ['except' => 'logout']);
        $this->activationService = $activationService;
        $this->products = $products;
        $this->subscriptionFactory = App::make('App\Factories\Subscriptions');
    }

    public function testAction() {
      
      $email = config('app.alion_email');
      echo "Here is the email\n";
      
      echo $email;
      die;
      
      return view('pages.test_view');
    }
  
    public function index() {
        return view('pages.index');
    }

    public function services() {
        return view('pages.services');
    }

    public function softwareDevelopment() {
        return view('services.software_development');
    }

    public function serviceHosting() {
        return view('services.service_hosting');
    }

    public function consulting() {
        return view('services.consulting');
    }

    public function recruitment() {
        return view('services.recruitment');
    }

    public function processImprovement() {
        return view('services.process_improvement');
    }

    public function infrastructure() {
        return view('services.infrastructure');
    }

    public function developmentEnvironment() {
        return view('services.software_development');
    }

    public function continuousIntegration() {
        return view('services.continuous_integration');
    }

    public function automatedTesting() {
        return view('services.automated_testing');
    }

    public function about() {
        return view('pages.about');
    }

    public function contact() {
        return view('pages.contact');
    }

    public function employersIndex() {
        return view('pages.employers');
    }

    public function candidatesIndex() {
        return view('pages.candidates');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEmail(Request $request) {

        $inputVariables = $this->validateEmailForm($request->all());
//        if($inputVariables === false) {
//            return response()->json(['email_error' => "Please fill all required fields in!"]);
//        }

        $this->validate($request, [
            'emailName' => 'required|max:100',
            'emailFrom' => 'required|email|max:100',
            'emailSubject' => 'required|max:100',
            'emailBody' => 'nullable|max:512',
        ]);

        if(empty($inputVariables['gRecaptureResponse']) ||  !$this->checkRecapture($inputVariables['gRecaptureResponse'])) {
            $this->logAndEmail('Job submission attempt failed due to recapture from IP ' . $request->ip());
            return $this->failReCapture();
        }

        try {
            $contactForm = new ContactForm();
            $contactForm->name = $inputVariables['emailName'];
            $contactForm->email = $inputVariables['emailFrom'];
            $contactForm->subject = $inputVariables['emailSubject'];
            $contactForm->body = $inputVariables['emailBody'];
            $contactForm->source_ip = $request->ip();
            $contactForm->save();
        } catch (\Exception $e) {
            return response()->json(['form_error' => $e->getMessage()]);
        }

        $this->sendContactFormEmails ($contactForm);
        Log::info("Contact form filled in: " . $contactForm->toString());

        $returnArray['form_success'] = "Your email has been sent to Alion Solutions.";

        return response()->json($returnArray);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadJobDescription(Request $request) {

        $inputVariables = $this->validateJobForm($request->all());
        if($inputVariables === false) {
            return response()->json(['form_error' => "Please fill all required fields!"], 422);
        }

        if(!$this->validateEmailAddress( $inputVariables['email'])) {
            return response()->json(['form_error' => "Email address format is invalid."], 422);
        }

        if(!empty($inputVariables['phone']) && !$this->validatePhoneNumber( $inputVariables['phone'])) {
            return response()->json(['form_error' => "Phone number format is invalid."], 422);
        }

        if(empty($inputVariables['g-recaptcha-response']) ||  !$this->checkRecapture($inputVariables['g-recaptcha-response'])) {
            $this->logAndEmail('Job submission attempt failed due to recapture failure from IP ' . $request->ip());
            return $this->failReCapture();
        }

        if($request->hasFile('job_description') && $request->file('job_description')->isValid()) {
            $file = $request->file('job_description');
            $filePath = $file->store('job_descriptions');
            if(empty($filePath)) {
                $this->logAndEmail('Error uploading a job description file. ' . $request->ip());
                return response()->json(['form_error' => "Error uploading the file. Please contact Alion Solutions directly."], 422);
            }
        } else{
            $this->logAndEmail('Error uploading an invalid resume file. ' . $request->ip());
            return response()->json(['form_error' => "There was an error uploading your file"], 422);
        }

        try {
            $job = new Job();
            $job->company = $inputVariables['company'];
            $job->department = !empty($inputVariables['department']) ? $inputVariables['department'] : null ;
            $job->first_name = $inputVariables['first_name'];
            $job->last_name = $inputVariables['last_name'];
            $job->email = $inputVariables['email'];
            $job->phone = $inputVariables['phone'];
            $job->position = !empty($inputVariables['position']) ? $inputVariables['position'] : null;
            $job->type = $inputVariables['type'];
            $job->skills = !empty($inputVariables['skills']) ? $inputVariables['skills'] : null;
            $job->job_description = $filePath;
            $job->message = $inputVariables['message'];
            $job->source_ip = $request->ip();
            $job->save();
        } catch (\Exception $e) {
            $this->logAndEmail('Error uploading a job description: ' . $e->getMessage());
            return response()->json(['form_error' => "\"There was an error submitting your job description. Please try again later or contact Alion directly on info@alion.com.au\""]);
        }

        $this->sendUploadJobEmails($job);
        Log::info('A job description uploaded: ' . $job->toString());

        $returnArray = ['form_success' => "The job description has been uploaded successfully. You will be contacted upon a successful candidate match."];

        return response()->json($returnArray);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadResume(Request $request) {

        $inputVariables = $this->validateResumeForm($request->all());
        if($inputVariables === false) {
            return response()->json(['form_error' => "Please fill all required fields!"], 422);
        }

        if(!$this->validateEmailAddress( $inputVariables['email'])) {
            return response()->json(['form_error' => "Email address format is invalid."], 422);
        }

        if(!$this->validatePhoneNumber( $inputVariables['phone'])) {
            return response()->json(['form_error' => "Phone number format is invalid."], 422);
        }

        if(empty($inputVariables['g-recaptcha-response']) ||  !$this->checkRecapture($inputVariables['g-recaptcha-response'])) {
            $this->logAndEmail('Resume upload failed due to recapture problem from IP ' . $request->ip());
            return $this->failReCapture();
        }

        if($request->hasFile('resume') && $request->file('resume')->isValid()) {
            $file = $request->file('resume');
            $resumeFilePath = $file->store('resumes');
            if(empty($resumeFilePath)) {
                $this->logAndEmail('Error uploading the resume. ' . $request->ip());
                return response()->json(['form_error' => "Error uploading the file. Please contact Alion Solutions directly."], 422);
            }
        } else{
            $this->logAndEmail('Invalid resume file uploaded. ' . $request->ip());
            return response()->json(['form_error' => "There was an error uploading your file"], 422);
        }

        try {
            $candidate = new Candidate();
            $candidate->first_name = $inputVariables['first_name'];
            $candidate->last_name = $inputVariables['last_name'];
            $candidate->email = $inputVariables['email'];
            $candidate->phone = $inputVariables['phone'];
            $candidate->skills = $inputVariables['skills'];
            $candidate->resume = $resumeFilePath;
            $candidate->message = $inputVariables['message'];
            $candidate->source_ip = $request->ip();
            $candidate->save();
        } catch (\Exception $e) {
            $this->logAndEmail('Error creating a candidate: ' . $e->getMessage());
            return response()->json(['form_error' => "There was an error uploading your resume. Please try again later or contact Alion directly on info@alion.com.au."], 422);
        }


        $this->sendUploadResumeEmails($candidate);

        Log::info('Resume uploaded: ' . $candidate->toString());
        $returnArray = ['form_success' => "The resume has successfully uploaded. You will be contacted upon a successful position match"];

        return response()->json($returnArray);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submitCallRequest(Request $request) {

        $inputVariables = $this->validateCallRequestForm($request->all());

        if($inputVariables === false) {
            return response()->json(['form_error' => "You must supply a contact name and at least one of either an email or a phone number."], 422);
        }

        if(!empty($inputVariables['contact_email']) && !$this->validateEmailAddress($inputVariables['contact_email'])) {
            return response()->json(['form_error' => "Email address format is invalid."], 422);
        }

        if(!empty($inputVariables['contact_phone']) && !$this->validatePhoneNumber($inputVariables['contact_phone'])) {
            return response()->json(['form_error' => "Phone number format is invalid."], 422);
        }

        // Return fake success to a bot
        if(!empty(trim($inputVariables['comments']))) {
            $this->logAndEmail("A bot attempted to fill in the form from IP: " . $request->ip());
            return response()->json(['form_success' => "Success submitting a call request"]);
        }

        try {
            $callRequest = new CallRequest();
            $callRequest->name = $inputVariables['contact_name'];
            $callRequest->phone = $inputVariables['contact_phone'];
            $callRequest->email = $inputVariables['contact_email'];
            $callRequest->topic = $inputVariables['contact_topic'];
            $callRequest->source_ip = $request->ip();
            $callRequest->save();
        } catch (\Exception $e) {
            $this->logAndEmail("Error while creating a call request: " . $e->getMessage());
            return response()->json(['form_error' => "Error creating call request. Try again later or contact Alion directly on info@alion.com.au"], 422);
        }

        Log::info("New call request received: " . $callRequest->toString());
        $this->sendCallRequestEmail($callRequest);

        $returnArray = ['form_success' => "Your call request has been submitted."];

        return response()->json($returnArray);
    }

    /**
     * @param Job $job
     */
    private function sendUploadJobEmails(Job $job) {

        $alionEmail = config('app.alion_email');

        $jobSubmitted = new JobSubmitted($job);
        Mail::to($alionEmail)->send($jobSubmitted);

        if(!empty($job->email)) {
            $jobSubmittedForEmployer = new JobSubmitted($job, true);
            Mail::to($job->email)->send($jobSubmittedForEmployer);
        }
    }

    /**
     * @param Candidate $candidate
     */
    private function sendUploadResumeEmails(Candidate $candidate) {

        $alionEmail = config('app.alion_email');

        $resumeSubmitted = new ResumeSubmitted($candidate);
        Mail::to($alionEmail)->send($resumeSubmitted);

        if(!empty($candidate->email)) {
            $resumeSubmittedForCandidate = new ResumeSubmitted($candidate, true);
            Mail::to($candidate->email)->send($resumeSubmittedForCandidate);
        }
    }

    /**
     * @param CallRequest $callRequest
     */
    private function sendCallRequestEmail(CallRequest $callRequest) {

        $alionEmail = config('app.alion_email');
        $callRequestReceived = new CallRequestReceived($callRequest);
        Mail::to($alionEmail)->send($callRequestReceived);
    }

    /**
     * @param ContactForm $contactForm
     */
    private function sendContactFormEmails(ContactForm $contactForm) {

        $alionEmail = config('app.alion_email');

        $contactMade = new ContactMade($contactForm->name, $contactForm->email, $contactForm->subject, $contactForm->body);
        Mail::to($alionEmail)->send($contactMade);
    }

    /**
     * @param $inputVariables
     * @return bool
     */
    private function validateEmailForm($inputVariables) {

        if(!empty($inputVariables['emailName'])) {
            $name = $this->checkInput($inputVariables['emailName']);
            $inputVariables['emailName'] = $name;
        }

        if(!empty($inputVariables['emailFrom'])) {
            $emailFrom = $this->checkInput($inputVariables['emailFrom']);
            $inputVariables['emailFrom'] = $emailFrom;
        }

        if(!empty($inputVariables['emailSubject'])) {
            $subject = $this->checkInput($inputVariables['emailSubject']);
            $inputVariables['emailSubject'] = $subject;
        }

        if(!empty($inputVariables['emailBody'])) {
            $message = $this->checkInput($inputVariables['emailBody']);
            $inputVariables['emailBody'] = $message;
        }

        if(empty($name) || empty($emailFrom) || empty($subject)) {
            return false;
        }

        return $inputVariables;
    }

    /**
     * @param $inputVariables
     * @return bool
     */
    private function validateJobForm($inputVariables) {

        if(!empty($inputVariables['company'])) {
            $company = $this->checkInput($inputVariables['company']);
            $inputVariables['company'] = $company;
        }

        if(!empty($inputVariables['department'])) {
            $department = $this->checkInput($inputVariables['department']);
            $inputVariables['department'] = $department;
        }

        if(!empty($inputVariables['first_name'])) {
            $firstName = $this->checkInput($inputVariables['first_name']);
            $inputVariables['first_name'] = $firstName;
        }

        if(!empty($inputVariables['last_name'])) {
            $lastName = $this->checkInput($inputVariables['last_name']);
            $inputVariables['last_name'] = $lastName;
        }

        if(!empty($inputVariables['position'])) {
            $position = $this->checkInput($inputVariables['position']);
            $inputVariables['position'] = $position;
        }

        if(!empty($inputVariables['email'])) {
            $email = $this->checkInput($inputVariables['email']);
            $inputVariables['email'] = $email;
        }

        if(!empty($inputVariables['skills'])) {
            $skills = $this->checkInput($inputVariables['skills']);
            $inputVariables['skills'] = $skills;
        }

        if(!empty($inputVariables['message'])) {
            $message = $this->checkInput($inputVariables['message']);
            $inputVariables['message'] = $message;
        }

        if(empty($firstName) || empty($lastName) || empty($position) || empty($email)) {
            return false;
        }

        return $inputVariables;
    }

    /**
     * @param $inputVariables
     * @return bool
     */
    private function validateResumeForm($inputVariables) {

        if(!empty($inputVariables['first_name'])) {
            $firstName = $this->checkInput($inputVariables['first_name']);
            $inputVariables['first_name'] = $firstName;
        }

        if(!empty($inputVariables['last_name'])) {
            $lastName = $this->checkInput($inputVariables['last_name']);
            $inputVariables['last_name'] = $lastName;
        }

        if(!empty($inputVariables['email'])) {
            $email = $this->checkInput($inputVariables['email']);
            $inputVariables['email'] = $email;
        }

        if(!empty($inputVariables['phone'])) {
            $phone = $this->checkInput($inputVariables['phone']);
            $inputVariables['phone'] = $phone;
        }

        if(!empty($inputVariables['skills'])) {
            $skills = $this->checkInput($inputVariables['skills']);
            $inputVariables['skills'] = $skills;
        }

        if(!empty($inputVariables['message'])) {
            $message = $this->checkInput($inputVariables['message']);
            $inputVariables['message'] = $message;
        }

        if(empty($firstName) || empty($lastName)) {
            return false;
        }

        return $inputVariables;
    }

    /**
     * @param $inputVariables
     * @return bool
     */
    private function validateCallRequestForm($inputVariables) {

        if(!empty($inputVariables['contact_name'])) {
            $contactName = $this->checkInput($inputVariables['contact_name']);
            $inputVariables['contact_name'] = $contactName;
        }

        if(!empty($inputVariables['contact_email'])) {
            $contactEmail = $this->checkInput($inputVariables['contact_email']);
            $inputVariables['contact_email'] = $contactEmail;
        }

        if(!empty($inputVariables['contact_phone'])) {
            $contactPhone = $this->checkInput($inputVariables['contact_phone']);
            $inputVariables['contact_phone'] = $contactPhone;
        }

        if(empty($contactName) || (empty($contactEmail) && empty($contactPhone))) {
            return false;
        }

        return $inputVariables;
    }



    private function validateEmailAddress($input) {
        if(!empty($input)) {
            return filter_var($input, FILTER_VALIDATE_EMAIL);
        }

        return true;
    }

    private function validatePhoneNumber($input) {
        if(!empty($input)) {
            $phoneRegex = "/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/";
            return preg_match($phoneRegex, $input);
        }

        return true;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    private function failReCapture() {
        $returnArray['form_error'] = "Capture verification failed!";
        return response()->json($returnArray, 500);
    }

    /**
     * @param $data
     * @return string
     */
    private function checkInput($data) {

        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     * @param $message
     */
    private function logAndEmail($message) {
        Log::info($message);
        Mail::to(config('app.alion_email'))->send(new GeneralMail($message));
    }

    private function checkRecapture($recapture) {

        $secret = config('app.recapture_secret');
        $reCaptureValidation = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $recapture . "&remoteip=".$_SERVER['REMOTE_ADDR']), true);

        if(empty($reCaptureValidation['success'])) {
            return false;
        }

        return true;
    }

    /**
     * @param $documentName
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewDocument($documentName) {

        $parameters = [
            'documentName' => 'alion_' . $documentName . '.docx',
            'fileType' => 'docx'
        ];
        return view('pages.view_document', $parameters);
    }

    public function viewLegal($currentDocument = null) {

        $documents = Document::where('published', 1)->where('type', 'Legal')->orderBy('sort_order')->get();

        if(empty($currentDocument) && !empty($documents)) {
            $firstDocument = $documents->first();
            $currentDocument = $firstDocument->link;

        }
        return view('pages.legal', ['documents' => $documents, 'currentDocument' => $currentDocument]);
    }

    public function broadcastAuth(Request $req) {


        $channel = $req->channel_name;

        /** @var User $user */
        $user = User::find(1);
        if(empty($user)) {
            return false;
        }

        $secretKey = config('app.pusher_app_secret');

        $userData = json_encode(["user_id" => $user->id, "user_info" => ["user" => $user]]);

        $signature = hash_hmac('sha256', $req->socket_id . ":" . $channel . ":"  . $userData, $secretKey);

        $authString = config('app.pusher_app_key') . ":" . $signature;

        return json_encode(["auth" => $authString, "channel_data" => $userData]);
    }

    public function startLiveChat($token) {

        $parameters = [
            'token' => $token
        ];
        return view('pages.live_chat', $parameters);
    }

    public function sendFirstChatMessage(Request $request) {

        if(empty(trim($request->get('user_name')))) {
            return response()->json(['error' => "Please provide your name."], 422);
        }

        if(empty(trim($request->get('question')))) {
            return response()->json(['error' => "Please provide a brief description of your question."], 422);
        }

        if(empty(trim($request->session()->get('_token')))) {
            return response()->json(['error' => "Security error"], 422);
        }

        $userName = $request->get('user_name');
        $question = $request->get('question');
        $token = $request->session()->get('_token');

        /** @var Conversation $conversation */
        $conversation = Conversation::where('token', $token)->first();

        try {

            if(empty($conversation)) {
                $conversation = new Conversation();
                $conversation->token =$token;
            }

            $conversation->user_name = $userName;
            $conversation->status = 'Queued';
            $conversation->source_ip = $request->ip();
            $conversation->save();

            $message = new Message();
            $message->message = $question;
            $message->user_name = $userName;
            $message->conversation_id = $conversation->id;
            $message->save();

        } catch (\Exception $e) {
            return response()->json(['error' => "There was an error starting the conversation. Please try again later or contact Alion by email: info@alion.com.au"], 422);
        }

        $slackTeam = new SlackTeam();
        $slackTeam->notify(new LiveChat($token));

        return response()->json(['conversation_id' => $conversation->id]);

    }

    public function closeConversation(Request $request) {
        /** @var Conversation $conversation */
        $conversation = Conversation::find($request->get('conversationID'));
        if(!empty($conversation)) {
            $conversation->status = "Done";
            $conversation->save();
        } else {
            return response()->json(['error' => "Conversation not found."], 422);
        }

        return response()->json(['success' => "Conversation has been closed."]);
    }

    public function sendChatMessage(Request $request) {

        if(empty(trim($request->get('userName')))) {
            return response()->json(['error' => "Username not found"], 422);
        }

        if(empty(trim($request->get('message')))) {
            return response()->json(['error' => "Message body must not be empty"], 422);
        }

        if(empty(trim($request->get('conversationID')))) {
            return response()->json(['error' => "System error"], 422);
        }

        if(empty(trim($request->get('channelToken')))) {
            return response()->json(['error' => "Security error"], 422);
        }

        /** @var User $user */
        $user = Auth::user();

        /** @var Conversation $conversation */
        $conversation = Conversation::find($request->get('conversationID'));

        if(empty($conversation)) {
            return response()->json(['error' => "Conversation not found."], 422);
        }

        if(!empty($user)) {
            if( $conversation->status == "Queued") {
                $conversation->status = "In Progress";
            }
            $conversation->alion_user =  $user->first_name;
            $userName = $user->first_name;
        } else {
            $userName =  $request->get('userName');
        }

        $conversation->save();

        try {
            $message = new Message();
            $message->message = $request->get('message');
            $message->conversation_id = $request->get('conversationID');
            $message->user_name = $userName;
            if (!empty($user)) {
                $message->user_id = $user->id;
            }
            $message->save();
        } catch (Exception $e) {
            return response()->json(['error' => "System error occurred."], 422);
        }


        $token = $request->get('channelToken');

        broadcast(new MessagePosted($message, $userName, $token, $user))->toOthers();

        return response()->json(['Status' => 0]);
    }

    public function getConversationMessages($conversationID) {

        /** @var Conversation $conversation */
        $conversation = Conversation::find($conversationID);
        if(empty($conversation)) {
            return json_encode([]);
        }
        $allMessages = $conversation->messages;

        $conversationMessages = [];
        /** @var Message $message */
        foreach ($allMessages as $message) {
            if(!empty($message->user)) {
                $conversationMessages[] = ['message' => $message->message, 'date' => $message->created_at->formatLocalized('%d %B %Y %H:%m:%S' ), 'user' => $message->user->first_name];
            } else {
                $conversationMessages[] = ['message' => $message->message, 'date' => $message->created_at->formatLocalized('%d %B %Y %H:%m:%S'), 'user' => $message->conversation->user_name];
            }
        }

        return json_encode($conversationMessages);
    }

    public function packages() {

        $args = [
            'basic' => $this->products->getProductByExternalReference('basic'),
            'intermediate' => $this->products->getProductByExternalReference('intermediate'),
            'advanced' => $this->products->getProductByExternalReference('advanced')
        ];
      
        return view('pages.packages', $args);
    }

    /**
     * * We come here from the package selection page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signUp(Request $request) {

        $args = [
            'basic' => $this->products->getProductByExternalReference('basic'),
            'intermediate' => $this->products->getProductByExternalReference('intermediate'),
            'advanced' => $this->products->getProductByExternalReference('advanced'),
            'selectedPackage' => $request->get('package')
        ];

        return view('pages.signup1', $args);
    }


    public function signUpSubmit(Request $request) {

        $this->validate($request, [
            'domain' => 'required|max:255|unique:accounts',
            'package' => 'required|max:100',
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'email' => 'required|max:255|unique:users',
            'username' => 'required|max:100|unique:users',
            'password' => 'required|max:100',
            'confirm_password' => 'same:password',
            'business' => 'max:255',
            'brn' => 'max:255',
            'tc' => 'required', Rule::in(['on'])
        ]);
      
        try {
          DB::beginTransaction();

          /** @var User $user */
          $user = User::create([
              'first_name' => $request->get('first_name'),
              'last_name' => $request->get('last_name'),
              'username' => $request->get('username'),
              'email' => $request->get('email'),
              'password' => bcrypt($request->get('password')),
              'd_password' => encrypt($request->get('password')),
              'is_admin' => 0,
              'active' => 0,
              'tc_accepted' => 1,
          ]);

          /** @var Account $account */
          $account = Account::create([
              'user_id' => $user->id,
              'business_name' => $request->get('business'),
              'abn' => $request->get('brn'),
              'domain' => $request->get('domain'),
              'activated_on' => new \Carbon\Carbon(),
              'account_status_id' => AccountStatus::CREATED
          ]);

          /** @var Product $product */
          $product = $this->products->getProductByExternalReference($request->get('package'));
          if(empty($product)) {
              DB::rollBack();
              abort(522, $request->get('package') . " subscription is unavailable");
          }

          $packageTerm = $product->initial_term;

          $initialInvoice = new Invoice();
          $initialInvoice->invoice_status_id = InvoiceStatus::UNPAID;
          $initialInvoice->term = $packageTerm;
          $initialInvoice->save();

          // Main package subscription
          $serviceSubscription = $this->subscriptionFactory->createInitialSubscription($product, $account, $packageTerm);
          $initialInvoice->addLine($serviceSubscription);

          // Dedicated IP subscription
          if($request->get('dedicated_ip') == "on") {

              $IPProduct = $this->products->getProductByName('IP');
              if(empty($IPProduct)) {
                  DB::rollBack();
                  abort(522, "Dedicated IP subscription is unavailable.");
              }

              $serviceSubscriptionIP = $this->subscriptionFactory->createInitialSubscription($IPProduct, $account, $packageTerm);
              $initialInvoice->addLine($serviceSubscriptionIP);
          }

          // This is for Shell access
          if($request->get('shell_access') == "on") {

              $shellProduct = $this->products->getProductByName('Shell');
              if(empty($shellProduct)) {
                  DB::rollBack();
                  abort(522, "Shell access subscription is unavailable.");
              }

              $serviceSubscriptionShell = $this->subscriptionFactory->createInitialSubscription($shellProduct, $account, $packageTerm);
              $initialInvoice->addLine($serviceSubscriptionShell);
          }

          // This is for Backups
          if($request->get('backups') == "on") {

              $backupProduct = $this->products->getProductByName('Backups');
              if(empty($backupProduct)) {
                  DB::rollBack();
                  abort(522, "Backup subscription is unavailable.");
              }

              $serviceSubscriptionBackup = $this->subscriptionFactory->createInitialSubscription($backupProduct, $account, $packageTerm);
              $initialInvoice->addLine($serviceSubscriptionBackup);
          }

          // This is for SSL Certificate
          if($request->get('ssl_certificate') == "on") {

              $SSLProduct = $this->products->getProductByName('SSL');
              if(empty($SSLProduct)) {
                  DB::rollBack();
                  abort(522, "SSL subscription is unavailable.");
              }

              $serviceSubscriptionSSL = $this->subscriptionFactory->createInitialSubscription($SSLProduct, $account, $packageTerm);
              $initialInvoice->addLine($serviceSubscriptionSSL);
          }

          $account->invoices()->save($initialInvoice);
          $this->activationService->sendActivationMail($user);
        } catch (\Exception $e) {
          DB::rollBack();
          abort(522, "System Error occured.");
        }

        DB::commit();
        return view('pages.signup_success');
    }
}
