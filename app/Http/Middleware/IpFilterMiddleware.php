<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Router as Route;

class IpFilterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowedIPs = [
                '14.201.28.79',
                '192.168.56.1',
                '144.139.89.141',
                '219.89.12.61',
                '166.65.189.100',
                '60.241.76.109',
                '211.26.214.15'
        ];

        $IPFilter = env('IP_FILTER');
        if (!empty($IPFilter) && !in_array($request->ip(), $allowedIPs)) {
            return response()->view('pages.commingsoon');
        }

        return $next($request);
    }
}

