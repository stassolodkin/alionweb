<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 16/8/17
 * Time: 3:12 PM
 */
namespace App\Repositories;



use App\Models\ServiceSubscription;
use App\Models\ServiceSubscriptionStatus;
use Illuminate\Support\Facades\DB;

class ServiceSubscriptions {

    /**
     * @param $name
     * @return ServiceSubscription
     */
    public function getServiceSubscriptionByNameAndInvoice($name, $invoiceId) {

       $serviceSubscription = DB::table('service_subscriptions')
           ->join('services', 'services.id', '=', 'service_subscriptions.service_id')
            ->where('services.name', '=', $name)
            ->where('invoice_id', '=', $invoiceId)
            ->first();

       if(empty($serviceSubscription)) {
           return null;
       }

       return ServiceSubscription::find($serviceSubscription->id);
    }

}