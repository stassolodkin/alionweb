<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 16/8/17
 * Time: 3:12 PM
 */
namespace App\Repositories;



use App\Models\Product;
use App\Models\ProductStatus;
use Illuminate\Support\Facades\DB;

class Products {

    /**
     * @param $name
     * @return Product
     */
    public function getProductByName($name) {

       $product = DB::table('products')
            ->where('name', '=', $name)
            ->where('product_status_id', '=', ProductStatus::ACTIVE)
            ->first();

        return Product::find($product->id);
    }
  
    /**
     * @param $name
     * @return Product
     */
    public function getProductByExternalReference($externalReference) {

       $product = DB::table('products')
            ->where('external_reference', '=', $externalReference)
            ->where('product_status_id', '=', ProductStatus::ACTIVE)
            ->first();

        return Product::find($product->id);
    }

}