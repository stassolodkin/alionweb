<?php

Route::group(['middleware' => 'ipcheck'], function () {

    Route::get('/', 'MainController@index');

    Route::get('services', 'MainController@services');
    Route::get('services/software_development', 'MainController@softwareDevelopment');
    Route::get('services/service_hosting', 'MainController@serviceHosting');
    Route::get('services/consulting', 'MainController@consulting');
    Route::get('services/recruitment', 'MainController@recruitment');
    Route::get('services/development_environment', 'MainController@developmentEnvironment');
    Route::get('services/infrastructure', 'MainController@infrastructure');
    Route::get('services/process_improvement', 'MainController@processImprovement');
    Route::get('services/continuous_integration', 'MainController@continuousIntegration');
    Route::get('services/automated_testing', 'MainController@automatedTesting');

    Route::get('about', 'MainController@about');
    Route::get('contact', 'MainController@contact');
    Route::post('send_email', 'MainController@sendEmail');
    Route::post('upload_resume', 'MainController@uploadResume');
    Route::post('upload_job_description', 'MainController@uploadJobDescription');
    Route::post('submit_call_request', 'MainController@submitCallRequest');

    Route::get('employers',  'MainController@employersIndex');
    Route::get('candidates', 'MainController@candidatesIndex');

    Route::get('start_live_chat/{token}', 'MainController@startLiveChat');
    Route::post('send_first_chat_message', 'MainController@sendFirstChatMessage');
    Route::post('send_chat_message', 'MainController@sendChatMessage');
    Route::get('get_conversation_messages/{conversationID}', 'MainController@getConversationMessages');
    Route::post('close_conversation', 'MainController@closeConversation');

    Route::post('/broadcasting/auth', 'MainController@broadcastAuth');

    Route::get('/view/{documentName}', 'MainController@viewDocument');
    Route::get('/legal/{currentDocument?}', 'MainController@viewLegal');

    Route::get('/packages', 'MainController@packages');
    Route::post('/signup', 'MainController@signUp');
    Route::get('/signup', 'MainController@signUp');

    Route::get('/user/activation/{token}', 'Auth\LoginController@activateUser')->name('user.activate');

    Route::post('/signup_submit', 'MainController@signUpSubmit');
  
    Route::get('/test', 'MainController@testAction');

    Auth::routes();
});

Route::group(['middleware' => ['auth', 'ipcheck', 'admin']], function () {

    Route::get('/applications', 'HomeController@applications');
    Route::get('/get_candidates', 'HomeController@getCandidates');
    Route::post('/delete_candidate', 'HomeController@deleteCandidate');

    Route::get('/jobs', 'HomeController@jobs');
    Route::get('/get_jobs', 'HomeController@getJobs');
    Route::post('/delete_job', 'HomeController@deleteJob');

    Route::get('/call_requests', 'HomeController@callRequests');
    Route::get('/get_call_requests', 'HomeController@getCallRequests');
    Route::post('/delete_call_request', 'HomeController@deleteCallRequest');

    Route::get('/contact_emails', 'HomeController@contactEmails');
    Route::get('/get_contact_emails', 'HomeController@getContactEmails');
    Route::post('/delete_contact_email', 'HomeController@deleteContactEmail');

    Route::get('/live_chat', 'HomeController@liveChat');
    Route::get('/get_live_chats', 'HomeController@getLiveChats');
    Route::get('/attend_live_chat/{token}/{readonly}', 'HomeController@attendLiveChat');

    Route::get('documents', 'HomeController@viewDocuments');
    Route::get('get_documents', 'HomeController@getDocuments');

    Route::get('/user_download/{type}/{job_id}', 'HomeController@userDownload');
    Route::get('/document/add', 'HomeController@addDocument');
    Route::get('/document/edit/{document_id}', 'HomeController@editDocument');
    Route::post('/document/store', 'HomeController@storeDocument');
    Route::post('/delete_document', 'HomeController@deleteDocument');
    Route::post('/toggle_document_published', 'HomeController@toggleDocumentPublished');
});

Route::group(['middleware' => ['auth', 'ipcheck']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/logout', 'HomeController@logout');
  
    Route::get('/cpanel_api_test', 'UserHomeController@testCpanelAPI');

    Route::get('/signup_step_2', 'UserHomeController@signUpStep2');
    Route::post('/signup_final', 'UserHomeController@signUpFinal');
    Route::get('/get_invoice_items/{invoice_id}', 'UserHomeController@getInvoiceItems');

    Route::post('/remove_subscription', 'UserHomeController@removeSubscription');
    Route::post('/add_subscription', 'UserHomeController@addSubscription');

    Route::get('paypal/success','UserHomeController@paypalSuccess');
});