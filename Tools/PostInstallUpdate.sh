#!/bin/bash

mkdir -p `pwd`/storage/app/resumes

if [ ! -L `pwd`/public/resumes ]; then
  ln -s `pwd`/storage/app/resumes `pwd`/public/resumes
fi

mkdir -p `pwd`/storage/app/job_descriptions

if [ ! -L `pwd`/public/job_descriptions ]; then
    ln -s `pwd`/storage/app/job_descriptions `pwd`/public/job_descriptions
fi
