#!/usr/bin/env bash

openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout /var/www/ssl/nginx.key \
    -new \
    -out /var/www/ssl/nginx.crt \
    -subj /CN=alion.app \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat /usr/lib/ssl/openssl.cnf \
        <(printf '[SAN]\nsubjectAltName=DNS:alion.app')) \
    -sha256 \
    -days 3650