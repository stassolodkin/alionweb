## After checking out the code into /var/www/alion

composer install

cp .env.local .env

php artisan key:generate

npm install

npm run dev

Tools/PostInstallUpdate.sh

php artisan migrate

## Install and configure supervisor
###### Install supervisor
sudo apt-get install supervisor

###### Put the following in /etc/supervisor/conf.d/laravel-worker.conf
```
[program:laravel-worker]
 process_name=%(program_name)s_%(process_num)02d
 command=php /var/www/alion/artisan queue:work database --sleep=3 --tries=3
 autostart=true
 autorestart=true
 user=root
 numprocs=1
 redirect_stderr=true
 stdout_logfile=/var/www/alion/storage/logs/worker.log
```

###### Re-read the supervisor config
sudo supervisorctl reread

sudo supervisorctl update

sudo supervisorctl start laravel-worker:*

sudo service supervisor restart

## Use the following in the virtual host config
```
server {

     listen 80;
     listen [::]:80;

     root /var/www/alion/public;
     index index.php index.html index.htm;
     sendfile off;

     server_name alion.app www.alion.app;

     location / {
         try_files $uri $uri/ /index.php?$query_string;
     }

     location ~ \.php$ {
         try_files $uri /index.php =404;
         fastcgi_split_path_info ^(.+\.php)(/.+)$;
         fastcgi_pass 127.0.0.1:9000;
         fastcgi_index index.php;
         fastcgi_read_timeout 600;
         fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
         include fastcgi_params;
     }
 }
```
