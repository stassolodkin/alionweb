const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/chat.js', 'public/js');
mix.js('resources/assets/js/invoice.js', 'public/js');

mix.combine(['resources/assets/js/main.js'], 'public/js/main.js')
    .sass('resources/assets/sass/main.scss', 'public/css');

// mix.combine(['resources/assets/js/libs/cycleSlider.js'], 'public/js/libs/cycleSlider.js')
//     .sass('resources/assets/sass/cycleSlider.scss', 'public/css');

// mix.combine(['resources/assets/js/email.js'], 'public/js/email.js')
mix.sass('resources/assets/sass/email.scss', 'public/css');
mix.sass('resources/assets/sass/packages.scss', 'public/css');

mix.combine(['resources/assets/js/form.js'], 'public/js/form.js');
mix.combine(['resources/assets/js/chat_helpers.js'], 'public/js/chat_helpers.js');

//mix.combine(['resources/assets/js/libs/sup-nav.js'], 'public/js/libs/sup-nav.js')
//    .sass('resources/assets/sass/sup-nav.scss', 'public/css');

mix.sass('resources/assets/sass/menu.scss', 'public/css');
mix.sass('resources/assets/sass/menu2.scss', 'public/css');
mix.sass('resources/assets/sass/loader.scss', 'public/css');
mix.sass('resources/assets/sass/form.scss', 'public/css');

mix.combine([
	'node_modules/normalize.css/normalize.css'
],  'public/css/normalize.css');

mix.combine(['node_modules/bootstrap3-dialog/src/js/bootstrap-dialog.js'], 'public/js/libs/bootstrap-dialog.js');
mix.combine(['node_modules/bootstrap3-dialog/src/css/bootstrap-dialog.css'], 'public/css/bootstrap-dialog.css');

mix.copy('node_modules/tinymce', 'public/js/libs/tinymce', false);

mix.combine(['resources/assets/js/libs/bootstrap-tagsinput.js'], 'public/js/libs/bootstrap-tagsinput.js');
mix.combine(['resources/assets/sass/bootstrap-tagsinput.scss'], 'public/css/bootstrap-tagsinput.css');

// Custom non-css fonts
mix.combine(['resources/assets/fonts/JuliusSansOne-Regular.ttf'], 'public/fonts/JuliusSansOne-Regular.ttf');

mix.combine(['node_modules/slick-carousel/slick/slick.min.js'], 'public/js/libs/slick.min.js');
mix.combine(['node_modules/slick-carousel/slick/slick.css'], 'public/css/slick.css');
mix.combine(['node_modules/slick-carousel/slick/slick-theme.css'], 'public/css/slick-theme.css');
mix.copy('node_modules/slick-carousel/slick/fonts/slick.eot', 'public/css/fonts/slick.eot');
mix.copy('node_modules/slick-carousel/slick/fonts/slick.svg', 'public/css/fonts/slick.svg');
mix.copy('node_modules/slick-carousel/slick/fonts/slick.ttf', 'public/css/fonts/slick.ttf');
mix.copy('node_modules/slick-carousel/slick/fonts/slick.woff', 'public/css/fonts/slick.woff');
mix.copy('node_modules/slick-carousel/slick/ajax-loader.gif', 'public/css/ajax-loader.gif');

