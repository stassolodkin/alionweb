function sendEmail() {

    $('#form_success').hide();
    $('#form_error').hide();

    if(!validateContactForm()) {
        return;
    }

    var emailName            = $('#email_name').val();
    var emailFrom            = $('#email_from').val();
    var emailSubject         = $('#email_subject').val();
    var emailBody            = $('#email_body').val();
    var gRecaptureResponse   =  grecaptcha.getResponse();

    var data = {
        emailName:           emailName,
        emailFrom:           emailFrom,
        emailSubject:        emailSubject,
        emailBody:           emailBody,
        gRecaptureResponse:  gRecaptureResponse
    };

    loader = $(".loader-container-2");
    loader.fadeIn(500);

    $.ajax({
        type: "POST",
        url: "/send_email",
        data: data,
        success: function (data) {
            
            for(var key in data) {

                if(key == 'form_success') {
                    clearContactForm();
                }

                $('#' + key).text(data[key]).fadeIn(1000);

                setTimeout(function() {
                    $('#' + key).fadeOut();
                }, 3000);
            }

            loader.fadeOut(500);
        },
        error: function (data) {
            try {
                ajaxError = JSON.parse(data.responseText);
            } catch(e) {
                ajaxError = data.responseText;
            }

            if(Array.isArray(ajaxError) && ajaxError.includes('form_error')) {
                actualError = ajaxError.form_error;
                $('#form_error').text(actualError).fadeIn(1000);
            } else {
                try {
                    actualError = JSON.stringify(ajaxError, null, 4);

                    var errorList = "<ul style=\"list-style:none;\"><li>" + ajaxError.message + "</li>";
                    for (var key in ajaxError.errors) {
                        errorList = errorList + "<li>" + ajaxError.errors[key] + "</li>";
                    }

                    errorList = errorList + "</ul>";

                    $('#form_error').html(errorList);
                    $('#form_error').fadeIn(1000);

                } catch(e) {
                    actualError = "We are currently experiencing problems with out upload facility. Please try again later.";
                    $('#form_error').text(actualError).fadeIn(1000);
                }
            }

            setTimeout(function() {
                $('#form_error').fadeOut();
            }, 10000);

            loader.fadeOut(500);
        }
    });
}

function clearContactForm() {
    emailName            = $('#email_name');
    emailFrom            = $('#email_from');
    emailSubject         = $('#email_subject');
    emailBody            = $('#email_body');

    emailName.val('');
    emailFrom.val('');
    emailSubject.val('');
    emailBody.val('');
}

$('#request_call_modal').on('hidden.bs.modal', function () {
    clearCallRequestForm();
});

function submitCallRequest() {

	$('#request_call_form_success').hide();
	$('#request_call_form_error').hide();

	if(!validateCallRequestForm()) {
		return;
	}

	formData = new FormData($('#call_request_form')[0]);

    loader = $(".loader-container-2");
    loader.fadeIn(500);

	if (formData) {
		$.ajax({
			url: "/submit_call_request",
			type: "POST",
			data: formData,
			processData: false,
			contentType: false,
			success: function (data) {

                clearCallRequestForm();
				for(var key in data) {
					$('#request_call_form_success').text(data[key]).fadeIn(1000);
				}

				loader.fadeOut(500);
                setTimeout(function() {
                    $('#request_call_modal').modal('hide');
                }, 3000);

			},
			error: function (data) {
                try {
                    ajaxError = JSON.parse(data.responseText);
                    errorList = "<ul style=\"list-style:none;\">";
                    for (var key in ajaxError) {
                        errorList = errorList + "<li>" + ajaxError[key] + "</li>";
                    }
                    errorList = errorList + "</ul>";
                    $('#request_call_form_error').html(errorList);
                } catch(e) {
                    $('#request_call_form_error').text("An error occurred! Try again later or contact Alion directly on info@alion.com.au");
                }

                $('#request_call_form_error').fadeIn(1000);
                loader.fadeOut(500);
                setTimeout(function() {
                    $('#request_call_form_error').fadeOut();
                }, 10000);

			}
		});
	}
}

function clearCallRequestForm() {

    contactName    = $('#contact_name');
    contactEmail   = $('#contact_email');
    contactPhone   = $('#contact_phone');
    contactTopic   = $('#contact_topic');
    comments       = $('#comments');

    contactName.removeClass('error_box');
    contactEmail.removeClass('error_box');
    contactPhone.removeClass('error_box');
    contactTopic.removeClass('error_box');

    errorMessage   = $('#request_call_form_error');
    successMessage = $('#request_call_form_success');

    contactName.val('');
    contactEmail.val('');
    contactPhone.val('');
    contactTopic.val('');
    comments.val('');

    errorMessage.text('');
    successMessage.text('');

    errorMessage.hide();
    successMessage.hide();
}

function uploadJobDescription() {

    $('#form_success').hide();
    $('#form_error').hide();

    if(!validateJobForm()) {
        return;
    }

    formData = new FormData($('#job_form')[0]);
    var skills = $('#alion-tags').tagsinput('items');
    formData.append("skills", skills);

    loader = $(".loader-container-2");
    loader.fadeIn(500);

    if (formData) {
        $.ajax({
            url: "/upload_job_description",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {

                clearJobForm();
                var successMessage =  $('#form_success');

                for(var key in data) {
                    successMessage.text(data[key]).fadeIn(1000);
                }

                loader.fadeOut(500);
                successMessage.fadeIn();
                setTimeout(function() {
                    successMessage.fadeOut();
                }, 4000);

            },
            error: function (data) {

                var errorDiv =  $('#form_error');
                try {
                    ajaxError = JSON.parse(data.responseText);
                    errorList = "<ul style=\"list-style:none;\">";
                    for (var key in ajaxError) {
                        errorList = errorList + "<li>" + ajaxError[key] + "</li>";
                    }
                    errorList = errorList + "</ul>";
                    errorDiv.html(errorList);
                } catch(e) {

                    try {
                        if(!data.responseText.includes("recipient address was suppressed due to system")) {
                            errorDiv.text("An error occurred! Try again later or contact Alion directly on info@alion.com.au");
                        } else {
                            clearResumeUploadForm();
                            $('#form_success').text("The job has been submitted successfully.").fadeIn(1000);
                        }
                    } catch (e2) {
                        errorDiv.text("An error occurred! Try again later or contact Alion directly on info@alion.com.au");
                    }
                }

                errorDiv.fadeIn(1000);
                loader.fadeOut(500);
                setTimeout(function() {
                    $('#form_error').fadeOut();
                }, 10000);
            }
        });
    }

    grecaptcha.reset();
}

function clearJobForm() {
    company                = $('#company');
    department             = $('#department');
    firstName              = $('#first_name');
    lastName               = $('#last_name');
    email                  = $('#email');
    phone                  = $('#phone');
    position               = $('#position');
    type                   = $('#type');
    message                = $('#message');
    skills                 = $('#alion-tags');
    jobDescription         = $('#job_description');
    jobDescriptionFileName = $('#job_description_file_name');

    company.removeClass('error_box');
    firstName.removeClass('error_box');
    lastName.removeClass('error_box');
    email.removeClass('error_box');
    phone.removeClass('error_box');
    position.removeClass('error_box');
    jobDescription.removeClass('error_box');

    errorMessage   = $('#form_error');
    successMessage = $('#form_success');

    company.val('');
    department.val('');
    firstName.val('');
    lastName.val('');
    email.val('');
    phone.val('');
    position.val('');
    type.val('Full-time');
    message.val('');
    skills.tagsinput('removeAll');
    jobDescription.val('');
    jobDescriptionFileName.val('');

    errorMessage.text('');
    successMessage.text('');

    errorMessage.hide();
    successMessage.hide();
}

function signUpSubmit() {

    var formError = $('#form_error');
    formError.hide();

    var gRecaptureResponse = grecaptcha.getResponse();

    if (gRecaptureResponse === "") {
        formError.text('Please take a moment to verify that you are a real person.').fadeIn(1000);
        setTimeout(function () {
            formError.fadeOut();
        }, 4000);
    } else {
        document.forms["signup_form"].submit();
    }
}

function uploadResume() {

    $('#form_success').hide();
    $('#form_error').hide();

    if(!validateResumeForm()) {
        return;
    }

    formData = new FormData($('#resume_form')[0]);
    var skills = $('#alion-tags').tagsinput('items');
	formData.append("skills", skills);

    loader = $(".loader-container-2");
    loader.fadeIn(500);

    if (formData) {
        $.ajax({
            url: "/upload_resume",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {

                clearResumeUploadForm();
                var successMessage =  $('#form_success');

                for(var key in data) {
                    successMessage.text(data[key]).fadeIn(1000);
                }

                loader.fadeOut(500);
                successMessage.fadeIn();
                setTimeout(function() {
                    successMessage.fadeOut();
                }, 4000);

            },
            error: function (data) {

                var errorDiv =  $('#form_error');
                try {
                    ajaxError = JSON.parse(data.responseText);
                    errorList = "<ul style=\"list-style:none;\">";
                    for (var key in ajaxError) {
                        errorList = errorList + "<li>" + ajaxError[key] + "</li>";
                    }
                    errorList = errorList + "</ul>";
                    errorDiv.html(errorList);
                } catch(e) {

                    try {
                        if(!data.responseText.includes("recipient address was suppressed due to system")) {
                            errorDiv.text("An error occurred! Try again later or contact Alion directly on info@alion.com.au");
                        } else {
                            clearResumeUploadForm();
                            $('#form_success').text("Resume has been uploaded successfully.").fadeIn(1000);
                        }
                    } catch (e2) {
                        errorDiv.text("An error occurred! Try again later or contact Alion directly on info@alion.com.au");
                    }

                }

                errorDiv.fadeIn(1000);
                loader.fadeOut(500);
                setTimeout(function() {
                    $('#form_error').fadeOut();
                }, 10000);
            }
        });
    }

    grecaptcha.reset();
}

function clearResumeUploadForm() {

    firstName      = $('#first_name');
    lastName       = $('#last_name');
    email          = $('#email');
    phone          = $('#phone');
    resume         = $('#resume');
    resumeFileName = $('#resume_file_name');
    skills         = $('#alion-tags');
    message        = $('#message');

    firstName.removeClass('error_box');
    lastName.removeClass('error_box');
    email.removeClass('error_box');
    phone.removeClass('error_box');
    resumeFileName.removeClass('error_box');

    errorMessage   = $('#form_error');
    successMessage = $('#form_success');

    firstName.val('');
    lastName.val('');
    email.val('');
    phone.val('');
    resume.val('');
    resumeFileName.val('');
    skills.tagsinput('removeAll');
    message.val('');

    errorMessage.text('');
    successMessage.text('');

    errorMessage.hide();
    successMessage.hide();
}

function validateResumeForm() {

    firstName      = $('#first_name');
    lastName       = $('#last_name');
    email          = $('#email');
    phone          = $('#phone');
    resume         = $('#resume');
    resumeFileName = $('#resume_file_name');

    // Reset errors
    firstName.removeClass('error_box');
    lastName.removeClass('error_box');
    email.removeClass('error_box');
    phone.removeClass('error_box');
    resumeFileName.removeClass('error_box');

    var gRecaptureResponse = grecaptcha.getResponse();

    if(gRecaptureResponse == "") {
        $('#form_error').text('Please take a moment to verify that you are a real person.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(firstName.val().trim() === "") {
        firstName.addClass('error_box');
        $('#form_error').text('First name must be supplied.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(lastName.val().trim() === "") {
        lastName.addClass('error_box');
        $('#form_error').text('Last name must be supplied.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(email.val().trim() === "") {
        email.addClass('error_box');
        phone.addClass('error_box');
        $('#form_error').text('Email must be supplied.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(!validateEmail(email.val().trim())) {
        email.addClass('error_box');
        $('#form_error').text('The email address is invalid.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }



    if(phone.val().trim() !== "" && !validateAustralianPhoneNumber(phone.val().trim())) {
        phone.addClass('error_box');
        $('#form_error').text('The phone number must be a valid Australian number.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    // Check a resume has been selected
    var resumeFile = resume[0].files[0];
    if(resumeFile === undefined) {
        resumeFileName.addClass('error_box');
        $('#form_error').text('No resume has been selected.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    // Check file type of the resume
    if (!!resumeFile.type.match(/wordprocessing.*/) && !!resumeFile.type.match(/application\/pdf.*/) && !!jobDescriptionFile.type.match(/application\/msword.*/)) {
        resumeFileName.addClass('error_box');
        $('#form_error').text('This file type is not allowed.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    // Check file size of the resume
    if( resumeFile.size > 2097152) {
        resumeFileName.addClass('error_box');
        $('#form_error').text('The file size is too big.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    // Check if FormData is supported
    if(!window.FormData) {
        $('#form_error').text('The browser you are using is too old. Please use a newer version.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    return true;
}

function validateJobForm() {

    company                = $('#company');
    firstName              = $('#first_name');
    lastName               = $('#last_name');
    email                  = $('#email');
    phone                  = $('#phone');
    position               = $('#position');
    jobDescription         = $('#job_description');
    jobDescriptionFileName = $('#job_description_file_name');

    company.removeClass('error_box');
    firstName.removeClass('error_box');
    lastName.removeClass('error_box');
    email.removeClass('error_box');
    phone.removeClass('error_box');
    position.removeClass('error_box');
    jobDescription.removeClass('error_box');

    var gRecaptureResponse = grecaptcha.getResponse();

    if(gRecaptureResponse === "") {
        $('#form_error').text('Please take a moment to verify that you are a real person.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(company.val().trim() === "") {
        company.addClass('error_box');
        $('#form_error').text('Company name must be supplied.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(firstName.val().trim() === "") {
        firstName.addClass('error_box');
        $('#form_error').text('First name must be supplied.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(lastName.val().trim() === "") {
        lastName.addClass('error_box');
        $('#form_error').text('Last name must be supplied.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(email.val().trim() === "") {
        email.addClass('error_box');
        $('#form_error').text('The email address is required.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(email.val().trim() !== "" && !validateEmail(email.val().trim())) {
        email.addClass('error_box');
        $('#form_error').text('The email address is invalid.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(phone.val().trim() !== "" && !validateAustralianPhoneNumber(phone.val())) {
        phone.addClass('error_box');
        $('#form_error').text('The phone number must be a valid Australian number.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    if(position.val().trim() === "") {
        position.addClass('error_box');
        $('#form_error').text('The position is required.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    // Check a resume has been selected
    var jobDescriptionFile = jobDescription[0].files[0];
    if(jobDescriptionFile === undefined) {
        jobDescriptionFileName.addClass('error_box');
        $('#form_error').text('No job description has been selected.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    // Check file type of the resume
    if (!!jobDescriptionFile.type.match(/wordprocessing.*/) && !!jobDescriptionFile.type.match(/application\/pdf.*/) && !!jobDescriptionFile.type.match(/application\/msword.*/)) {
        jobDescriptionFileName.addClass('error_box');
        $('#form_error').text('This file type is not allowed.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    // Check file size of the resume
    if( jobDescriptionFile.size > 2097152) {
        jobDescriptionFileName.addClass('error_box');
        $('#form_error').text('The file size must be up to 2MB.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    // Check if FormData is supported
    if(!window.FormData) {
        $('#form_error').text('The browser you are using is too old. Please use a newer version.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 4000);
        return false;
    }

    return true;
}

function validateCallRequestForm() {

	var contactName    = $('#contact_name');
	var contactEmail   = $('#contact_email');
	var contactPhone   = $('#contact_phone');
	var contactTopic   = $('#contact_topic');
    var comments       = $('#comments');

	// Reset errors
	contactName.removeClass('error_box');
	contactEmail.removeClass('error_box');
	contactPhone.removeClass('error_box');
	contactTopic.removeClass('error_box');

	if(contactName.val().trim() === "") {
		contactName.addClass('error_box');
		$('#request_call_form_error').text('Contact name must be supplied.').fadeIn(1000);
		setTimeout(function() {
			$('request_call_form_error').fadeOut();
		}, 4000);
		return false;
	}

    if(contactPhone.val().trim() === "") {
        contactPhone.addClass('error_box');
        $('#request_call_form_error').text('Phone number name must be supplied.').fadeIn(1000);
        setTimeout(function() {
            $('request_call_form_error').fadeOut();
        }, 4000);
        return false;
    }

	if(contactEmail.val() !== "" && !validateEmail(contactEmail.val())) {
		contactEmail.addClass('error_box');
		$('#request_call_form_error').text('The email address is invalid.').fadeIn(1000);
		setTimeout(function() {
			$('request_call_form_error').fadeOut();
		}, 4000);
		return false;
	}

	if(contactPhone.val() !== "" && !validateAustralianPhoneNumber(contactPhone.val())) {
		contactPhone.addClass('error_box');
		$('#request_call_form_error').text('The phone number must be a valid Australian number.').fadeIn(1000);
		setTimeout(function() {
			$('request_call_form_error').fadeOut();
		}, 4000);
		return false;
	}

	// Check if FormData is supported
	if(!window.FormData) {
		$('#frequest_call_form_error').text('The browser you are using is too old. Please use a newer version.').fadeIn(1000);
		setTimeout(function() {
			$('request_call_form_error').fadeOut();
		}, 4000);
		return false;
	}

	return true;
}

function validateContactForm() {

    var emailName            = $('#email_name');
    var emailFrom            = $('#email_from');
    var emailSubject         = $('#email_subject');

    // Reset errors
    emailName.removeClass('error_box');
    emailFrom.removeClass('error_box');
    emailSubject.removeClass('error_box');

    var gRecaptureResponse = grecaptcha.getResponse();

    if(gRecaptureResponse === "") {
    	$('#form_error').text('Please take a moment to verify that you are a real person.').fadeIn(1000);
    	setTimeout(function() {
    		$('form_error').fadeOut();
    	}, 3000);
    	return false;
    }

    if(emailName.val().trim() === "") {
        emailName.addClass('error_box');
        $('#form_error').text('Contact name must be supplied.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 3000);
        return false;
    }

    if(emailFrom.val().trim() === "") {
        emailFrom.addClass('error_box');
        $('#form_error').text('The email address is required.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 3000);
        return false;
    }

    if(emailFrom.val().trim() !== "" && !validateEmail(emailFrom.val())) {
        emailFrom.addClass('error_box');
        $('#form_error').text('The email address is invalid.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 3000);
        return false;
    }

    if(emailSubject.val().trim() === "") {
        emailSubject.addClass('error_box');
        $('#form_error').text('Subject must be filled in.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 3000);
        return false;
    }

    // Check if FormData is supported
    if(!window.FormData) {
        $('#form_error').text('The browser you are using is too old. Please use a newer version.').fadeIn(1000);
        setTimeout(function() {
            $('form_error').fadeOut();
        }, 3000);
        return false;
    }

    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateAustralianPhoneNumber(number) {
    var phoneRe = /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/;
    return phoneRe.test(number);
}

$('#package').on('change', function() {
    if(this.value === 'advanced') {
        $('#backups').hide();
        $('#dedicated_ip').hide();
    } else {
        $('#backups').show();
        $('#dedicated_ip').show();
    }
});

$(document).ready( function() {
    if($('#package').val() === 'advanced') {
        $('#backups').hide();
        $('#dedicated_ip').hide();
    }
});

$(document).ready(function() {
    $('input[type=radio][name=payment_option]').change(function() {
        if (this.value === 'credit_card') {
            $('.paypal_form').slideUp(500, function() {
                $('.credit_card_form').slideDown(500);
            });


        }
        else if (this.value === 'paypal') {
            $('.credit_card_form').slideUp(500, function () {
                $('.paypal_form').slideDown(500);
            });
        }
    });
});
