

Vue.component('invoice-item', require('./components/InvoiceItem.vue'));
Vue.component('invoice-table', require('./components/InvoiceTable.vue'));

// import VeeValidate from 'vee-validate';
// Vue.use(VeeValidate);

const invoice = new Vue({
    el: '#invoice',
    data: {
        items: [],
        addOns: [],
        invoiceId: 0,
        ipSubscription: 0,
        shellSubscription: 0,
        backupSubscription: 0,
        sslSubscription: 0,
        cardNumber: ''
    },
    methods: {
        addOnClick(addOn) {

            let newValue = this.addOns[addOn];
            let productName = '';
            let subscriptionId = 0;

            switch(addOn) {

                case 'shell_access':

                    productName = 'Shell';
                    subscriptionId = this.shellSubscription;
                    break;

                case 'dedicated_ip':

                    productName = 'IP';
                    subscriptionId = this.ipSubscription;

                    break;
                case 'backups':

                    productName = 'Backups';
                    subscriptionId = this.backupSubscription;

                    break;
                case 'ssl_certificate':

                    productName = 'SSL';
                    subscriptionId = this.sslSubscription;

                    break;
                default:
            }

            if(newValue === true) {
                addSubscription(this, productName, subscriptionId);
            } else {
                removeSubscription(this, subscriptionId);
            }

        },
        removeAddOn(data) {
            let addOnElementName = getElementName(data.productName);
            this.addOns[addOnElementName] = false;
            removeSubscription(this, data.subscriptionId);
        },
        submitForm() {
            // debugger;

            let payBy = $( 'input[name=payment_option]:checked' ).val();
			if(payBy === "credit_card") {

				let cardNumberElement  = $('input[name=cardNumber]');
				let cardHolderElement  = $('input[name=cardHolder]');
				let expiryMonthElement = $('input[name=expiryMonth]');
				let expiryYearElement  = $('input[name=expiryYear]');
				let cvvCodeElement     = $('input[name=cvvCode]');

				cardNumberElement.removeClass('error_box');
				cardHolderElement.removeClass('error_box');
				expiryMonthElement.removeClass('error_box');
				expiryYearElement.removeClass('error_box');
				cvvCodeElement.removeClass('error_box');

				let cardNumber = cardNumberElement.val().replace(/\s+/g, '');

				if(!validCardNumber(cardNumber)) {
					cardNumberElement.addClass('error_box');
					return false;
				}

				let cardHolder = cardHolderElement.val();

				if(cardHolder.replace(/\s+/g, '') === "") {
					cardHolderElement.addClass('error_box');
					return false;
				}

				let expiryMonth = expiryMonthElement.val();
				let expiryYear = expiryYearElement.val();

				if(!validExpiryDate(expiryMonth, expiryYear)) {
					expiryMonthElement.addClass('error_box');
					expiryYearElement.addClass('error_box');
					return false;
				}

				let cvvCode = cvvCodeElement.val();
				if(!validCvv(cvvCode)) {
					cvvCodeElement.addClass('error_box');
					return false;
				}
			}

            document.getElementById("payment_form").submit();
        }
    },
    computed: {
        invoiceTotal: function(){

            let totalPrice = 0;

            for (let i = 0; i < this.items.length; i++) {
                totalPrice += this.items[i].term_months * this.items[i].monthly_rate;
            }

            return totalPrice;
        }
    },
    created() {
        getItems(this);
    }
});


function addSubscription(element, productName, subscriptionId) {

    axios.post('/add_subscription', {
        invoiceId: element.invoiceId,
        productName: productName,
        currentSubscription: subscriptionId
    })
    .then(function (response) {
        setAddOns(element, response.data);
    })
    .catch(function (error) {
        alert(error.response.data.error);
    });
}

function removeSubscription(element, subscriptionId) {

    axios.post('/remove_subscription', {
        invoiceId: element.invoiceId,
        currentSubscription: subscriptionId
    })
    .then(function (response) {
        setAddOns(element, response.data);
    })
    .catch(function (error) {
        alert(error.response.data.error);
    });
}
/**
 *
 * @param element
 */
function getItems(element) {

    let invoiceId = $('meta[name="invoice-id"]').attr('content');

    if(invoiceId === undefined) {
    	return;
	}

    axios.get('/get_invoice_items/' + invoiceId)
        .then(function (response) {
            setAddOns(element, response.data);
        })
        .catch(function (error) {
            alert(error.response.data.error);
        });
}

/**
 *
 * @param element
 * @param data
 */
function setAddOns(element, data) {

    if(typeof data['items'] !== 'undefined') {

        $.each(data['items'], function(k, v) {
            updateAddOnElements(v, element);
        });

        element.items = data['items'];
        element.invoiceId = data['invoice_id'];

    } else {
        alert("System Error - please contact Alion support at support@alion.com.au");
    }
}

/**
 *
 * @param data
 * @param element
 */
function updateAddOnElements(data, element) {

    switch(data['name']) {
        case 'Shell':
            element.addOns['shell_access'] = true;
            element.shellSubscription = data['subscription_id'];
            break;
        case 'IP':
            element.addOns['dedicated_ip'] = true;
            element.ipSubscription = data['subscription_id'];
            break;
        case 'Backups':
            element.addOns['backups'] = true;
            element.backupSubscription = data['subscription_id'];
            break;
        case 'SSL':
            element.addOns['ssl_certificate'] = true;
            element.sslSubscription = data['subscription_id'];
            break;
        default:
    }
}

function getElementName(addOnName) {
    switch(addOnName) {
        case 'Shell':
           return 'shell_access';
        case 'IP':
            return 'dedicated_ip';
        case 'Backups':
            return 'backups';
        case 'SSL':
            return 'ssl_certificate';
        default:
            return "";
    }
}

function validCardNumber(cardNumber) {

	let visa = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
	let master = /^(?:5[1-5][0-9]{14})$/;
	let amex = /^(?:3[47][0-9]{13})$/;
	// let discover = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;
	// let diners = /^(?:3(?:0[0-5]|[68][0-9])[0-9]{11})$/;

	return cardNumber.match(amex) ||
		   cardNumber.match(visa) ||
		   cardNumber.match(master);
		// || cardNumber.match(discover) ||
		//    cardNumber.match(diners);
}

function validExpiryDate(expiryMonth, expiryYear) {

	let d = new Date();
	let currentYear = d.getFullYear();
	let currentMonth = d.getMonth() + 1;

	let year = parseInt(expiryYear);
	if(year < 2000) year += 2000;
	let month = parseInt(expiryMonth);

	return (month >= 1 && month <= 12) && (year >= currentYear || (year === currentYear && month >= currentMonth));
}

function validCvv(cvv){

	let myRe = /^[0-9]{3,4}$/;

	return cvv.match(myRe);
}