function closeConversation (conversationID) {

    loader = $(".loader-container-2");
    loader.fadeIn(500);

    $.ajax({
        type: "POST",
        url: "/close_conversation",
        data: {conversationID: conversationID},
        success: function (data) {

            loader.fadeOut(500);
            location.reload();
        },
        error: function (data) {
            var ajaxError = JSON.parse(data.responseText);
            alert(ajaxError.error);
            // $('#table_error').text(ajaxError.error).fadeIn(1000);
            loader.fadeOut(500);
        }
    });
}