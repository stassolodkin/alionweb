
$(document).ready(function() {

    $('textarea').maxlength({
        alwaysShow: false,
        threshold: 50,
        warningClass: "label label-success",
        limitReachedClass: "label label-danger",
        separator: ' out of ',
        preText: 'Used ',
        postText: ' chars.',
        validate: true
    });

	$('#alion-tags').tagsinput({
		tagClass: 'alion_tags',
		maxTags: 5,
		confirmKeys: [13, 32, 44]
	});

    $('#live-chat-table').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            // pageLength: 2,
            paging: false,
            ajax: '/get_live_chats',
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return row['user_name'];
                    },
                    "targets": 0
                },
                {
                    "render": function ( data, type, row ) {
                        return row['source_ip'];
                    },
                    "targets": 1
                },
                {
                    "render": function ( data, type, row ) {
                        return row['token'];
                    },
                    "targets": 2
                },
                {
                    "render": function ( data, type, row ) {

                        if(row['status'] === "Queued") {
                            return "<i style=\"color:darkred;\" class=\"fa fa-spinner fa-spin\" aria-hidden=\"true\"></i>";
                        } else if(row['status'] === "In Progress") {
                            return "<i style=\"color:green;\" class=\"fa fa-comments-o blink\" aria-hidden=\"true\"></i>";
                        } else {
                            return "<i style=\"color:grey;\" class=\"fa fa-check\" aria-hidden=\"true\"></i>";
                        }
                    },
                    "targets": 3,
                    "className": "dt-center",
                },
                {
                    "render": function ( data, type, row ) {
                        return row['alion_user'];
                    },
                    "targets": 4
                },
                {
                    "render": function ( data, type, row ) {
                        submitionDate = new Date(row['created_at']);
                        return submitionDate.getDate() + '/' + submitionDate.getMonth() + '/' + submitionDate.getFullYear() + '&nbsp;' + submitionDate.getHours() + ':' + submitionDate.getMinutes();
                    },
                    "targets": 5
                },
                {
                    "render": function ( data, type, row ) {

                        if(row['status'] === "Queued") {

                            return "<a href='javascript:void(0)' " +
                                "onclick=\"window.open('/attend_live_chat/" + row['token'] + "/0'" +
                                ", '_blank', 'location=no,height=575,width=520,resizable=no,scrollbars=no,status=yes');\"" +
                                "><i style=\"color:#5a69a6;\" class=\"fa fa-users\" aria-hidden=\"true\"></i></a>";
                        } else  if(row['status'] === "In Progress") {

                            return "<a href='javascript:void(0)' " +
                                "onclick=\"window.open('/attend_live_chat/" + row['token'] + "/0'" +
                                ", '_blank', 'height=575,width=520');\"" +
                                "><i style=\"color:#5a69a6;\" class=\"fa fa-users\" aria-hidden=\"true\"></i></a>" +
                                "<a href='javascript:void(0)' onclick=\"closeConversation(" + row['id'] + ")\">" +
                                "<i style=\"color:darkred; padding-left:8px;\" class=\"fa fa-close\" aria-hidden=\"true\"></i></a>";

                        } else {
                            return "<a href='javascript:void(0)' " +
                                "onclick=\"window.open('/attend_live_chat/" + row['token'] + "/1'" +
                                ", '_blank', 'location=no,height=575,width=520,resizable=no,scrollbars=no,status=yes');\"" +
                                "><i style=\"color:#5a69a6;\" class=\"fa fa-list-alt\" aria-hidden=\"true\"></i></a>";
                        }
                    },
                    "className": "dt-center",
                    "targets": 6
                }
            ]
        }
    );

    $('#applications-table').DataTable(
		{
			processing: true,
			serverSide: true,
			responsive: true,
			ajax: '/get_candidates',
			"columnDefs": [
				{
					"render": function ( data, type, row ) {
						return row['first_name']+' '+row['last_name'];
					},
					"targets": 0
				},
				{
					"render": function ( data, type, row ) {
						return "<a href='mailto:" + row['email'] + "'>" + row['email'] + "</a>";
					},
					"targets": 1
				},
				{
					"render": function ( data, type, row ) {
						return "<a href='tel:" + row['phone'] + "'>" + row['phone'] + "</a>";
					},
					"targets": 2
				},
				{
					"render": function ( data, type, row ) {
						return row['skills'];
					},
					"targets": 3
				},
				{
					"render": function ( data, type, row ) {
						// return "<a href='" + row['resume'] + "'><i class=\"fa fa-download\" aria-hidden=\"true\"></i></a>";
                        return "<a href='/user_download/2/" + row['id'] + "'><i class=\"fa fa-download\" aria-hidden=\"true\"></i></a>";
					},
                    "className": "dt-center",
					"targets": 4
				},
				{
					"render": function ( data, type, row ) {
						submitionDate = new Date(row['created_at']);
						return submitionDate.getDate() + '/' + submitionDate.getMonth() + '/' + submitionDate.getFullYear();
					},
					"targets": 5
				},
                {
                    "render": function ( data, type, row ) {
                        return row['source_ip'];
                    },
                    "targets": 6
                },
                {
                    "render": function ( data, type, row ) {
                        return "<a href='javascript:void(0)' onclick=\"deleteCandidate(" + row['id'] + ")\"><i style=\"color:red;\" class=\"fa fa-close\" aria-hidden=\"true\"></i></a>";
                    },
                    "className": "dt-center",
                    "targets": 7
                }
			]
		}
	);

    $('#document-table').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '/get_documents',
            'fnDrawCallback': function (oSettings) {
                var addDocumentButton = document.getElementById("add-document");
                if(addDocumentButton === null) {
                    $('.dataTables_filter').append('<a id="add-document" href="/document/add" class="buttonlink add_table_button">Add Document</a>');
                }
            },
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return "<a href=\"legal/" + row['link'] + "\">" + row['name'] + "</a>";
                    },
                    "targets": 0
                },
                {
                    "render": function ( data, type, row ) {
                        return row['description'];
                    },
                    "targets": 1
                },
                {
                    "render": function ( data, type, row ) {
                        return row['link'];
                    },
                    "targets": 2
                },
                {
                    "render": function ( data, type, row ) {
                        return row['type'];
                    },
                    "targets": 3
                },
                {
                    "render": function ( data, type, row ) {
                        return row['sort_order'];
                    },
                    "className": "dt-center",
                    "targets": 4
                },
                {
                    "render": function ( data, type, row ) {

                        var html = "<a href='javascript:void(0)' onclick=\"toggleDocumentPublished(" + row['id'] + ")\">";

                        if( row['published'] === "1" ) {
                            html += "<i style=\"color:#9cb128;\" class=\"fa fa-toggle-on\" aria-hidden=\"true\"></i>";
                        } else {
                            html += "<i style=\"color:darkred;\" class=\"fa fa-toggle-off\" aria-hidden=\"true\"></i>";
                        }

                        html += "</a>";
                        return html;

                    },
                    "className": "dt-center",
                    "targets": 5
                },
                {
                    "render": function ( data, type, row ) {
                        updateDate = new Date(row['updated_at']);
                        return updateDate.getDate() + '/' + updateDate.getMonth() + '/' + updateDate.getFullYear();
                    },
                    "targets": 6
                },
                {
                    "render": function ( data, type, row ) {
                        var html = "<a href='javascript:void(0)' onclick=\"deleteDocument(" + row['id'] + ")\"><i style=\"color:darkred; margin-right: 5px;\" class=\"fa fa-close\" aria-hidden=\"true\"></i></a>";

                        html += "<a href=\"/document/edit/" + row['id'] + "\"><i style=\"color:#5a69a6; margin-right: 5px;\" class=\"fa fa-edit\" aria-hidden=\"true\"></i></a>";
                        html += "<a href=\"/legal/" + row['link'] + "\"><i style=\"color:#9cb128; margin-right: 5px;\" class=\"fa fa-eye\" aria-hidden=\"true\"></i></a>";
                        return html;
                    },
                    "className": "dt-center",
                    "targets": 7
                }
            ]
        }
    );

	$('#jobs-table').DataTable(
		{
			processing: true,
			serverSide: true,
			responsive: true,
			ajax: '/get_jobs',
			"columnDefs": [
				{
					"render": function ( data, type, row ) {
						return row['company'];
					},
					"targets": 0
				},
				{
					"render": function ( data, type, row ) {
						return row['department'];
					},
					"targets": 1
				},
				{
					"render": function ( data, type, row ) {
						return row['first_name']+' '+row['last_name'];
					},
					"targets": 2
				},
				{
					"render": function ( data, type, row ) {
						return row['email'];
					},
					"targets": 3
				},

                {
                    "render": function ( data, type, row ) {
                        return row['phone'];
                    },
                    "targets": 4
                },
                {
                    "render": function ( data, type, row ) {
                        return row['position'];
                    },
                    "targets": 5
                },

				{
					"render": function ( data, type, row ) {
						return row['type'];
					},
					"targets": 6
				},
                {
                    "render": function ( data, type, row ) {
                        return row['skills'];
                    },
                    "targets": 7
                },
				{
					"render": function ( data, type, row ) {
						return "<a href='/user_download/1/" + row['id'] + "'><i class=\"fa fa-download\" aria-hidden=\"true\"></i></a>";
						// return "<a href='" + row['job_description'] + "'><i class=\"fa fa-download\" aria-hidden=\"true\"></i></a>";
					},
                    "className": "dt-center",
					"targets": 8
				},
				{
					"render": function ( data, type, row ) {
						submitionDate = new Date(row['created_at']);
						return submitionDate.getDate() + '/' + submitionDate.getMonth() + '/' + submitionDate.getFullYear();
					},
					"targets": 9
				},
                {
                    "render": function ( data, type, row ) {
                        return row['source_ip'];
                    },
                    "targets": 10
                },
                {
                    "render": function ( data, type, row ) {
                        return "<a href='javascript:void(0)' onclick=\"deleteJob(" + row['id'] + ")\"><i style=\"color:red;\" class=\"fa fa-close\" aria-hidden=\"true\"></i></a>";
                    },
                    "className": "dt-center",
                    "targets": 11
                }
			]
		}
	);

    $('#contact-email-table').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '/get_contact_emails',
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return row['name'];
                    },
                    "targets": 0
                },
                {
                    "render": function ( data, type, row ) {
                        return "<a href='mailto:" + row['email'] + "'>" + row['email'] + "</a>";
                    },
                    "targets": 1
                },
                {
                    "render": function ( data, type, row ) {
                        return row['subject'];
                    },
                    "targets": 2
                },
                {
                    "render": function ( data, type, row ) {
                        return row['body'];
                    },
                    "targets": 3
                },
                {
                    "render": function ( data, type, row ) {
                        submitionDate = new Date(row['created_at']);
                        return submitionDate.getDate() + '/' + submitionDate.getMonth() + '/' + submitionDate.getFullYear();
                    },
                    "targets": 4
                },
                {
                    "render": function ( data, type, row ) {
                        return row['source_ip'];
                    },
                    "targets": 5
                },
                {
                    "render": function ( data, type, row ) {
                        return "<a href='javascript:void(0)' onclick=\"deleteContactEmail(" + row['id'] + ")\"><i style=\"color:red;\" class=\"fa fa-close\" aria-hidden=\"true\"></i></a>";
                    },
                    "className": "dt-center",
                    "targets": 6
                }
            ]
        }
    );

    $('#call-request-table').DataTable(
        {
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '/get_call_requests',
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return row['name'];
                    },
                    "targets": 0
                },
                {
                    "render": function ( data, type, row ) {
                        return "<a href='mailto:" + row['email'] + "'>" + row['email'] + "</a>";
                    },
                    "targets": 1
                },
                {
                    "render": function ( data, type, row ) {
                        return "<a href='tel:" + row['phone'] + "'>" + row['phone'] + "</a>";
                    },
                    "targets": 2
                },
                {
                    "render": function ( data, type, row ) {
                        return row['topic'];
                    },
                    "targets": 3
                },
                {
                    "render": function ( data, type, row ) {
                        submitionDate = new Date(row['created_at']);
                        return submitionDate.getDate() + '/' + submitionDate.getMonth() + '/' + submitionDate.getFullYear();
                    },
                    "targets": 4
                },
                {
                    "render": function ( data, type, row ) {
                        console.log(data);
                        return row['source_ip'];
                    },
                    "targets": 5
                },
                {
                    "render": function ( data, type, row ) {
                        return "<a href='javascript:void(0)' onclick=\"deleteCallRequest(" + row['id'] + ")\"><i style=\"color:red;\" class=\"fa fa-close\" aria-hidden=\"true\"></i></a>";
                    },
                    "className": "dt-center",
                    "targets": 6
                }
            ]
        }
    );

    if($('.service-slides')) {
		$('.service-slides').slick({
			dots: false,
			arrows: false,
			infinite: true,
			speed: 1000,
			fade: true,
			autoplay: true,
			autoplaySpeed: 1000,
			cssEase: 'linear'
		});
	}
    if($('.technologies_slide').length) {
		$('.technologies_slide').slick({
			dots: false,
			arrows: false,
			slidesToShow: 8,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 4000,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 1,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
			]
		});
	}
});

$(function() {

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    $('.browse-button').on('click', function() {
        var fileInput = $(this).parents('.input-group').find(':file');
        fileInput.trigger('click');
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        });
    });

});

$(window).on('load', function() {
	$(".loader-container").fadeOut("slow");

    setTimeout(function() {
        $('ul#highlights li').each(function (index) {
            bringUpItem($(this), index);
        });


    }, 700);

	setTimeout( function() {
		var softwareServicesMessage =  $('.highlight_summarise');
		softwareServicesMessage.fadeIn(1000);
		softwareServicesMessage.addClass('animate');
		softwareServicesMessage.addClass('zoomInRight');
	}, 1500);

});

function bringUpItem (item, index) {
    setTimeout(
        function() {
            item.fadeIn();
            item.addClass('animated');
            item.addClass('slideInLeft');
        }
        ,150 * index);

    // if(index === 4) {
    //     setTimeout( function() {
    //         var softwareServicesMessage =  $('.highlight_summarise');
    //         softwareServicesMessage.fadeIn(1000);
    //         softwareServicesMessage.addClass('animate');
    //         softwareServicesMessage.addClass('zoomInRight');
    //     }, 1500);
    // }
}

$(document).scroll(function() {

	var isMobile = false; //initiate as false
// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
		|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
		isMobile = true;
	}

	var yValue = $(this).scrollTop();

	if(!isMobile) {
        if (yValue > 70) {
            $('.header-wrapper-2').fadeIn();
            // $('.header-wrapper-2').slideDown();
            // $('.header-wrapper').fadeOut();
        } else {
            $('.header-wrapper-2').fadeOut();
            // $('.header-wrapper-2').slideUp();
            // $('.header-wrapper').fadeIn();
        }
    }
});

$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});



/**
 * Handles toggling the navigation menu for small screens.
 */
( function() {
    var button = document.getElementById( 'topnav' ).getElementsByTagName( 'div' )[0],
        menu   = document.getElementById( 'topnav' ).getElementsByTagName( 'ul' )[0];

    if ( undefined === button )
        return false;

    // Hide button if menu is missing or empty.
    if ( undefined === menu || ! menu.childNodes.length ) {
        button.style.display = 'none';
        return false;
    }

    button.onclick = function() {
        if ( -1 == menu.className.indexOf( 'srt-menu' ) )
            menu.className = 'srt-menu';

        if ( -1 != button.className.indexOf( 'toggled-on' ) ) {
            button.className = button.className.replace( ' toggled-on', '' );
            menu.className = menu.className.replace( ' toggled-on', '' );
        } else {
            button.className += ' toggled-on';
            menu.className += ' toggled-on';
        }
    };
} )();

function deleteCandidate(candidateId) {

    BootstrapDialog.confirm({
        title: 'Confirm Deletion',
        message: 'Are you sure you want to delete the candidate?',
        type: BootstrapDialog.TYPE_PRIMARY,
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'No, do not delete!', // <-- Default value is 'Cancel',
        btnOKLabel: 'Yes, please delete!',
        btnOKClass: 'btn-info',
        callback: function(result) {
            if(result) {
                loader = $(".loader-container-2");
                loader.fadeIn(500);

                $('#table_success').hide();
                $('#table_error').hide();

                $.ajax({
                    type: "POST",
                    url: "/delete_candidate",
                    data: {candidateId: candidateId},
                    success: function (data) {

                        for(var key in data) {
                            $('#table_' + key + ' span').text(data[key]);
                            $('#table_' + key).fadeIn(1000);
                        }

                        $('#applications-table').dataTable().api().ajax.reload();

                        loader.fadeOut(500);
                    },
                    error: function (data) {
                        var ajaxError = JSON.parse(data.responseText);
                        $('#table_error').text(ajaxError.error).fadeIn(1000);
                        loader.fadeOut(500);
                    }
                });
            }else {
                close();
            }
        }
    });
}

function deleteJob(jobId) {

    BootstrapDialog.confirm({
        title: 'Confirm Deletion',
        message: 'Are you sure you want to delete the job?',
        type: BootstrapDialog.TYPE_PRIMARY,
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'No, do not delete!', // <-- Default value is 'Cancel',
        btnOKLabel: 'Yes, please delete!',
        btnOKClass: 'btn-info',
        callback: function(result) {

            if(result) {
                loader = $(".loader-container-2");
                loader.fadeIn(500);

                $('#table_success').hide();
                $('#table_error').hide();

                $.ajax({
                    type: "POST",
                    url: "/delete_job",
                    data: {jobId: jobId},
                    success: function (data) {

                        for(var key in data) {
                            $('#table_' + key + ' span').text(data[key]);
                            $('#table_' + key).fadeIn(1000);
                        }

                        $('#jobs-table').dataTable().api().ajax.reload();

                        loader.fadeOut(500);
                    },
                    error: function (data) {
                        var ajaxError = JSON.parse(data.responseText);
                        $('#table_error').text(ajaxError.error).fadeIn(1000);
                        loader.fadeOut(500);
                    }
                });
            }else {
                close();
            }
        }
    });
}

function toggleDocumentPublished(documentId) {

    BootstrapDialog.confirm({
        title: 'Confirm Publishing/Unpublishing',
        message: 'Are you sure you want to toggle the publishing status of this document?',
        type: BootstrapDialog.TYPE_PRIMARY,
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'No, do not proceed!', // <-- Default value is 'Cancel',
        btnOKLabel: 'Yes, please proceed!',
        btnOKClass: 'btn-info',
        callback: function(result) {

            if(result) {
                loader = $(".loader-container-2");
                loader.fadeIn(500);

                $('#table_success').hide();
                $('#table_error').hide();

                $.ajax({
                    type: "POST",
                    url: "/toggle_document_published",
                    data: {documentId: documentId},
                    success: function (data) {

                        for(var key in data) {
                            $('#table_' + key + ' span').text(data[key]);
                            $('#table_' + key).fadeIn(1000);
                        }

                        $('#document-table').dataTable().api().ajax.reload();

                        loader.fadeOut(500);
                    },
                    error: function (data) {
                        var ajaxError = JSON.parse(data.responseText);
                        $('#table_error').text(ajaxError.error).fadeIn(1000);
                        loader.fadeOut(500);
                    }
                });
            }else {
                close();
            }
        }
    });
}

function deleteDocument(documentId) {

    BootstrapDialog.confirm({
        title: 'Confirm Deletion',
        message: 'Are you sure you want to delete this document?',
        type: BootstrapDialog.TYPE_PRIMARY,
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'No, do not delete!', // <-- Default value is 'Cancel',
        btnOKLabel: 'Yes, please delete!',
        btnOKClass: 'btn-info',
        callback: function(result) {

            if(result) {
                loader = $(".loader-container-2");
                loader.fadeIn(500);

                $('#table_success').hide();
                $('#table_error').hide();

                $.ajax({
                    type: "POST",
                    url: "/delete_document",
                    data: {documentId: documentId},
                    success: function (data) {

                        for(var key in data) {
                            $('#table_' + key + ' span').text(data[key]);
                            $('#table_' + key).fadeIn(1000);
                        }

                        $('#document-table').dataTable().api().ajax.reload();

                        loader.fadeOut(500);
                    },
                    error: function (data) {
                        var ajaxError = JSON.parse(data.responseText);
                        $('#table_error').text(ajaxError.error).fadeIn(1000);
                        loader.fadeOut(500);
                    }
                });
            }else {
                close();
            }
        }
    });
}


function deleteCallRequest(callRequestId) {

    BootstrapDialog.confirm({
        title: 'Confirm Deletion',
        message: 'Are you sure you want to delete the email?',
        type: BootstrapDialog.TYPE_PRIMARY,
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'No, do not delete!', // <-- Default value is 'Cancel',
        btnOKLabel: 'Yes, please delete!',
        btnOKClass: 'btn-info',
        callback: function(result) {

            if(result) {
                loader = $(".loader-container-2");
                loader.fadeIn(500);

                $('#table_success').hide();
                $('#table_error').hide();

                $.ajax({
                    type: "POST",
                    url: "/delete_call_request",
                    data: {callRequestId: callRequestId},
                    success: function (data) {

                        for(var key in data) {
                            $('#table_' + key + ' span').text(data[key]);
                            $('#table_' + key).fadeIn(1000);
                        }

                        $('#call-request-table').dataTable().api().ajax.reload();

                        loader.fadeOut(500);
                    },
                    error: function (data) {
                        var ajaxError = JSON.parse(data.responseText);
                        $('#table_error').text(ajaxError.error).fadeIn(1000);
                        loader.fadeOut(500);
                    }
                });
            }else {
                close();
            }
        }
    });
}

function deleteContactEmail(contactEmailId) {

    BootstrapDialog.confirm({
        title: 'Confirm Deletion',
        message: 'Are you sure you want to delete the email?',
        type: BootstrapDialog.TYPE_PRIMARY,
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'No, do not delete!', // <-- Default value is 'Cancel',
        btnOKLabel: 'Yes, please delete!',
        btnOKClass: 'btn-info',
        callback: function(result) {
            if(result) {
                loader = $(".loader-container-2");
                loader.fadeIn(500);

                $('#table_success').hide();
                $('#table_error').hide();

                $.ajax({
                    type: "POST",
                    url: "/delete_contact_email",
                    data: {contactEmailId: contactEmailId},
                    success: function (data) {

                        for(var key in data) {
                            $('#table_' + key + ' span').text(data[key]);
                            $('#table_' + key).fadeIn(1000);
                        }

                        $('#contact-email-table').dataTable().api().ajax.reload();

                        loader.fadeOut(500);
                    },
                    error: function (data) {
                        var ajaxError = JSON.parse(data.responseText);
                        $('#table_error').text(ajaxError.error).fadeIn(1000);
                        loader.fadeOut(500);
                    }
                });
            }else {
                close();
            }
        }
    });
}
