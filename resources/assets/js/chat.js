

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo'

window.Pusher = require('pusher-js');

// window.Pusher.logToConsole = true;

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '5731f1813b456170f5c0',
    cluster: 'ap1',
    encrypted: false
});

Vue.component('chat-message', require('./components/ChatMessage.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));
Vue.component('chat-starter', require('./components/ChatStarter.vue'));

const app = new Vue({
    el: '#app',
    data: {
        messages: [

        ],
        conversationID: '',
        userName: '',
        firstMessageSent: false
    },
    methods: {
        addFirstMessage(userData) {

            let self = this;
            axios.post('/send_first_chat_message', userData)
                .then(function (response) {

                    self.conversationID = response.data.conversation_id;
                    self.userName = userData.user_name;
                    self.firstMessageSent = true;

                    $('#chat_messages').addClass('chat_messages');

                    self.messages.push({
                        message: 'You are the first in the queue. One of our consultants will be with you shortly.',
                        user: "Alion Support",
                        date: formatDate(new Date())
                    });

                    self.messages.push({
                        message: userData.question,
                        user: self.userName,
                        date: formatDate(new Date())
                    });

                    Vue.nextTick(scrollLog);
                })
                .catch(function (error) {
                    console.log("Error posing first message");
                    console.log(error);
                });
        },
        addMessage(message) {

            this.messages.push({
                message: message.message,
                user: this.userName,
                date: formatDate(new Date())
            });

            Vue.nextTick(scrollLog);

            /**
             * If conversation token is defined, it means we are the operator and are coming from the LIVE CHAT screen in the back end.
             * Otherwise we are the customer and are staring a conversation using the csrf token
             */
            let channelToken = $('meta[name="conversation-token"]').attr('content');
            if(channelToken === undefined) {
                channelToken = $('meta[name="csrf-token"]').attr('content');
            }

            axios.post('/send_chat_message', {
                                                'message':        message.message,
                                                'conversationID': this.conversationID,
                                                'userName':       this.userName,
                                                'channelToken':   channelToken
            }).then(function (response) {

            }).catch(function (error) {
                console.log(error);
                alert(error);
            });
        }
    },
    created() {
        chat(this);
    }
});

function chat(element) {
    /**
     * If conversation token is defined, it means we are the operator and are coming from the LIVE CHAT screen in the back end.
     * Otherwise we ae the customer and are staring a conversation using the csrf token
     */
    let token = $('meta[name="conversation-token"]').attr('content');
    if(token !== undefined) {
        element.conversationID = $('meta[name="conversation-id"]').attr('content');
        element.userName = $('meta[name="user-name"]').attr('content');
    } else {
        // A customer is starting a conversation
        token = $('meta[name="csrf-token"]').attr('content');
    }

    window.Echo.join('livechat.' + token)
        .here((users) => {
            if(element.conversationID > 0) {
                axios.get('/get_conversation_messages/' + element.conversationID)
                .then(function (response) {
                    // debugger;
                    element.messages = response.data;
                    Vue.nextTick(scrollLog);
                })
                .catch(function (error) {
                    // debugger;
                    console.log(error);
                    alert(error);
                });
            }
        })

        .joining((user) => {
            console.log('Joining:');
            console.log(user.name);
        })
        .leaving((user) => {
            console.log('Leaving:');
            console.log(user.name);
        })
        .listen('MessagePosted', (e) => {
            console.log('Got message:');
            console.log(e);
            element.messages.push({
                message: e.message.message,
                user: e.userName,
                date: formatDate(new Date())
            });

            Vue.nextTick(scrollLog);

        });
}

function scrollLog() {
    let messageLog = document.getElementById("chat_messages");
    messageLog.scrollTop = messageLog.scrollHeight;
}

function formatDate(date) {
    let monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    let day = date.getDate();
    let monthIndex = date.getMonth();
    let year = date.getFullYear();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let seconds = date.getSeconds();

    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' ' + hours + ':' + minutes + ':' + seconds;
}