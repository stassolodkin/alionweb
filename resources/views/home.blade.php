@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Dashboard</h2>
    </section>

    <div class="wrapper top-pad" style="height:100%">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @if(Auth::check() && Auth::user()->is_admin == "1")
                                <section>
                                    <h3>Admin Link</h3>
                                    <ul>
                                        <li><a href="/jobs">Jobs</a></li>
                                        <li><a href="/applications">Applications</a></li>
                                        <li><a href="/call_requests">Call Requests</a></li>
                                        <li><a href="/contact_emails">Contact Emails</a></li>
                                        <li><a href="/live_chat">Live Chat</a></li>
                                        <li><a href="/documents">Documents</a></li>
                                    </ul>
                                </section>
                            @elseif(Auth::check() && Auth::user()->is_admin != "1")
                                <section>
                                    <h3>User Link</h3>
                                    <ul>
                                        <li><a href="jobs">Billing Info</a></li>
                                    </ul>
                                </section>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    @include('elements.request_call')--}}

{{--    @include('elements.prefooter')--}}

@endsection
