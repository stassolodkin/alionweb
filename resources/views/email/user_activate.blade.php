Dear {{ $firstName }},
<br /><br />
Thank you for signing up for Alion Hosting account. Please follow the link below to activate your account.
<br/><br />
<a href="{{ $link }}">Click Here</a>
<br /><br />
Have a great day!
<br /><br />
Your Alion Team

