

<div style="background-color: grey;">
    <div style="color: blue;">
        <h1>Alion has received an email from the website. Please follow up on that so you can have a successful business!</h1>
    </div>
    <div>
        From: {{ $emailName }}
    </div>
    <div>
        Email: {{ $emailFrom }}
    </div>
    <div>
        Subject: {{ $emailSubject }}
    </div>
    <div>
        Body: {{ $emailBody }}
    </div>
</div>
