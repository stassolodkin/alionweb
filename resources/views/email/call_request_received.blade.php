

<div style="background-color: grey;">
    <div style="color: blue;">
        <h1>You have received a call request, Stas. Please follow up on that so you can have a successful business!</h1>
    </div>
    <div>
        From: {{ $name }}
    </div>
    <div>
        Email: {{ $email }}
    </div>
    <div>
        Phone: {{ $phone }}
    </div>
    <div>
        Topic: {{ $topic }}
    </div>
</div>
