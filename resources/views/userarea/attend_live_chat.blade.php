@extends('layouts.chat_window')

@section('content')

    <meta name="conversation-token" content="{{ $token }}">
    <meta name="user-name" content="{{ $userName }}">
    <meta name="conversation-id" content="{{ $conversationID }}">

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Live Chat</h2>
    </section>

    <div id="app" class="chat-body" style="padding: 10px;">
        <div class="container bottom-pad">
            <div id="chat_messages" class="row chat_messages">
                <chat-log :messages="messages"></chat-log>
            </div>

            @if ($readOnly == 0)
                <div class="row chat_input">
                    <chat-composer :logged-in="{{ Auth::user() }}" v-on:messagesent="addMessage"></chat-composer>
                </div>
            @endif
        </div>
    </div>
@endsection
