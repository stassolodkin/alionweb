@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Jobs</h2>
    </section>

    <div class="container top-pad">

        <div id="table_success" class="alert alert-success alert-dismissable" style="display: none;">
            <span></span>
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>

        <div  id="table_error" class="alert alert-danger alert-dismissable" style="display: none;">
            <span></span>
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>

        <div class="row top-pad">

            <table id="jobs-table" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" >
                <thead>
                <tr>
                    <th>Company</th>
                    <th>Department</th>
                    <th>Contact Name</th>
                    <th>Contact Email</th>
                    <th>Contact Phone</th>
                    <th>Position</th>
                    <th>Type</th>
                    <th>Skills</th>
                    <th>Job Description</th>
                    <th>Submitted on</th>
                    <th>IP Address</th>
                    <th style="text-align: center;">Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
