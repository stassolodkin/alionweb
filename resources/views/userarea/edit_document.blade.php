@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Documents</h2>
    </section>

    <div class="wrapper top-pad bottom-pad" id="main">

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Document Edit</h3>
                    <hr />
                    {{--<p>Add a document here</p>--}}
                </div>
            </div>
        </div>
        <div class="container">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (!empty($userNotices))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($userNotices as $notice)
                            <li>{{ $notice }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" class="alion_form" id="document_form" enctype="multipart/form-data" action="/document/store">
                <input type="hidden" name="id" id="id" value="@if(!empty($document)){{ $document->id }}@else{{old('id')}}@endif"/>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group required">
                            <label for="name">Document Name</label>
                            <input type="text" name="name" id="name" value="@if(!empty($document)){{ $document->name }}@else{{old('name')}}@endif" maxlength="50"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" name="description" id="description" value="@if(!empty($document)){{ $document->description }}@else{{old('description')}}@endif" maxlength="250"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group required">
                            <label for="link">Link (Must contain no spaces e.g.: 'privacy_policy')</label>
                            <input type="text" name="link" id="link" value="@if(!empty($document)){{ $document->link }}@else{{old('link')}}@endif" maxlength="100"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group required">
                            <label for="type">Type</label><br />
                            <select id="type" name="type" class="form-control">
                                <option value="Legal"
                                    @if(!empty($document) && $document->type == "Legal"  || old('type') == "Legal")
                                            selected="selected"
                                    @endif
                                >Legal</option>
                                <option value="FAQ"
                                    @if(!empty($document) && $document->type == "FAQ" || old('type') == "FAQ")
                                        selected="selected"
                                    @endif
                                >FAQ</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group required">
                            <label for="sort_order">Sort order(number)</label>
                            <input type="text" name="sort_order" value="@if(!empty($document)){{ $document->sort_order }}@else{{old('sort_order')}}@endif" id="sort_order"/>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="published">Published</label>
                                <input id="published" name="published" type="checkbox" class="form-control"
                                    @if(!empty($document))
                                       @if($document->published == true)
                                            checked
                                       @endif
                                    @elseif( old('published') == "on")
                                       checked
                                    @endif
                                >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <div class="form-group required">
                            <label for="content">Content</label><br />
                            <textarea name="content" id='content'>
                                @if(!empty($document->content))
                                    {{ $document->content }}
                                @else
                                    {{ old('content') }}
                                @endif

                            </textarea>
                        </div>
                    </div>
                </div>

                <div class="row"></div>

                {{--<div class="row">--}}
                    {{--<div class="col-xs-12 col-sm-8">--}}
                        {{--<p id="form_success" style="color: green;   display: none;"></p>--}}
                        {{--<p id="form_error"   style="color: darkred; display: none;"></p>--}}
                    {{--</div>--}}
                {{--</div>--}}

{{--                <meta name="csrf-token" content="{{ csrf_token() }}">--}}

                {{ csrf_field() }}

                {{--<div class="row top-pad">--}}
                    {{--<div class="col-xs-12 col-sm-6 col-md-4">--}}
                        {{--<div class="g-recaptcha recapture-width" data-sitekey="6LfDsB0UAAAAAK94xuewWMU5AqnwJWrnvvD014nS"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="row top-pad">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <input type="submit" value="Save">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection