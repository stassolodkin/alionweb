@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Consulting services</h2>
    </section>

    <div class="wrapper top-pad">
      
        <div class="front_image bottom-margin">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-8 col-lg-push-4">
                        <div class="service_front_image_wrapper">
                            <img class="service_front_image" src="/images/services/consulting_front_image.jpg">
                            <div class="service_image_layer"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="service_quote_header">
                                        UTILISE LATEST TECHNOLOGY
                                    </div>
                                    <div class="row">
                                         <div class="col-xs-12 col-sm-6">
                                            <div class="service_quote_body">
                                            to maximise the quality of your software products by fast and efficient delivery and automated testing
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-lg-pull-8 highlight_wrapper">
                        <div>
                            <h1>Our expertise</h1><hr/>
                            <ul id="highlights" class="service_highlights" style="width: 300px;">
                                <li>Development Process</li>
                                <li>Testing Process</li>
                                <li>Delivery Process</li>
                                <li>Agile Practices</li>
                                <li>Infrastructure</li>
                            </ul>
                        </div>
                        <div class="highlight_summarise bottom-margin">
                            <span class="message">For a free consultation.</span><span class="link"><a href="/contact">Click here</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <section class="service_left">
                        <h2>Modern Practices</h2><br />
                        <ul>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Conntinuous Integration</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Continuous Delivery</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Automated Testing</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Automated Rollout</li>
                        </ul>
                    </section>
                </div>
                <div class="hidden-xs hidden-sm col-md-4 bottom-pad">
                    <img src="/images/basic-pic3.png" class="service_card centre-in-div" />
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <section class="service_right">
                        <h2>Technologies</h2><br />
                        <ul>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>AWS, Azure, Compute Cloud</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Docker, rkt</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Kubernetes, Nomad, Swarm</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Jenkins, Bamboo</li>
                        </ul>
                    </section>
                </div>
            </div> 
        </div>
    </div>

    @include('services.consulting_description')
    @include('elements.devops_technologies')
    @include('elements.request_call')
    @include('elements.prefooter')
@endsection
