@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Hosting</h2>
    </section>

    <div class="wrapper top-pad">
      
        <div class="front_image bottom-margin">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-8 col-lg-push-4">
                        <div class="service_front_image_wrapper">
                            <img class="service_front_image" src="/images/services/hosting_front_image.jpg">
                            <div class="service_image_layer"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="service_quote_header">
                                        ALION HOSTING
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="service_quote_body">
                                                provides easy to use web and email solutions
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-lg-pull-8 highlight_wrapper">
                        <div>
                            <h1>Best Plan</h1><hr/>
                            <ul id="highlights" class="service_highlights" style="width: 250px;">
                                <li>Unlimited websites</li>
                                <li>Unlimited email</li>
                                <li>Unlimited space</li>
                                <li>24/7 Support</li>
                                <li>6 months free</li>
                            </ul>
                        </div>
                        <div class="highlight_summarise bottom-margin">
                            <span class="message">To Sign Up</span><span class="link"><a href="/packages">Click here</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <section class="service_left">
                        <h2>Servers</h2><br />
                        <ul>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Located in Australia</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Cloud-based</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Fastest network</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Based on cPanel</li>
                        </ul>
                    </section>
                </div>
                <div class="hidden-xs hidden-sm col-md-4 bottom-pad">
                    <img src="/images/basic-pic2.png" class="service_card centre-in-div" />
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <section class="service_right">
                        <h2>One-click installs</h2><br />
                        <ul>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>WordPress</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>PrestaShop</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Website Builder</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Lots more</li>
                        </ul>
                    </section>
                </div>
            </div>
          
            
        </div>
    </div>
    <div class="wrapper top-pad">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <section>
                        <h3>Online services</h3>
                        <p class="justify_text">
                            Hosting your own websites, email and other services, even if they are on a cloud, requires constant support.
                            This results in having to spend time and resources, but most importantly requires attention that could be utilised
                            in providing your primary services. It's much more convenient to report a problem than having to solve it.
                        </p>
                        <p>
                            Our team has a number of experienced DevOps engineers that can setup and maintain your services, be it your own
                            application or a third-party software.
                        </p>
                    </section>
                </div>
            </div>
        </div>
    </div>

    @include('elements.hosting_technologies')
    @include('elements.request_call')
    @include('elements.prefooter')
@endsection
