@extends('layouts.web')

@section('content')

    @include('elements.useful-links')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Recruitment</h2>
    </section>

    <div class="wrapper top-pad">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <section>
                        <h3>Network design</h3>
                        <p class="justify_text">
                            We offer a range of consulting services which include enough, to build your company's scalable IT infrastructure from
                            ground up and be able to maintain it for you. The services include:
                        </p>
                        <ul>
                            <li><a href="infrastructure">Infrastructure</a></li>
                            <li><a href="process_improvement">Process Improvement</a></li>
                            <li><a href="development_environment">Development Environment</a></li>
                            <li><a href="automated_testing">Automated Testing</a></li>
                            <li><a href="continuous_integration">Continuous Integration</a></li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection
