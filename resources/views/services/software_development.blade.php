@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Software Development</h2>
    </section>

    <div class="wrapper top-pad">
      
        <div class="front_image bottom-margin">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-8 col-lg-push-4">
                        <div class="service_front_image_wrapper">
                            <img class="service_front_image" src="/images/services/software_development_front_image.jpg">
                            <div class="service_image_layer"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="service_quote_header">
                                        ACCELERATE YOUR PROJECT
                                    </div>
                                    <div class="row">
                                         <div class="col-xs-12 col-sm-6">
                                            <div class="service_quote_body">
                                                with custom software carefully crafted to fulfill your business needs
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-lg-pull-8 highlight_wrapper">
                        <div>
                            <h1>Our expertise</h1><hr/>
                            <ul id="highlights" class="service_highlights" style="width: 195px;">
                                <li>Requirements</li>
                                <li>Architecture</li>
                                <li>Development</li>
                                <li>Deployment</li>
                                <li>Maintenance</li>
                            </ul>
                        </div>
                        <div class="highlight_summarise bottom-margin">
                            <span class="message">For a free consultation.</span><span class="link"><a href="/contact">Click here</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <section class="service_left">
                        <h2>Custom Software</h2><br />
                        <ul>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Built to suit your workflow</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Hosted in our infrastructure</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Managed by our team</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Any combination of the above</li>
                        </ul>
                    </section>
                </div>
                <div class="hidden-xs hidden-sm col-md-4 bottom-pad">
                    <img src="/images/basic-pic1.png" class="service_card centre-in-div" />
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <section class="service_right">
                        <h2>Details</h2><br />
                        <ul>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>User experience design</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Software Architecture</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Agile development</li>
                            <li><i class="fa fa-check right-margin" aria-hidden="true"></i>Technology agnostic</li>
                        </ul>
                    </section>
                </div>
            </div>
          
            
        </div>
    </div>

    @include('elements.technologies')
    @include('elements.request_call')
    @include('elements.prefooter')
@endsection
