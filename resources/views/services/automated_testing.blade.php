@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Automated Testing</h2>
    </section>

    <div class="wrapper top-pad">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <section>
                        <h3>Push of a button</h3>
                        <p class="justify_text">
                            Automated testing and test driven development have been in the software development game for quite for some time now.
                            For many software vendors, however, to stay stable in a competitive market a constant focus must be made on delivering
                            of new features and bug fixes. Due to this fact, many software companies have stil not had chance to attend to this
                            and introduce automated testing to their software development process.
                        </p>
                        <p>
                            We have extensive experience in manual as well as automated testing and can define required tests for you as well as
                            create the necessary automation environment.
                        </p>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection
