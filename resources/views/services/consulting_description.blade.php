{{-- @extends('layouts.web') --}}

{{-- @section('content') --}}

    {{-- <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Consulting services</h2>
    </section> --}}

    <div class="wrapper top-pad">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <section>
                        {{-- <h3>Network design</h3>
                        <p class="justify_text">
                            We offer a range of consulting services which include enough, to build your company's scalable IT infrastructure from
                            ground up and be able to maintain it for you. The services include:
                        </p> --}}
                        {{--<ul>--}}
                            {{--<li><a href="infrastructure">Infrastructure</a></li>--}}
                            {{--<li><a href="process_improvement">Process Improvement</a></li>--}}
                            {{--<li><a href="development_environment">Development Environment</a></li>--}}
                            {{--<li><a href="automated_testing">Automated Testing</a></li>--}}
                            {{--<li><a href="continuous_integration">Continuous Integration</a></li>--}}
                        {{--</ul>--}}


                        <h3>Process Improvement</h3>
                        <h4>Agile</h4>
                        <p style="text-align:justify;">
                            We can either analyse the applicability of Agile methodology to your existing processes and help upgrade them as necessary or design and help implementing the processes from scratch.
                        </p>
                        <p><a href="process_improvement">Read More...</a></p>

                        <h3>Development Environment</h3>
                        <p style="text-align:justify;">
                            We can design your development environment and process and help automating the process of building these environments for new developers.
                        </p>
                        <p><a href="development_environment">Resd More...</a></p>

                        <h3>Infrastructure</h3>
                        <p style="text-align:justify;">
                            We can design, implement and maintain infrastructure for your production, testing or developmet environments.
                        </p>
                        <p><a href="infrastructure">Resd More...</a></p>

                        <h3>Automated Testing</h3>
                        <p>
                            Automated testing as well as test-driven development has proved to be invaluable in the industry, but has still not been adopted by a lot of software development companies.
                            We can assist in designing the process and/or building the automated test suite for you.
                        </p>
                        <p><a href="automated_testing">Resd More...</a></p>

                        <h3>Continuous Integration and Delivery</h3>
                        <p>
                            Continuous integration and delivery are processes by which your automated tests are run and software is deployed automatically whenever a developer commits a change to your repository.
                            We can set this up and host this for you.
                        </p>
                        <p><a href="continuous_integration">Resd More...</a></p>

                    </section>
                </div>
            </div>
        </div>
    </div>

{{-- @endsection --}}
