@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Process Improvement</h2>
    </section>

    <div class="wrapper top-pad">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <section>
                        <h3>Do you have a brilliant Idea?</h3>
                        <p class="justify_text">
                            We do not! We are software builders that can sculpt your brilliant idea and turn it into reality.
                        </p>
                        <p>
                            Our software develoment experience ranges from simple static websites to high availability real-time applications
                            supporting heavy loads and geographic redundancy
                        </p>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection
