<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Alion Live Chat</title>
        <meta name="description" content="Alion Solutions - Your ultimate IT solution">
        <meta name="keywords" content="">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

        <link rel="shortcut icon" href="favicon.ico"  type="image/x-icon">

        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
        <link href="{{ mix('/css/main.css') }}" rel="stylesheet">

        <link href="{{ mix('/css/form.css') }}" rel="stylesheet">
        <link href="{{ mix('/css/menu.css') }}" rel="stylesheet">
        <link href="{{ mix('/css/loader.css') }}" rel="stylesheet">
        <link href="{{ mix('/css/normalize.css') }}" rel="stylesheet">

        <link href="{{ mix('/fonts/JuliusSansOne-Regular.ttf') }}" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Damion|Gurajada|Macondo|NTR|Philosopher|Tillana|Vibur" rel="stylesheet">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
            window.Laravel = <?php echo json_encode([
                    'csrfToken' => csrf_token(),
            ]); ?>
        </script>

    </head>

    <body id="home">

        <div class="loader-container-2">
            <div class='loader loader3'>
                <div>
                    <div>
                        <div>
                            <div>
                                <div>
                                    <div>
                                        <div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="bg_image">--}}
        {{--</div>--}}

        {{--<div class="bg_layer"></div>--}}

        <header class="header-wrapper clearfix">

            <div id="banner">
                <div id="logo"><a href="{{ url('/') }}"><img width="163" height="65" src="/images/alion_logo_green_blue.png" alt="logo"></a></div>
            </div>

            <span class="call-now">
                <i class="fa fa-mobile-phone" aria-hidden="true"></i>
                <a href="tel:03 9972 9846">03 9972 9846</a>
                <a style="padding-left:10px;" href="mailto:info@alion.com.au"><i style="padding-right:3px;" class="fa fa-envelope" aria-hidden="true"></i>info@alion.com.au</a>
            </span>
            {{--<span class="mail-now">--}}
                {{--<a href="mailto:info@alion.com.au"><i class="fa fa-envelope" aria-hidden="true"></i> info@alion.com.au</a>--}}
            {{--</span>--}}
        </header>

        @yield('content')

        <div class="chat_footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div
                                id="attribution"
                                style="font-size:11px;"
                        >
                            <div class="footer_content">
                                Alion Solutions &copy; 2017 All rights reserved.
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ mix('/js/app.js') }}"></script>
        <script src="{{ mix('/js/chat_helpers.js') }}"></script>
        <script src="{{ mix('/js/chat.js') }}"></script>
        <script src="{{ mix('/js/form.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-maxlength/1.7.0/bootstrap-maxlength.min.js"></script>

    </body>

</html>