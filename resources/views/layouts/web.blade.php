<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    @include('elements.css')

    <body id="home">
    
        <div class="loader-container-2">
            <div class='loader loader3'>
                <div>
                    <div>
                        <div>
                            <div>
                                <div>
                                    <div>
                                        <div>
                                            {{--<div>--}}
                                            {{--<div>--}}
                                            {{--<div></div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="loader-container">
            <div class='loader loader3'>
                <div>
                    <div>
                        <div>
                            <div>
                                <div>
                                    <div>
                                        <div>
                                            {{--<div>--}}
                                                {{--<div>--}}
                                                    {{--<div></div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bg_image"></div>
        <div class="bg_layer"></div>

        <div class="header_background">
            @include('elements.header')
        </div>

        @if (Auth::check())
            @include('userarea.user_strip')
        @endif
{{--TODO: This is temporary - might turn floating menu on for users later--}}
        @if (!Auth::check())
            @include('elements.header-2')
        @endif

        @yield('content')

        @include('elements.footer')

        @include('elements.javascript')

    </body>

</html>