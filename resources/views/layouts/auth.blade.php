@extends((!isset(Auth::user()->id)) ? 'layouts.web': ((Auth::user()->is_admin == "1") ? 'layouts.admin' : 'layouts.customer'));

@yield('content')