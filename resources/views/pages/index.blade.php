@extends('layouts.web')

@section('content')

{{--    @include('elements.useful-links')--}}

{{--<section id="page-header" class="clearfix">--}}
    {{--<h2 class="heading elegantshadow">HOME</h2>--}}
{{--</section>--}}



    <div class="wrapper top-pad">
        {{--<div class="container">--}}
            <div class="banner">
                <div class="banner_content">
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            Do you need a cyber space?
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <a class="signup" href="/packages">SIGN UP</a>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            6 months Free Web/Email hosting.
                        </div>
                    </div>
                </div>
            </div>
        {{--</div>--}}
    </div>

    <div class="wrapper top-pad">
        <div class="front_image">
            <div class="container">

                <div class="row">
                    <div class="image col-xs-12 col-lg-8 col-lg-push-4">
                        <div class="front_image_wrapper">
                            <img src="/images/relax3.jpg">
                            <div class="front_image_layer"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-4 col-lg-pull-8" >
                        <div class="highlight">
                            <h1>Alion Solutions</h1><hr/>
                            <ul id="highlights">
                                <li>Customer focus</li>
                                <li>Tailored software</li>
                                <li>cPanel hosting</li>
                                <li>Managed services</li>
                                <li>Free consultation</li>
                            </ul>

                            <div class="highlight_summarise">
                                <span class="message">Services for software companies.</span><span class="link"><a href="/services">Learn more...</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<!-- main content area -->--}}

    <div id="do-it-right" class="wrapper">
        <div class="container">
            <h1 class="section_title">Do IT Right</h1>
            <div class="row">
                <div class="col-xs-12 col-md-4 col-md-push-8">
                    <div>
                        <div id="old_technology" class="framed">
                            {{--<img src="/images/typewriter_3.jpg">--}}
                            <img src="/images/auto.jpeg">
                            <div class="old_technology_layer"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-8 col-md-pull-4">

                    <p style="text-align:justify;">
                        As we all know, today, new technologies emerge every day, standards change rapidly and the rate of this change increases
                        exponentially. Software development is a very involved process that requires a lot of attention and it is common for a
                        software company to find itself using old tools and processes that are well behind the current standards before anyone
                        has a chance to attend to this.
                    </p>
                    <p style="text-align:justify;">
                        Sooner or later the result of neglecting the ever evolving industry tools will render your software unmaintainable if not
                        unusable and will require rewriting it from scratch or going through a lengthy and tedious series of upgrades.
                    </p>
                    <p style="text-align:justify;">
                        One of our specialties is helping companies that do not have required resources to attend to these matters and analyse where it
                        currently stands within this realm as well as designing the strategy and, if required, implementing the process
                        of moving the required components to a more modern version or a counterpart.
                    </p>
                    <p style="text-align:justify;">
                        Please read more about our services by following the link below.
                    </p>

                    <a href="services" class="buttonlink">Services</a>
                </div>


            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <h1 class="first-header">Software development</h1>
                    <div class="bottom-pad">
                        <a href="services/software_development">
                            <img src="images/basic-pic1.png" class="card centre-in-div" />
                        </a>
                    </div>
                    <p style="text-align:justify;">Software is like humans. No matter how many different types already exist or how flexible they are, there is always some one who would benefit from a custom unit tailored to one's unique needs.</p>
                    <p><a href="services/software_development">Read more...</a></p>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <h1 class="first-header">Hosting/DevOps</h1>
                    <div class="bottom-pad">
                        <a href="services/service_hosting">
                            <img src="images/basic-pic2.png" class="card centre-in-div" />
                        </a>
                    </div>
                    <p style="text-align:justify;">Service hosting is burden that many companies could benefit from avoiding. In a fast-paced environment that requires full attention, the last thing that one needs to worry about is having setup and maintain a web or an email server.</p>
                    <p style="text-align:justify;">We can setup your servers within a cloud-based provider, e.g.: AWS, and maintain them for you</p>
                    <p><a href="services/service_hosting">Read more...</a></p>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <h1 class="first-header">Consulting</h1>
                    <div class="bottom-pad">
                        <a href="services/consulting">
                            <img src="images/basic-pic3.png" class="card centre-in-div" />
                        </a>
                    </div>
                    <p style="text-align:justify;">Whether you are at the start of your venture or an already established business, in the age of rapid technological evolution, having a professional asses your current setup as well as process can often prove invaluable for those who are not afraid of change.</p>
                    <p style="text-align:justify;">We have a team that is passionate about new technologies as well as the software development infrastructure in itself, who would be more then happy to assess your tools and processes, while making a strong emphasis on your business needs and budget and suggest appropriate course of action.</p>
                    <p><a href="services/consulting">Read more...</a></p>
                </div>
            </div>
        </div>
    </div>

    @include('elements.request_call')

    @include('elements.prefooter')

@endsection