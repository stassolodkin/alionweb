@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Alion Hosting</h2>
    </section>


    <div class="wrapper top-pad bottom-pad" id="main">

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Signup success</h3>
                    <hr />
                    <p>Your activation link has expired. <a href="">Click here</a> to resend the activation email.</p>
                </div>
            </div>
        </div>
    </div>
@endsection