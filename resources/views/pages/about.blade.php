@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        {{--<div class="wrapper">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-12">--}}
                        <h2 class=" heading elegantshadow">About</h2>
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </section>


    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <section>
                        <p style="text-align:justify;">
                        <p>Alion Solutions is a Melbourne-based software development and IT consultancy firm. We have emerged from a group of individuals that come from a wide variety of information technology-related areas ranging from hardware and software support, through application development to IT project management.</p>
                        <p>We specialise in creation of online applications and assisting software companies in modernising their tools and processes in order to keep up with the ever evolving industry.</p>
                    </section>
                </div>
            </div>
        </div>
    </div>

    @include('elements.prefooter')

@endsection
