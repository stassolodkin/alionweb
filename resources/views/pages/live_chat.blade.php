@extends('layouts.chat_window')

@section('content')

    <meta name="conversation-token" content="{{ $token }}">

    <section id="page-header" class="clearfix">
        <h2 class="heading elegantshadow">Live Chat</h2>
    </section>

    <div id="app" class="chat-body" style="padding: 10px;">
        <div class="container">
            <div id="chat_messages" class="row">
                <chat-starter v-show="!firstMessageSent" v-on:firstmessagesent="addFirstMessage"></chat-starter>
                <chat-log v-show="firstMessageSent" :messages="messages"></chat-log>
            </div>
            <div class="row chat_input">
                {{--<div class="col-xs-12">--}}
                    <chat-composer v-show="firstMessageSent" v-on:messagesent="addMessage"></chat-composer>
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection
