@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Contacts</h2>
    </section>


    <div class="wrapper top-pad" id="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 bottom-pad">

                    @include('elements.contact_form')
                </div>
                <div class="col-xs-12 col-sm-6">
                    <section>
                        <h2>Contact details</h2>
                        <p>
                            Address: 3/21 Anderson St, Caulfield  Vic 3162.<br/>
                            Ph:      03 99729846<br/>
                            Email:   info@alion.com.au<br/>
                        </p>
                    </section>
                </div>
            </div>
        </div>
    </div>

@endsection