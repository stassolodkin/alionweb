@extends('layouts.web')

@section('content')
    <div class="container top-pad">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{--<h1 class="panel-title"><span class="glyphicon glyphicon-file text-primary"></span>--}}
                                <span class="panel-title">legal documents</span>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                @foreach ($documents as $document)
                                    <tr>
                                        <td><a href="{{ '/legal/' . $document->link  }}">
                                            @if($document->link == $currentDocument)
                                                <span class="glyphicon glyphicon-arrow-right"></span>
                                            @endif
                                            {{ $document->name }}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                {{--<tr>--}}
                                    {{--<td>--}}
                                        {{--@if( preg_match( '/legal$/', Request::path() ) )--}}
                                            {{--<span class="glyphicon glyphicon-arrow-right"></span>--}}
                                        {{--@endif--}}
                                        {{--<a href="{{ url('/privacy_policy$') }}">Terms of Service</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>--}}
                                        {{--@if( preg_match( '/privacy_policy/', Request::path() ) )--}}
                                            {{--<span class="glyphicon glyphicon-arrow-right"></span>--}}
                                        {{--@endif--}}
                                        {{--<a href="http://www.jquery2dotnet.com">Privacy Policy</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>--}}
                                        {{--<a href="http://www.jquery2dotnet.com">Web Hosting Terms</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>--}}
                                        {{--<a href="http://www.jquery2dotnet.com">Email Service Terms</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>--}}
                                        {{--<a href="http://www.jquery2dotnet.com">DNS Service Terms</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>--}}
                                        {{--<a href="http://www.jquery2dotnet.com">Software Development Terms</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>--}}
                                        {{--<a href="http://www.jquery2dotnet.com">Consulting Terms</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>--}}
                                        {{--<a href="http://www.jquery2dotnet.com">Recruitment Terms</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}

                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-9 col-md-9">
                <div class="well">

                    @foreach ($documents as $document)
                        @if($document->link == $currentDocument)
                            {!! $document->content !!}
                            <div class="updated_date" style="float: right; font-size: 12px; font-style: italic;">
                                Last updated on: {{ Carbon\Carbon::parse($document->updated_at)->format('d M Y') }}
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection