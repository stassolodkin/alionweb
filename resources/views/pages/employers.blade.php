@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Employers</h2>
    </section>

    <div class="wrapper top-pad bottom-pad" id="main">

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Submit a job</h3>
                    <hr />
                    <p>Submit your job description here and we will do our best to find the right candidate within a given time frame.</p>
                </div>
            </div>
        </div>
        <div class="container">
            <form method="POST" class="alion_form" id="job_form" enctype="multipart/form-data">

                <div class="panel panel-primary">
                    <div class="panel-heading">Employer's Details</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group required">
                                    <label for="company">Company</label>
                                    <input type="text" name="company" id="company" maxlength="50"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label for="department">Department</label>
                                    <input type="text" name="department" id="department" maxlength="50"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group required">
                                    <label for="first_name">First Name</label><br />
                                    <input type="text" name="first_name" id="first_name" maxlength="50"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group required">
                                    <label for="last_name">Last Name</label><br />
                                    <input type="text" name="last_name" id="last_name" maxlength="50"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group required">
                                    <label for="email">Email</label><br />
                                    <input type="text" name="email" id="email" maxlength="100"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label for="phone">Phone</label><br />
                                    <input type="text" name="phone" id="phone" maxlength="50"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Job Details</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group required">
                                    <label for="position">Position</label><br />
                                    <input type="text" name="position" id="position" maxlength="50"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group required">
                                    <label for="type">Type</label><br />
                                    <select id="type" name="type" class="form-control">
                                        <option value="Full-time">Full-time</option>
                                        <option value="Part-time">Part-time</option>
                                        <option value="Casual">Casual</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label for="skills">Add Required Skills(Up to 5)</label><br />
                                    <select multiple data-role="tagsinput" id="alion-tags"></select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group required">
                                    <label for="job_description_file_name">Job description(PDF or MS Word file up to 2MB)</label><br />
                                    <div class="input-group">
                                        <input id="job_description" name="job_description" type="file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf" style="display: none;">
                                        <input id="job_description_file_name" type="text" class="form-control" placeholder="Select file">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary browse-button" type="button">Browse</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-8">
                                <div class="form-group">
                                    <label for="message">Additional information</label><br />
                                    <textarea name="message" id='message' maxlength="512"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row"></div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-8">
                                <p id="form_success" style="color: green; display: none;"></p>
                                <p id="form_error" style="color: darkred; display: none;"></p>
                            </div>
                        </div>

                        <meta name="csrf-token" content="{{ csrf_token() }}">

                        <div class="row top-pad">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="g-recaptcha recapture-width" data-sitekey="6LfDsB0UAAAAAK94xuewWMU5AqnwJWrnvvD014nS"></div>
                            </div>
                        </div>

                        <div class="row top-pad">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <input type="button" value="Send" onclick="uploadJobDescription()">
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection