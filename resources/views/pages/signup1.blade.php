@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Alion Hosting</h2>
    </section>


    <div class="wrapper top-pad bottom-pad" id="main">

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>New Customer SignUp</h3>
                    <hr />
                    {{--<p>Please feel in the form below.</p>--}}
                </div>
            </div>
        </div>
        <div class="container">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)

                            @if($error == 'The tc field is required.')
                                <li>You must accept our terms and conditions.</li>
                            @else
                                <li>{{ $error }}</li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (!empty($userNotices))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($userNotices as $notice)
                            <li>{{ $notice }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="panel panel-primary">

                <div class="panel-heading">Step 1 of 2</div>
                <form method="POST" action="/signup_submit" class="alion_form" id="signup_form" enctype="multipart/form-data">
                    <div class="panel-body">

                        <div>Domain Name and Package</div>
                        <hr/>

                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group required">
                                    <label for="domain">Domain Name</label><br />
                                    <input type="text" name="domain" id="domain" value="{{ old('domain') }}" maxlength="255"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group required">
                                    <label for="package">Package</label><br />
                                    <select id="package" name="package" class="form-control">
                                        <option value="basic"
                                            @if(!empty($selectedPackage) && $selectedPackage == "basic" || old('package') == "basic")
                                                selected="selected"
                                            @endif
                                        >{{ $basic->name }}</option>
                                        <option value="intermediate"
                                            @if(!empty($selectedPackage) && $selectedPackage == "intermediate" || old('package') == "intermediate")
                                                selected="selected"
                                            @endif
                                        >{{ $intermediate->name }}</option>
                                        <option value="advanced"
                                            @if(!empty($selectedPackage) && $selectedPackage == "advanced" || old('package') == "advanced")
                                                selected="selected"
                                            @endif
                                        >{{ $advanced->name }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div>Personal Details</div>
                        <hr/>




                        <div class="col-xs-12 col-sm-8">

                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group required">
                                        <label for="first_name">First Name</label><br />
                                        <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" maxlength="100"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group required">
                                        <label for="last_name">Last Name</label><br />
                                        <input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" maxlength="100"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group required">
                                        <label for="email">Email</label><br />
                                        <input type="text" name="email" id="email" value="{{ old('email') }}" maxlength="255"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group required">
                                        <label for="username">Username</label><br />
                                        <input type="text" name="username" id="username" value="{{ old('username') }}" maxlength="100"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group required">
                                        <label for="password">Password</label><br />
                                        <input type="password" name="password" id="password" maxlength="100"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group required">
                                        <label for="confirm_password">Confirm Password</label><br />
                                        <input type="password" name="confirm_password" id="confirm_password" maxlength="100"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="business">Business Name</label>
                                        <input type="text" name="business" id="business" value="{{ old('business') }}" maxlength="50"/>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="brn">Business Registration Number</label>
                                        <input type="text" name="brn" id="brn" value="{{ old('brn') }}" maxlength="50"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="add_ons" class="col-xs-12 col-sm-4">
                            <div id="add_ons_frame">
                                <div class="row" id="add_ons_header">
                                    Optional Add-ons
                                </div>

                                <div class="row">
                                    <div id="shell_access" class="col-xs-12">
                                        <div class="form-group">
                                            <input style="width:auto;" type="checkbox" name="shell_access" id="sa"
                                                @if( old('shell_access') == "on")
                                                    checked
                                                @endif
                                            >
                                            <label for="sa">Shell Access($4.99 Monthly)</label>
                                        </div>
                                    </div>
                                </div>
{{--<div class="row">{{$package}}</div>--}}
                                {{--@unless(!empty($package) && $package == "best" || old('package') == "best")--}}
                                    <div class="row">
                                        <div id="dedicated_ip" class="col-xs-12">
                                            <div class="form-group">
                                                <input style="width:auto;" type="checkbox" name="dedicated_ip" id="dip"
                                                       @if( old('dedicated_ip') == "on")
                                                       checked
                                                        @endif
                                                >
                                                <label for="dip">Dedicated IP($4.99 Monthly)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div id="backups" class="col-xs-12">
                                            <div class="form-group">
                                                <input style="width:auto;" type="checkbox" name="backups" id="bu"
                                                    @if( old('backups') == "on")
                                                        checked
                                                    @endif
                                                >
                                                <label for="bu">Backups($2.99 Monthly)</label>
                                            </div>
                                        </div>
                                    </div>
                                {{--@endunless--}}
                                <div class="row">
                                    <div id="ssl_certificate" class="col-xs-12">
                                        <div class="form-group">
                                            <input style="width:auto;" type="checkbox" name="ssl_certificate" id="ssl"
                                            @if( old('ssl_certificate') == "on")
                                                checked
                                            @endif
                                            >
                                            <label for="ssl">SSL Certificate($4.99 Monthly)</label>
                                        </div>
                                    </div>
                                </div>
                                <p style="font-size: 0.7em; padding:10px; text-align: center"><span style="color:red">*</span>Minimum term applies as defined in the main package</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <hr />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <input style="width:auto;" type="checkbox" name="tc" id="tc"/>
                                    <span style="color:darkred">*</span>
                                    <label style="display: inline;" for="tc">I, acknowledge that I have read and hereby accept the terms and conditions outlined in the following documents:
                                        <a href="">Terms And Conditions</a>,
                                        <a href="">Privacy Policy</a>
                                    </label>
                                    {{--<span>I acknowledge that I read and hereby accept the conditions outlined in the following legal</span>--}}
                                </div>
                            </div>
                        </div>

                        <p id="form_error" style="color: darkred; display: none;"></p>

                        {{ csrf_field() }}

                        <div class="row top-pad">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="g-recaptcha recapture-width" data-sitekey="6LfDsB0UAAAAAK94xuewWMU5AqnwJWrnvvD014nS"></div>
                            </div>
                        </div>

                        <div class="row top-pad">
                            <div class="col-xs-12">
                                <div class="pull-right">
                                    <input type="button" value="Create" onclick="signUpSubmit()">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection