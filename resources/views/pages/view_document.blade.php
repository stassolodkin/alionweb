@if($fileType == 'docx')
    <iframe src="https://view.officeapps.live.com/op/view.aspx?src={{ env('APP_URL') }}/docs/{{$documentName}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>
@elseif($fileType == 'pdf')
    <iframe src="{{ env('APP_URL') }}/docs/{{$documentName}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>
@endif
