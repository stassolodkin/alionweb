@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        {{--<div class="wrapper">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-12">--}}
                        <h2 class=" heading elegantshadow">Services</h2>
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </section>


    <div class="wrapper top-pad" id="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Software development</h3>
                    <p style="text-align:justify;">
                        We have a wide range of experience, from static responsive websites to high volume, real-time, mission-critical, systems that have no right for error, i.e.: Mobile Prepaid Billing.
                    </p>

                    <h3>cPanel and managed hosting</h3>
                    <p style="text-align:justify;">
                        We can host a variety of services, from Web, Email and DNS to any custom application.
                    </p>


                    <h3>Consulting</h3>
                    <h4>Software companies</h4>
                    <ul>
                        <li>Process analysis</li>
                        <li>Codebase analysis</li>
                        <li>Automated testing</li>
                        <li>Continuous Integration</li>
                        <li>Infrastructure</li>
                        <li>Free consultation</li>
                    </ul>
                    <p style="text-align:justify;">
                        Aiding software development companies to setup and maintain modern practices.
                    </p>

                    <h4>Everyone</h4>
                    <p style="text-align:justify;">
                        Design, implementation and maintenance of IT infrastructure.
                    </p>

                    <h3>Recruitment</h3>
                    <p style="text-align:justify;">
                        We are determined to find an employee who would be perfect for your job requirements. All candidates will pass a technical
                        test relevant to your position description before being sent in for an interview.
                    </p>

                    <!--    </section>-->

                    <!-- sidebar -->
                    <!--    <aside>-->
                    <!--        <h3>Secondary Section menu</h3>-->
                    <!--            <nav id="secondary-navigation">-->
                    <!--                    <ul>-->
                    <!--                        <li><a href="#">menu item</a></li>-->
                    <!--                        <li class="current"><a href="#">current menu item</a></li>-->
                    <!--                        <li><a href="#">menu item</a></li>-->
                    <!--                        <li><a href="#">menu item</a></li>-->
                    <!--                        <li><a href="#">menu item</a></li>-->
                    <!--                    </ul>-->
                    <!--             </nav>-->
                    <!--      </aside>-->
                </div>
            </div>
        </div>
    </div>

    @include('elements.prefooter')
@endsection