@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class="heading elegantshadow">Alion Hosting</h2>
    </section>

    <meta name="invoice-id" content="{{ $invoice->id }}">

    <div class="wrapper top-pad bottom-pad" id="main">

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Web and Email Hosting Checkout</h3>
                </div>
            </div>
        </div>
        <div id="invoice"  class="container">
            <form method="POST" action="/signup_final" class="alion_form" id="payment_form" enctype="multipart/form-data" @submit.prevent="submitForm">

                <div class="panel panel-primary signup_panel">
                    <div class="panel-heading">Step 2 of 2</div>
                    <div class="panel-body">

                        <span>Account: {{ $account->id }}</span>
                        <span id="invoice_id" class="pull-right">Invoice: {{ $invoice->id }}</span>
                        <hr/>
                        <div>
                            <invoice-table :items="items" :invoice-total="invoiceTotal" v-on:addonremoved="removeAddOn"></invoice-table>
                        </div>
                        <hr>

                        {{--Addons--}}
                        <div class="row">

                            <div id="add_ons" class="col-xs-12 col-md-6 col-md-push-6">
                                <div id="add_ons_frame">
                                    <div class="row" id="add_ons_header">
                                        <h1>Optional Add-ons</h1>
                                    </div>



                                    <div class="row">
                                        <div id="shell_access" class="col-xs-12">
                                            <div class="form-group">
                                                <input style="width:auto;" type="checkbox" name="shell_access" id="sa" v-model="addOns.shell_access" @click="addOnClick('shell_access')">
                                                <label for="sa">Shell Access($4.99 Monthly)</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div id="dedicated_ip" class="col-xs-12">
                                            <div class="form-group">
                                                <input style="width:auto;" type="checkbox" name="dedicated_ip" id="dip" v-model="addOns.dedicated_ip" @click="addOnClick('dedicated_ip')">
                                                <label for="dip">Dedicated IP($4.99 Monthly)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div id="backups" class="col-xs-12">
                                            <div class="form-group">
                                                <input style="width:auto;" type="checkbox" name="backups" id="bu" v-model="addOns.backups" @click="addOnClick('backups')">
                                                <label for="bu">Backups($2.99 Monthly)</label>
                                            </div>
                                        </div>
                                    </div>
                                    {{--@endunless--}}
                                    <div class="row">
                                        <div id="ssl_certificate" class="col-xs-12">
                                            <div class="form-group">
                                                {{--<input style="width:auto;" type="checkbox" name="ssl_certificate" id="ssl"--}}
                                                <input style="width:auto;" type="checkbox" name="ssl_certificate" id="ssl" v-model="addOns.ssl_certificate" @click="addOnClick('ssl_certificate')"
                                                       {{--@if( old('ssl_certificate') == "on")--}}
                                                       {{--checked--}}
                                                        {{--@endif--}}

                                                        {{--@foreach($invoice->serviceSubscriptions as $serviceSubscription)--}}
                                                            {{--@if( $serviceSubscription->service->name == "SSL")--}}
                                                                {{--checked--}}
                                                            {{--@endif--}}
                                                        {{--@endforeach--}}
                                                >
                                                <label for="ssl">SSL Certificate($4.99 Monthly)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <p style="font-size: 0.8em; padding:10px; text-align: center"><span style="color:red; margin-right: 3px">*</span>Minimum term applies as defined in the main package</p>
                                </div>
                            </div>


                            <div class="col-xs-12 col-md-6 col-md-pull-6">

                                <div class="row">
                                    <div class="col-xs-12 control-label">
                                        <h1>Payment methods</h1>
                                        <label class="radio-inline"><input type="radio" value="credit_card" name="payment_option" checked>Credit Card</label>
                                        <label class="radio-inline"><input type="radio" value="paypal" name="payment_option">PayPal</label>
                                    </div>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12">
                                    <div class="credit_card_form">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Credit Card</h3>
                                              
<!--                                                   <img style="" height="10px" width="20px" src="/images/cc_icons/visa.png"></img>
                                                  <img height="10px" width="20px" src="/images/cc_icons/mastercard.png"></img>
                                                  <img height="10px" width="20px" src="/images/cc_icons/americanexpress.png"></img> -->
                                    
                                                  <img height="10px" width="20px" src="/images/visa.png"></img>
                                                  <img height="10px" width="20px" src="/images/mastercard.png"></img>
                                                  <img height="10px" width="20px" src="/images/american_express.png"></img>
<!--                                                   <img height="25px" width="100px" src="/images/cc/mcvsax_acc_opt_hrz_144_3x.png"></img> -->
                                    
                                                <div class="checkbox pull-right">
                                                    <label for="remember"><input type="checkbox" name="remember"/>Remember</label>
                                                </div>
                                            </div>

                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="cardNumber">
                                                        CARD NUMBER</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="cardNumber" id="cardNumber" placeholder="Valid Card Number"
                                                               {{--v-model="cardNumber" data-rules="required"--}}
                                                        />
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                                    </div>
                                                </div>
                                                <!-- Name -->
                                                <div class="control-group">
                                                    <label class="control-label"  for="username">CARD HOLDER'S NAME</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" name="cardHolder" id="cardHolder" placeholder=""/>
                                                    </div>
                                                </div>

                                                <input type="hidden" class="form-control" value="{{ $invoice->id }}" name="invoiceId" id="invoiceId" />

                                                <div class="row">
                                                    <div class="col-xs-7 col-md-7">
                                                        {{--<div class="form-group">--}}
                                                        <label for="expiryMonth">EXPIRY DATE</label>
                                                        <div class="form-group">
                                                            <div class="col-xs-6 col-lg-6 pl-ziro">
                                                                <input type="text" class="form-control" name="expiryMonth" id="expiryMonth" placeholder="MM"/>
                                                            </div>
                                                            <div class="col-xs-6 col-lg-6 pl-ziro">
                                                                <input type="text" class="form-control" name="expiryYear" id="expiryYear" placeholder="YY"/>
                                                            </div>
                                                        </div>
                                                        {{--</div>--}}
                                                    </div>
                                                    <div class="col-xs-5 col-md-5 pull-right">
                                                        <div class="form-group">
                                                            <label for="cvvCode">
                                                                CVV2 CODE</label>
                                                            <input type="password" class="form-control" name="cvvCode" id="cvvCode" placeholder="CVV2"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-xs-12">
                                      <div class="paypal_form" style="display: none; position:absolute; width:100%; height:100%">
                                          <img
                                                  style="margin-left: auto;margin-right: auto;display: block;"
                                                  src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_150x38.png" alt="Use PayPal" />
                                      </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <ul class="nav nav-pills nav-stacked">
                                    <li class="active" :invoice-total="invoiceTotal"><a href="#"><span class="badge pull-right"><span class="glyphicon glyphicon-usd"></span>@{{ invoiceTotal  }}</span> Invoice Total</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        {{ csrf_field() }}

                        <div class="row top-pad">
                            <div class="col-xs-12">
                                <div class="pull-right">
                                    <input type="submit" value="Pay Now" style="width:130px;">
                                    {{--<button style="width:130px;">Pay Now</button>--}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection