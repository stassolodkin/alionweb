@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        <h2 class=" heading elegantshadow">Candidates</h2>
    </section>

    <div class="wrapper top-pad bottom-pad" id="main">

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Resume upload</h3>
                    <hr />
                    <p>Upload your resume here to be considered for available positions. You may be contacted regarding currently available positions as well as future opportunities.</p>
                </div>
            </div>
        </div>
        <div class="container">
            <form method="POST" class="alion_form" id="resume_form" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group required">
                            <label for="first_name">First Name</label>
                            <input type="text" name="first_name" id="first_name" maxlength="50"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group required">
                            <label for="last_name">Last Name</label>
                            <input type="text" name="last_name" id="last_name" maxlength="50"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group required">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" maxlength="100"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" id="phone" maxlength="20"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="alion-tags">Add Skills(Up to 5)</label>
                            <select name="alion-tags" multiple data-role="tagsinput" id="alion-tags"></select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group required">
                            <label for="resume_file_name">Resume</label>
                            <div class="input-group">
                                <input id="resume" name="resume" type="file" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf" style="display: none;">
                                <input id="resume_file_name" type="text" class="form-control" placeholder="Select file">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary browse-button" type="button">Browse</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <div class="form-group">
                            <label for="message">Message</label><br />
                            <textarea name="message" id='message' maxlength="512"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row"></div>

                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <p id="form_success" style="color: green;   display: none;"></p>
                        <p id="form_error"   style="color: darkred; display: none;"></p>
                    </div>
                </div>


                <meta name="csrf-token" content="{{ csrf_token() }}">

                <div class="row top-pad">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="g-recaptcha recapture-width" data-sitekey="6LfDsB0UAAAAAK94xuewWMU5AqnwJWrnvvD014nS"></div>
                    </div>
                </div>

                <div class="row top-pad">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <input type="button" value="Send" onclick="uploadResume()">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection