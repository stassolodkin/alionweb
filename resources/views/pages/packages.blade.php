@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        {{--<div class="wrapper">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-12">--}}
                        <h2 class=" heading elegantshadow">Alion hosting packages</h2>
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </section>


    <div class="wrapper top-pad">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="packages_banner">
                        <img src="/images/packages_banner.gif" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    Select a suitable package. The prices in the table below are per month. The payments are taken yearly.
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div id="pricing-table" class="clear">
                    <div class="col-xs-12 col-md-4">
                        <div class="plan">

                            <h3>{{ $basic->name }}<span>$5</span></h3>

                            <div>$8 after 12 months</div>
<!--                             <form id="id_basic" action="/signup" method="POST">
                               <input type="hidden" name="package" value="basic"/>
                              {{ csrf_field() }}
                              <input class="signup" type="submit" value="Sign Up">
                            </form> -->
                           <a class="signup" href="http://hosting.alion.com.au/members/cart.php?a=add&pid=1&promocode=AlionBasic">Sign up</a>
                            <ul>
                                <li><b>500MB</b> Disk Space</li>
                                <li><b>2</b> Email Accounts</li>
                                <li>1 Domain </li>
                                <li> - </li>
                                <li> - </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        {{--<div class="plan" id="most-popular">--}}
                        <div class="plan">
                            <h3>{{ $intermediate->name }}<span>$8</span></h3>
                            <div>$12 after 12 months</div>
<!--                             <form id="id_intermediate" action="/signup" method="POST">
                                <input type="hidden" name="package" value="intermediate"/>
                                {{ csrf_field() }}
                                <input class="signup" type="submit" value="Sign Up">
                            </form> -->
                           <a class="signup" href="http://hosting.alion.com.au/members/cart.php?a=add&pid=2&promocode=AlionIntermediate">Sign up</a>
                            <ul>
                                <li><b>3GB</b> Disk Space</li>
                                <li><b>10</b> Email Accounts</li>
                                <li><b>5</b> Add On Domains</li>
                                <li> - </li>
                                <li> - </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="plan">
                            <h3>{{ $advanced->name }}<span>Free</span></h3>
                            <div>$19 after 6 months</div>
<!--                             <form id="id_advanced" action="/signup" method="POST">
                               <input type="hidden" name="package" value="advanced"/>
                              {{ csrf_field() }}
                              <input class="signup" type="submit" value="Sign Up">
                            </form> -->
                            <a class="signup" href="http://hosting.alion.com.au/members/cart.php?a=add&pid=3&promocode=AlionAdvanced">Sign up</a>
                            <ul>
                                <li><b>Unlimited</b> Disk Space</li>
                                <li><b>Unlimited</b> Email Accounts</li>
                                <li><b>Unlimited</b> Add On Domains</li>
                                <li>Daily Backups</li>
                                <li>Shell Access</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper top-pad bottom-pad">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    Note: The prices in the table above are monthly. The payments for the packages are taken yearly on the day of signup and, later, on the first day of each billing year.
                    For the Good and the Better packages, the first billing year starts on the first day of the 7th month after the initial signup.
                </div>
            </div>
        </div>
    </div>

    @include('elements.prefooter')

@endsection
