<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Alion Solutions</title>
    <meta name="description" content="Alion Solutions - Your ultimate IT solution">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

    <link rel="shortcut icon" href="favicon.ico"  type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/main.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/email.css') }}" rel="stylesheet">


    <link rel="stylesheet" href="css/normalize.css">

    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body id="home">




<div class="wrapper clearfix">
    {{--<div id="banner">--}}
        <div id="logo"><a href="{{ url('/') }}"><img width="156" height="70" src="/images/alion_logo.png" alt="logo"></a></div>
    {{--</div>--}}
<br />


</div>

<div style="text-align: center; vertical-align: middle; font-size: 1.3em;">
    Alion Solutions is delighted to announce a new start. Check back in a few days.
</div>
<br />
<div style="text-align: center; vertical-align: middle; font-size: 1.3em;">
    For any enquiries, use the our contact details or the email form below:
</div>
<hr />
<div class="wrapper top-pad">
    <h2>Contact details</h2>
    <p>
        Address: 3/21 Anderson St, Caulfield  Vic 3162.<br/>
        Ph:      03 99729846<br/>
        Email:   info@alion.com.au<br/>
    </p>
</div>

<div class="wrapper bottom-pad">
            @include('elements.contact_form')
</div>

@include('elements.footer')

<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>