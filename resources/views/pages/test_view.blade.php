@extends('layouts.web')

@section('content')

    <section id="page-header" class="clearfix">
        {{--<div class="wrapper">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-xs-12">--}}
                        <h2 class=" heading elegantshadow">TEST VIEW</h2>
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </section>


    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <section>
                      {{ $testData }}
                    </section>
                </div>
            </div>
        </div>
    </div>

    @include('elements.prefooter')

@endsection
