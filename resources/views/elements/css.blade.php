<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Alion Solutions</title>
    <meta name="description" content="Alion Solutions - Your ultimate IT solution">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

    <link rel="shortcut icon" href="/favicon.ico"  type="image/x-icon">

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/main.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/email.css') }}" rel="stylesheet">
{{--    <link href="{{ mix('/css/cycleSlider.css') }}" rel="stylesheet">--}}
    <link href="{{ mix('/css/menu.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/menu2.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/form.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/packages.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/packages.css') }}" rel="stylesheet">

    {{-- <link href="{{ mix('/css/sup-nav.css') }}" rel="stylesheet"> --}}
  
    <link href="{{ mix('/css/loader.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/normalize.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/bootstrap-tagsinput.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/bootstrap-dialog.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/slick.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/slick-theme.css') }}" rel="stylesheet">

    {{--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">--}}
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">


    {{--Fonts--}}
{{--    <link href="{{ mix('/fonts/JuliusSansOne-Regular.ttf') }}" rel="stylesheet">--}}

    <link href="https://fonts.googleapis.com/css?family=Unica+One" rel="stylesheet">

    <link href="//fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">

    <link href="//fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">

    <link href="//fonts.googleapis.com/css?family=Damion|Gurajada|Macondo|NTR|Philosopher|Tillana|Vibur" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed|Dancing+Script|Didact+Gothic|Flamenco|Galdeano|Gidugu|Glass+Antiqua|Lekton|Mandali|Montserrat|Mukta+Mahee|Six+Caps|Smythe|Sofadi+One|Tenali+Ramakrishna|Tillana" rel="stylesheet">





    {{--<link rel="stylesheet" href="css/normalize.css">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>