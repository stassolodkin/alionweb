<header class="header-wrapper clearfix">

    <div id="banner">
        <div id="logo"><a href="{{ url('/') }}"><img width="163" height="65" src="/images/alion_logo_green_blue.png" alt="logo"></a></div>
    </div>
        @if (!Auth::check() || Auth::user()->is_admin != "1")
                <span class="live_chat_widget">
                    <a
                            href='javascript:void(0)'
                            onclick="window.open('/start_live_chat/{{ csrf_token() }}', '_blank', 'location=0,height=575,width=520,resizable=0,scrollbars=0,status=yes');"
                    >
                        Live Chat
                    </a>
                </span>
        @endif

    <span class="call-now">
        <i class="fa fa-mobile-phone" aria-hidden="true"></i>
        <a href="tel:03 9972 9846">03 9972 9846</a>
        <a style="padding-left:10px;" href="mailto:info@alion.com.au"><i style="padding-right:3px;" class="fa fa-envelope" aria-hidden="true"></i>info@alion.com.au</a>
    </span>
    @include('elements.menu')

</header>