<header class="header-wrapper-2 clearfix">

    {{--<div  class="wrapper" >--}}
        <span class="call-now-2">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <a href="tel:03 9972 9846">03 9972 9846</a>
            <a style="padding-left:10px;" href="mailto:info@alion.com.au"><i style="padding-right:3px;" class="fa fa-envelope" aria-hidden="true"></i>info@alion.com.au</a>
        </span>

    <span class="live_chat_widget_floating_header">
        @if (!Auth::check())
            <a
                    href='javascript:void(0)'
                    onclick="window.open('/start_live_chat/{{ csrf_token() }}', '_blank', 'location=0,height=570,width=520,resizable=0,scrollbars=0,status=yes');"
            >
                Live Chat
            </a>
        @endif
    </span>


        @include('elements.menu2')
    {{--</div>--}}

</header>