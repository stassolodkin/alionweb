<div class="prefooter">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 top-pad bottom-pad white-form-text">
                    @include('elements.contact_form')
                </div>
                <div class="col-xs-12 col-sm-4 top-pad bottom-pad white-text">
                    <h2>How we can help</h2>
                    <hr />
                    {{--<h2 class="white-text">General</h2>--}}
                    <ul style="list-style: none;"><li><h2>General</h2></li></ul>
                    <ul style="list-style: none;">
                        <li>Build your software</li>
                        <li>Host your software</li>
                        <li>Build your IT infrastructure</li>
                        <li>Maintain your infrastructure</li>
                        <li>Security analysis and implementation</li>
                    </ul>
                    <hr />
                    {{--<h2 class="white-text">Software company specialty</h2>--}}
                    <ul style="list-style: none;"><li><h2>Software company specialty</h2></li></ul>
                    <ul style="list-style: none;">
                        <li>Code review</li>
                        <li>Automated testing</li>
                        <li>Continuous integration</li>
                        <li>Agile Implementation</li>
                        <li>Tools upgrade</li>
                    </ul>
                    <hr />
                    {{--<h2 class="white-text">Recruitment</h2>--}}
                    <ul style="list-style: none;"><li><h2>Recruitment</h2></li></ul>
                    <ul style="list-style: none;">
                        <li>Find your staff</li>
                        <li>Train your staff</li>
                        <li>Run technical tests</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4 top-pad bottom-pad white-text">
                    <h2>Social Media</h2>
                    <hr />
                    <div class="social-buttons">
                        <div class="row">
                            <div class="col-xs-3">
                                <a href="https://www.facebook.com/Alion-Solutions-917660771705071/"
                                   target="_blank">
                                    <i class="fa fa-2x fa-facebook-official"></i>
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a href="https://www.linkedin.com/company-beta/18086846"
                                   target="_blank">
                                    <i class="fa fa-2x fa-linkedin-square"></i>
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a href="https://plus.google.com/101341028837121710223"
                                        target="_blank">
                                    <i class="fa fa-2x fa-google-plus-square"></i>
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a href=""
                                   target="_blank">
                                    <i class="fa fa-2x fa-twitter-square"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>