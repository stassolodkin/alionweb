<section class="contact_form">

    <h2>Enquire Now</h2>
    <hr/>

    <form method="POST" class="form">

        {{--<div class="form-group">--}}
            {{--<label class="col-md-4 control-label">Name</label>--}}
            {{--<div class="col-md-4 inputGroupContainer">--}}
                {{--<div class="input-group">--}}
                    {{--<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>--}}
                    {{--<input  id="email_name" name="email_name" placeholder="Your Name" class="form-control"  type="text">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <p class="name">
            <label for="email_name">Name</label><br />
            <input type="text" name="email_name" id="email_name" maxlength="100"/>
        </p>

        <p class="email">
            <label for="email_from">Email</label><br />
            <input type="text" name="email_from" id="email_from" maxlength="100"/>
        </p>

        <p class="subject">
            <label for="email_subject">Subject</label><br />
            <input type="text" name="email_subject" id="email_subject" maxlength="100"/>
        </p>

        <p class="message">
            <label for="email_body">Message</label><br />
            <textarea name="email_body" id='email_body' maxlength="512"></textarea>
        </p>

        <div class="row"></div>

        <p id="form_success" style="color: green; display: none;"></p>
        <p id="form_error" style="color: darkred; display: none;"></p>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <div class="g-recaptcha recapture-width" data-sitekey="6LfDsB0UAAAAAK94xuewWMU5AqnwJWrnvvD014nS"></div>
        {{--<div class="g-recaptcha" data-sitekey="6LeURx4UAAAAADCYoJ-uS2klIDniHW0-5fu671_1"></div>--}}

        <br />
        <input type="button" class="send-email" value="Send" onclick="sendEmail()">
    </form>
</section>
