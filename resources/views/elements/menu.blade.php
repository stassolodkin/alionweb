<nav id="topnav" role="navigation">
    {{--<div class="menu-toggle">Menu<i style="float:right" class="menu-bars fa fa-bars fa-lg"></i></div>--}}
    <div class="menu-toggle"><i class="menu-bars fa fa-bars fa-lg"></i></div>
    <ul class="srt-menu" id="menu-main-navigation">
            @if (Auth::check())
                <li><a href="{{ url('/home') }}">DASHBOARD</a></li>
            @endif

{{--                @extends((!isset(Auth::user()->id)) ? 'layouts.web': ((Auth::user()->is_admin == "1") ? 'layouts.admin' : 'layouts.customer'));--}}

                @if(!Auth::check())
                    <li
                        @if( preg_match( '/\//', Request::path() ) )
                            class="current"
                        @endif
                    ><a href="{{ url('/') }}">HOME</a></li>
                @endif

                <li
                    @if( preg_match( '/services*/', Request::path() ) )
                        class="current"
                    @endif
                ><a href="{{ url('/services') }}">SERVICES</a>

                    <ul>
                        <li><a href="{{ url('/services/software_development') }}">Software Development</a></li>
                        <li><a href="{{ url('/services/service_hosting') }}">Hosting</a></li>
                        <li><a href="{{ url('/services/consulting') }}">Consulting</a></li>
                        {{-- <li><a href="{{ url('/services/recruitment') }}">Recruitment</a></li> --}}
                    </ul>
                </li>



            <li
                @if( preg_match( '/about$/', Request::path() ) )
                    class="current"
                @endif
            ><a href="{{ url('/about') }}">ABOUT US</a></li>

            <li
                @if( preg_match( '/contact$/', Request::path() ) )
                    class="current"
                @endif
            ><a href="{{ url('/contact') }}">CONTACT</a></li>
    </ul>
</nav>