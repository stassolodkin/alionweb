<div class="request-call-section">
    <div class="request-call-link">
        <h1 class="call_request_long">For a free consultation <span  style="cursor:pointer;" data-toggle="modal" data-target="#request_call_modal"><i>Request a call</i></span></h1>
        <h1 class="call_request_short"><span  style="cursor:pointer;" data-toggle="modal" data-target="#request_call_modal"><i>Request a free call</i></span></h1>
    </div>
</div>

<div class="modal fade bottom-left" id="request_call_modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div id="banner">
                    <div id="logo"><a href="{{ url('/') }}"><img width="163" height="65" src="/images/alion_logo_green_blue.png" alt="logo"></a></div>
                </div>

                <span class="call-now">
                    <i class="fa fa-mobile-phone" aria-hidden="true"></i>
                    <a href="tel:03 9972 9846">03 9972 9846</a>
                </span>
            </div>

            <div class="modal-body">
                <h2 class="modal-title">Call request</h2>
                <hr />
                <p>Submit your details here. You will be contacted at the next available opportunity during Australian business hours.</p>


                <hr />
                <div class="container-fluid">
                    <div class="row">
                        <form method="POST" class="alion_form" id="call_request_form" enctype="multipart/form-data">

                            <div class="col-xs-12 col-md-4">
                                <label for="contact_name">Contact Name</label>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <input type="text" name="contact_name" id="contact_name" maxlength="100"/>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <label for="contact_phone">Phone</label>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <input type="text" name="contact_phone" id="contact_phone" maxlength="20"/>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <label for="contact_email">Email(Optional)</label>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <input type="text" name="contact_email" id="contact_email" maxlength="100"/>
                            </div>

                            <div class="col-xs-12 col-md-4">
                                <label for="contact_topic">Topic(Optional)</label>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <input type="text" name="contact_topic" id="contact_topic" maxlength="100"/>
                            </div>

                            <div class="col-xs-12 top-pad">
                                <meta name="csrf-token-call-request" content="{{ csrf_token() }}">
                                <p id="request_call_form_success" style="color: green; display: none;"></p>
                                <p id="request_call_form_error" style="color: darkred; display: none;"></p>
                                {{--<div class="pull-right g-recaptcha recapture-width" data-sitekey="6LfDsB0UAAAAAK94xuewWMU5AqnwJWrnvvD014nS"></div>--}}
                            </div>

                            <input type="text" name="comments" id="comments" class="comments_class" tabindex="-1"/>

                            <div class="col-xs-12 top-pad">
                                <button type="button" class="btn btn-primary pull-right left-margin" onclick="submitCallRequest()">Submit</button>
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>

            <div class="modal-footer">
                <div class="container-fluid text-center">
                    Alion Solutions - Do IT Right
                </div>
            </div>

        </div>

    </div>
</div>