  <div class="technologies_slide">
      <div><i class="devicon-php-plain"></i></div>
      <div><i class="devicon-python-plain-wordmark"></i></div>
      <div><i class="devicon-ruby-plain-wordmark"></i></div>
      <div><i class="devicon-nodejs-plain-wordmark"></i></div>
      <div><i class="devicon-mysql-plain-wordmark"></i></div>
      <div><i class="devicon-oracle-original"></i></div>
      <div><i class="devicon-postgresql-plain-wordmark"></i></div>
      <div><i class="devicon-redis-plain-wordmark"></i></div>
      <div><i class="devicon-apache-line-wordmark"></i></div>
      <div><i class="devicon-nginx-original"></i></div>
  </div>