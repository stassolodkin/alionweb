<nav id="topnav-2" role="navigation">

    <ul class="srt-menu-2" id="menu-main-navigation-2">

        <li><a href="{{ url('/') }}">Home</a></li>

            <li><a href="{{ url('/services') }}">Services</a>

                <ul>
                    <li><a href="{{ url('/services/software_development') }}">Software Development</a></li>
                    <li><a href="{{ url('/services/service_hosting') }}">Hosting</a></li>
                    <li><a href="{{ url('/services/consulting') }}">Consulting</a></li>
                    {{-- <li><a href="{{ url('/services/recruitment') }}">Recruitment</a></li> --}}
                </ul>

            </li>

            <li><a href="{{ url('/about') }}">About</a></li>

            <li><a href="{{ url('/contact') }}">Contact</a></li>
    </ul>

</nav>