<script src="{{ mix('/js/app.js') }}"></script>
<script src="{{ mix('/js/main.js') }}"></script>
<script src="{{ mix('/js/chat_helpers.js') }}"></script>
<script src="{{ mix('/js/form.js') }}"></script>
@if( preg_match( '/signup_step_2/', Request::path() ) )
    <script src="{{ mix('/js/invoice.js') }}"></script>
@endif
<script src="{{ mix('/js/libs/bootstrap-tagsinput.js') }}"></script>
<script src="{{ mix('/js/libs/bootstrap-dialog.js') }}"></script>
<script src="{{ mix('/js/libs/slick.min.js') }}"></script>
{{--<script src="{{ mix('/js/libs/sup-nav.js') }}"></script>--}}
{{--<script src="{{ mix('/js/libs/tinymce/tinymce.min.js') }}"></script>--}}
<script type="text/javascript" charset="utf8" src="/js/libs/tinymce/tinymce.min.js"></script>

<script>
    var editor_config = {
        path_absolute : "{{ URL::to('/') }}/",
        selector : "#content",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.grtElementByTagName('body')[0].clientHeight;
            var cmsURL = editor_config.path_absolute+'laravel-filemanaget?field_name'+field_name;
            if (type = 'image') {
                cmsURL = cmsURL+'&type=Images';
            } else {
                cmsUrl = cmsURL+'&type=Files';
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizeble : 'yes',
                close_previous : 'no'
            });
        }
    };

    tinymce.init(editor_config);
</script>
{{--        <script src="{{ mix('/js/libs/cycleSlider.js') }}"></script>--}}
{{--        <script src="{{ mix('/js/libs/sup-nav.js') }}"></script>--}}

<script src='//www.google.com/recaptcha/api.js'></script>
{{--<script src='http://www.google.com/recaptcha/api.js'></script>--}}
{{--<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>--}}
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-maxlength/1.7.0/bootstrap-maxlength.min.js"></script>
