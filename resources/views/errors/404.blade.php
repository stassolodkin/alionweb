@extends('layouts.web')

@section('content')

    <div id="main" class="wrapper">
        <h1 style="text-align:center;">ERROR 404</h1>
        <p style="text-align:center;">
            The page you requested does not exist.
        </p>
    </div>

    @include('elements.prefooter')

@endsection