@extends('layouts.web')

@section('content')

    <div id="main" class="wrapper">
        <h1 style="text-align:center;">An error has occurred: {{ $exception->getMessage() }}</h1>
        <p style="text-align:center;">
            Your account has not been setup correctly. Please contact Alion Support on info@alion.com.au
        </p>
    </div>

    @include('elements.prefooter')

@endsection